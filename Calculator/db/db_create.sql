create database `currency_calculator`;

use `currency_calculator`;

create or replace table currencies
(
	id int auto_increment
		primary key,
	name varchar(50) not null,
	code varchar(5) not null,
	unit double not null,
	value_return double not null,
	value_in_bgn double not null,
	constraint currencies_code_uindex
		unique (code),
	constraint currencies_name_uindex
		unique (name)
);

create or replace table users
(
	id int auto_increment
		primary key,
	username varchar(20) not null,
	password varchar(20) not null,
	constraint users_username_uindex
		unique (username)
);

