-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for currency_calculator
CREATE DATABASE IF NOT EXISTS `currency_calculator` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `currency_calculator`;

-- Dumping structure for table currency_calculator.currencies
CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(5) NOT NULL,
  `unit` double NOT NULL,
  `value_in_bgn` double NOT NULL,
  `value_return` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `currencies_code_uindex` (`code`),
  UNIQUE KEY `currencies_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Dumping data for table currency_calculator.currencies: ~32 rows (approximately)
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` (`id`, `name`, `code`, `unit`, `value_in_bgn`, `value_return`) VALUES
	(1, 'Bulgarian lev', 'BGN', 1, 1, 1),
	(2, 'British pound', 'GBP', 1, 2.25227, 0.443997),
	(3, 'Australian dollar', 'AUD', 1, 1.25591, 0.796235),
	(4, 'Chinese yuan', 'CNY', 10, 2.50854, 3.98638),
	(5, 'Canadian dollar', 'CAD', 1, 1.32088, 0.757071),
	(15, 'South Korean won', 'KRW', 1000, 1.44667, 691.243),
	(16, 'Mexican peso', 'MXN', 100, 8.01336, 12.4792),
	(17, 'Malaysian ringgit', 'MYR', 10, 3.95661, 2.52742),
	(18, 'Norwegian krone', 'NOK', 10, 1.95271, 5.12109),
	(19, 'New Zealand dollar', 'NZD', 1, 1.16516, 0.858251),
	(20, 'Philippine peso', 'PHP', 100, 3.36742, 29.6963),
	(21, 'Polish zloty', 'PLN', 10, 4.29683, 2.3273),
	(22, 'Romanian leu', 'RON', 10, 3.9697, 2.51908),
	(23, 'Russian rouble', 'RUB', 100, 2.15525, 46.3983),
	(24, 'Swedish krona', 'SEK', 10, 1.92119, 5.20511),
	(25, 'Singapore dollar', 'SGD', 1, 1.21904, 0.820318),
	(26, 'Thai baht', 'THB', 100, 5.20652, 19.2067),
	(27, 'Turkish lira', 'TRY', 10, 1.95413, 5.11737),
	(28, 'United States Dollar', 'USD', 1, 1.6239, 0.615801),
	(29, 'Japanese yen', 'JPY', 100, 1.48281, 67.4395),
	(30, 'Brazilian real', 'BRL', 10, 2.98568, 3.34932),
	(31, 'Swiss franc', 'CHF', 1, 1.77544, 0.563241),
	(32, 'Czech koruna', 'CZK', 100, 7.58868, 13.1775),
	(33, 'Danish krone', 'DKK', 10, 2.63004, 3.80222),
	(34, 'Hong Kong dollar', 'HKD', 10, 2.09059, 4.78334),
	(35, 'Croatian kuna', 'HRK', 10, 2.5928, 3.85683),
	(36, 'Hungarian forint', 'HUF', 1000, 5.43377, 184.034),
	(37, 'Indonesian rupiah', 'IDR', 10000, 1.12569, 8883.44),
	(38, 'Israeli new shekel', 'ILS', 10, 5.00814, 1.99675),
	(39, 'Indian rupee', 'INR', 100, 2.19766, 45.5029),
	(40, 'South African rand', 'ZAR', 10, 1.12434, 8.89411),
	(41, 'Icelandic krona', 'ISK', 100, 1.29525, 77.2052);
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;

-- Dumping structure for table currency_calculator.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table currency_calculator.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`) VALUES
	(1, 'admin', 'admin'),
	(2, 'ivan', '12345678'),
	(3, 'matey', '12345678'),
	(4, 'hristo', '12345');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
