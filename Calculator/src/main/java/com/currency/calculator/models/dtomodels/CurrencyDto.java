package com.currency.calculator.models.dtomodels;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CurrencyDto {

    @NotNull(message = "Currency name should not be empty")
    @Size(min = 2, max = 20, message = "Currency name should be between 2 and 50 symbols")
    private String name;

    @NotNull(message = "Currency code should not be empty")
    @Size(min = 2, max = 20, message = "Currency code should be between 2 and 5 symbols")
    private String code;

    @Positive
    @NotNull
    private double units;

    @Positive
    @NotNull
    private double valueBGN;

    @Positive
    @NotNull
    private double valueReturn;

    public CurrencyDto() {
    }

    public CurrencyDto(String name, String code, double units, double valueBGN, double valueReturn) {
        this.name = name;
        this.code = code;
        this.units = units;
        this.valueBGN = valueBGN;
        this.valueReturn = valueReturn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getValueBGN() {
        return valueBGN;
    }

    public void setValueBGN(double valueBGN) {
        this.valueBGN = valueBGN;
    }

    public double getValueReturn() {
        return valueReturn;
    }

    public void setValueReturn(double valueReturn) {
        this.valueReturn = valueReturn;
    }
}
