package com.currency.calculator.models.dtomodels;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull(message = "Username should not be empty")
    @Size(min = 2, max = 20, message = "Username should be between 2 and 20 symbols.")
    private String username;

    @NotNull(message = "Password should not be empty")
    @Size(min = 2, max = 20, message = "Password should be between 2 and 20 symbols.")
    private String password;

    public UserDto() {
    }

    public UserDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
