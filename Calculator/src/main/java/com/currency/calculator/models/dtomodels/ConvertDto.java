package com.currency.calculator.models.dtomodels;

public class ConvertDto {

    private int currencyFromId;

    private int currencyToId;

    private double amount;

    public ConvertDto(int currencyFromId, int currencyToId, double amount) {
        this.currencyFromId = currencyFromId;
        this.currencyToId = currencyToId;
        this.amount = amount;
    }

    public ConvertDto() {
    }

    public int getCurrencyFromId() {
        return currencyFromId;
    }

    public void setCurrencyFromId(int currencyFromId) {
        this.currencyFromId = currencyFromId;
    }

    public int getCurrencyToId() {
        return currencyToId;
    }

    public void setCurrencyToId(int currencyToId) {
        this.currencyToId = currencyToId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
