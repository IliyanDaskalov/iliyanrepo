package com.currency.calculator.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "currencies")
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "unit")
    private double units;

    @Column(name = "value_in_bgn")
    private double valueBGN;

    @Column(name = "value_return")
    private double valueReturn;

    public Currency() {
    }

    public Currency(String name, String code, double units, double valueBGN, double valueReturn) {
        this.name = name;
        this.code = code;
        this.units = units;
        this.valueBGN = valueBGN;
        this.valueReturn = valueReturn;
    }

    public Currency(int id, String name, String code, double units, double valueBGN, double valueReturn) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.units = units;
        this.valueBGN = valueBGN;
        this.valueReturn = valueReturn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public double getValueBGN() {
        return valueBGN;
    }

    public void setValueBGN(double valueBGN) {
        this.valueBGN = valueBGN;
    }

    public double getValueReturn() {
        return valueReturn;
    }

    public void setValueReturn(double valueReturn) {
        this.valueReturn = valueReturn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return name.equals(currency.name) && code.equals(currency.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, code);
    }
}
