package com.currency.calculator.repositories.contracts;

import com.currency.calculator.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    void createUser(User user);

    List<User> getAllUsers();

    User tryGetUser(String username, String password);

    boolean usernameExists(String name);

    User getUserByUsername(String username);

    Optional<User> userByUsername(String username);
}
