package com.currency.calculator.repositories;

import com.currency.calculator.exceptions.EntityNotFoundException;
import com.currency.calculator.models.Currency;
import com.currency.calculator.repositories.contracts.CurrencyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CurrencyRepositoryImpl implements CurrencyRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CurrencyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Currency> getAllCurrencies() {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery("from Currency order by name asc", Currency.class);
            return query.getResultList();
        }
    }

    @Override
    public Currency getCurrencyById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Currency currency = session.get(Currency.class, id);
            if (currency == null) {
                throw new EntityNotFoundException("Currency", id);
            }
            return currency;
        }
    }

    @Override
    public void createCurrency(Currency currency) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(currency);
            session.getTransaction().commit();
        }
    }

    @Override
    public Currency updateCurrency(Currency currency) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(currency);
            session.getTransaction().commit();
        }
        return currency;
    }

    @Override
    public Currency getCurrencyByCode(String code) {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery("from Currency where code = :code", Currency.class);
            query.setParameter("code", code);
            List<Currency> currencyList = query.getResultList();
            if (currencyList.isEmpty()) {
                throw new EntityNotFoundException("Currency", "code", code);
            }
            return currencyList.get(0);
        }
    }

    @Override
    public boolean nameExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery("from Currency WHERE name = :name", Currency.class);
            query.setParameter("name", name);
            List<Currency> result = query.list();
            return !result.isEmpty();
        }
    }

    @Override
    public boolean codeExists(String code) {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery("from Currency WHERE code = :code", Currency.class);
            query.setParameter("code", code);
            List<Currency> result = query.list();
            return !result.isEmpty();
        }
    }

}
