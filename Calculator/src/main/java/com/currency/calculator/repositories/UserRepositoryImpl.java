package com.currency.calculator.repositories;

import com.currency.calculator.exceptions.EntityNotFoundException;
import com.currency.calculator.exceptions.WrongPasswordException;
import com.currency.calculator.models.User;
import com.currency.calculator.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User ", User.class);
            return query.getResultList();
        }
    }

    @Override
    public void createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public User tryGetUser(String username, String password) {
        User user = getUserByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("User", "username", username);
        }
        if (!user.getPassword().equals(password)) {
            throw new WrongPasswordException();
        }
        return user;

    }

    @Override
    public boolean usernameExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User WHERE username = :name", User.class);
            query.setParameter("name", name);
            List<User> result = query.list();
            return !result.isEmpty();
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("FROM User WHERE username = :username", User.class);
            query.setParameter("username", username);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return query.getResultList().get(0);
        }
    }

    @Override
    public Optional<User> userByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("FROM User WHERE username = :username", User.class);
            query.setParameter("username", username);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "username", username);
            }
            List<User> result = query.getResultList();
            return Optional.of(result.get(0));
        }
    }
}
