package com.currency.calculator.repositories.contracts;


import com.currency.calculator.models.Currency;

import java.util.List;

public interface CurrencyRepository {
    List<Currency> getAllCurrencies();

    Currency getCurrencyById(int id);

    void createCurrency(Currency currency);

    Currency updateCurrency(Currency currency);

    Currency getCurrencyByCode(String code);

    boolean nameExists(String name);

    boolean codeExists(String code);
}
