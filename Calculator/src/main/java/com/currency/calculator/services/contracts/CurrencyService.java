package com.currency.calculator.services.contracts;

import com.currency.calculator.models.Currency;

import java.util.List;

public interface CurrencyService {
    List<Currency> getAllCurrencies();

    Currency getCurrencyById(int id);

    Currency createCurrency(Currency currency);

    Currency updateCurrency(Currency currency);

    double convert(int currencyFromId, int currencyToId, double amount);

    Currency getCurrencyByCode(String code);
}
