package com.currency.calculator.services.mappers;

import com.currency.calculator.models.User;
import com.currency.calculator.models.dtomodels.UserDto;
import com.currency.calculator.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {

    private final UserService userService;

    @Autowired
    public UserModelMapper(UserService userService) {
        this.userService = userService;
    }

    public User userDtoToUser(UserDto userDto) {
        return new User(userDto.getUsername(), userDto.getPassword());
    }
}
