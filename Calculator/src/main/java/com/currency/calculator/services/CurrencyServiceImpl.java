package com.currency.calculator.services;

import com.currency.calculator.exceptions.DuplicateEntityException;
import com.currency.calculator.models.Currency;
import com.currency.calculator.repositories.contracts.CurrencyRepository;
import com.currency.calculator.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public List<Currency> getAllCurrencies() {
        return currencyRepository.getAllCurrencies();
    }

    @Override
    public Currency getCurrencyById(int id) {
        return currencyRepository.getCurrencyById(id);
    }

    @Override
    public Currency createCurrency(Currency currency) {
        if (currencyRepository.nameExists(currency.getName())) {
            throw new DuplicateEntityException("Currency", "name", currency.getName());
        }
        if (currencyRepository.codeExists(currency.getCode())) {
            throw new DuplicateEntityException("Currency", "code", currency.getCode());
        }
        currencyRepository.createCurrency(currency);
        return currency;
    }

    @Override
    public Currency updateCurrency(Currency currency) {
        currencyRepository.updateCurrency(currency);
        return currency;
    }

    @Override
    public double convert(int currencyFromId, int currencyToId, double amount) {
        Currency currencyFrom = getCurrencyById(currencyFromId);
        Currency currencyTo = getCurrencyById(currencyToId);

        return ((amount * currencyFrom.getValueBGN()) * currencyTo.getValueReturn()) / currencyFrom.getUnits();
    }

    @Override
    public Currency getCurrencyByCode(String code) {
        return currencyRepository.getCurrencyByCode(code);
    }
}
