package com.currency.calculator.services.mappers;

import com.currency.calculator.models.Currency;
import com.currency.calculator.models.dtomodels.CurrencyDto;
import com.currency.calculator.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CurrencyModelMapper {

    private final CurrencyService currencyService;

    @Autowired
    public CurrencyModelMapper(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }


    public Currency currencyDtoToCurrency(CurrencyDto currencyDto) {
        return new Currency(currencyDto.getName(),
                currencyDto.getCode(),
                currencyDto.getUnits(),
                currencyDto.getValueBGN(),
                currencyDto.getValueReturn());
    }
}
