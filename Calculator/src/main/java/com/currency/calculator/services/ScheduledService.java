package com.currency.calculator.services;

import com.currency.calculator.models.Currency;
import com.currency.calculator.services.contracts.CurrencyService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

import static com.currency.calculator.services.utils.Constants.TEN_MINUTES;

@Service
@Transactional
public class ScheduledService {

    public final CurrencyService currencyService;

    @Autowired
    public ScheduledService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @Scheduled(fixedRate = TEN_MINUTES)
    public void scrapeData() {
        Document doc;
        try {
            doc = Jsoup.connect("https://www.infostock.bg/infostock/control/exchangerates/bnb")
                    .userAgent("mozilla/17.0")
                    .get();
            Elements evenElements = doc.select("tr.evenrow");
            Elements oddElements = doc.select("tr.oddrow");

            updateData(evenElements);
            updateData(oddElements);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateData(Elements elements) {
        for (Element element : elements) {
            Currency currency = currencyService.getCurrencyByCode(element.getElementsByTag("td").get(0).text());

            String textUnits = element.getElementsByTag("td").get(1).text().replace(" ", "");
            String textBGN = element.getElementsByTag("td").get(2).text().replace(" ", "");
            String textReturn = element.getElementsByTag("td").get(4).text().replace(" ", "");

            currency.setCode(element.getElementsByTag("td").get(0).text());
            currency.setUnits(Double.parseDouble(textUnits));
            currency.setValueBGN(Double.parseDouble(textBGN));
            currency.setValueReturn(Double.parseDouble(textReturn));

            currencyService.updateCurrency(currency);
        }
    }

}
