package com.currency.calculator.services;

import com.currency.calculator.exceptions.DuplicateEntityException;
import com.currency.calculator.models.User;
import com.currency.calculator.repositories.contracts.UserRepository;
import com.currency.calculator.services.contracts.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public User createUser(User user) {
        if (userRepository.usernameExists(user.getUsername())) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        userRepository.createUser(user);
        return user;
    }

    @Override
    public User tryGetUser(String username, String password) {
        return userRepository.tryGetUser(username, password);
    }

    @Override
    public Optional<User> userByUsername(String username) {
        return userRepository.userByUsername(username);
    }
}
