package com.currency.calculator.services.contracts;

import com.currency.calculator.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();

    User getUserByUsername(String username);

    User createUser(User user);

    User tryGetUser(String username, String password);

    Optional<User> userByUsername(String username);
}
