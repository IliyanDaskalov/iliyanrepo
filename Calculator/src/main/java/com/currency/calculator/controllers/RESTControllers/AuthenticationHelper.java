package com.currency.calculator.controllers.RESTControllers;

import com.currency.calculator.exceptions.EntityNotFoundException;
import com.currency.calculator.exceptions.UnauthorizedOperationException;
import com.currency.calculator.models.User;
import com.currency.calculator.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationHelper {

    public static final String AUTHORIZATION_USERNAME = "username";
    public static final String AUTHORIZATION_PASSWORD = "password";

    private final UserService userService;

    @Autowired
    public AuthenticationHelper( UserService userService) {

        this.userService = userService;
    }

    public User getUserCredentials(HttpHeaders headers) throws EntityNotFoundException {
        if (!headers.containsKey(AUTHORIZATION_USERNAME) || !headers.containsKey(AUTHORIZATION_PASSWORD)) {
            throw new UnauthorizedOperationException("The request status requires authentication");
        }

        String username = headers.getFirst(AUTHORIZATION_USERNAME);
        String password = headers.getFirst(AUTHORIZATION_PASSWORD);
        return userService.tryGetUser(username, password);

    }
}
