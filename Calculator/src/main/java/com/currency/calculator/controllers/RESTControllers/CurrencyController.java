package com.currency.calculator.controllers.RESTControllers;

import com.currency.calculator.models.Currency;
import com.currency.calculator.models.dtomodels.CurrencyDto;
import com.currency.calculator.models.User;
import com.currency.calculator.services.contracts.CurrencyService;
import com.currency.calculator.services.mappers.CurrencyModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/currency")
public class CurrencyController {

    private final CurrencyService currencyService;
    private final CurrencyModelMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CurrencyController(CurrencyService currencyService,
                              CurrencyModelMapper mapper,
                              AuthenticationHelper authenticationHelper) {
        this.currencyService = currencyService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Currency> getAllCurrencies() {
        return currencyService.getAllCurrencies();
    }

    @GetMapping("{id}")
    public Currency getCurrencyById(@PathVariable int id) {
        return currencyService.getCurrencyById(id);
    }

    @GetMapping("{currencyFromId}/to/{currencyToId}")
    public double convert(@PathVariable int currencyFromId,
                          @PathVariable int currencyToId,
                          @RequestParam double amount) {
        return currencyService.convert(currencyFromId, currencyToId, amount);
    }

    @PostMapping
    public Currency createCurrency(@Valid @RequestBody CurrencyDto currencyDto, @RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.getUserCredentials(headers);
        Currency currency = mapper.currencyDtoToCurrency(currencyDto);
        currencyService.createCurrency(currency);
        return currency;
    }
}
