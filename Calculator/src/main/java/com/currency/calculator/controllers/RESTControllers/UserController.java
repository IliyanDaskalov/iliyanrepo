package com.currency.calculator.controllers.RESTControllers;

import com.currency.calculator.models.User;
import com.currency.calculator.models.dtomodels.UserDto;
import com.currency.calculator.services.contracts.UserService;
import com.currency.calculator.services.mappers.UserModelMapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;
    private final UserModelMapper mapper;

    public UserController(UserService userService, UserModelMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @PostMapping
    public User createUser(@Valid @RequestBody UserDto userDto) {
        User user = mapper.userDtoToUser(userDto);
        userService.createUser(user);
        return user;
    }
}
