package com.currency.calculator.controllers.MVCControllers;

import com.currency.calculator.models.User;
import com.currency.calculator.models.dtomodels.ConvertDto;
import com.currency.calculator.models.dtomodels.UserDto;
import com.currency.calculator.services.contracts.CurrencyService;
import com.currency.calculator.services.contracts.UserService;
import com.currency.calculator.services.mappers.UserModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping()
public class HomeControllerMVC {

    private final CurrencyService currencyService;
    private final UserService userService;
    private final UserModelMapper userModelMapper;

    @Autowired
    public HomeControllerMVC(CurrencyService currencyService,
                             UserService userService,
                             UserModelMapper userModelMapper) {
        this.currencyService = currencyService;
        this.userService = userService;
        this.userModelMapper = userModelMapper;
    }

    @GetMapping
    public String showHomepage(Model model, HttpSession session) {
        User user = (User) session.getAttribute("loggedUser");
        model.addAttribute("allCurrencies", currencyService.getAllCurrencies());
        model.addAttribute("convert", new ConvertDto());

        if (user != null) {
            return "redirect:/admin";
        }
        return "index";
    }

    @PostMapping("")
    public String convert(@Valid @ModelAttribute("convert")ConvertDto convertDto,
                          HttpSession session,
                          Model model) {
        model.addAttribute("allCurrencies", currencyService.getAllCurrencies());
        Double result = currencyService.convert(convertDto.getCurrencyFromId(), convertDto.getCurrencyToId(), convertDto.getAmount());
        model.addAttribute("result", result);
        model.addAttribute("convert", convertDto);
        return "index";
    }

    @GetMapping("/login")
    public String showLogin(HttpSession session, Model model) {
        model.addAttribute("user", new UserDto());
        return "login";
    }

    @PostMapping("register")
    public String register(@Valid @ModelAttribute("user") UserDto userDto,
                           BindingResult errors,
                           HttpSession session,
                           Model model) {
        if (errors.hasErrors()) {
            return "/login";
        }
        try {
            User newUser = userModelMapper.userDtoToUser(userDto);
            userService.createUser(newUser);
            return "redirect:/";
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logoutPage")
    public String showLogout(HttpSession session) {
        return "/logoutPage";
    }
}
