package com.currency.calculator.controllers.MVCControllers;

import com.currency.calculator.models.dtomodels.CurrencyDto;
import com.currency.calculator.services.contracts.CurrencyService;
import com.currency.calculator.services.mappers.CurrencyModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdministratorControllerMVC {

    private final CurrencyService currencyService;
    private final CurrencyModelMapper currencyModelMapper;

    @Autowired
    public AdministratorControllerMVC(CurrencyService currencyService,
                                      CurrencyModelMapper currencyModelMapper) {
        this.currencyService = currencyService;
        this.currencyModelMapper = currencyModelMapper;
    }

    @GetMapping
    public String showAdminPage(Model model, HttpSession session) {
        model.addAttribute("allCurrencies", currencyService.getAllCurrencies());
        return "administrator";
    }

    @GetMapping("/currency")
    public String getCreateCategoryView(Model model, HttpSession session) {
        CurrencyDto currencyDto = new CurrencyDto();
        model.addAttribute("currency", currencyDto);

        return "createCurrency";
    }

    @PostMapping("/currency")
    public String createCategory(@Valid @ModelAttribute("currency") CurrencyDto currencyDto,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {

        if (errors.hasErrors()) {
            return "createCurrency";
        }
        try {
            currencyService.createCurrency(currencyModelMapper.currencyDtoToCurrency(currencyDto));
        } catch (RuntimeException e) {
            model.addAttribute("error", e.getMessage());
            return "createCurrency";
        }
        return "redirect:/admin";
    }
}
