package com.currency.calculator.services;

import com.currency.calculator.exceptions.DuplicateEntityException;
import com.currency.calculator.models.User;
import com.currency.calculator.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.currency.calculator.services.Helpers.createUser;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void createUser_Should_Throws_When_Username_Exists() {
        User mockUser = createUser();
        User newUser = createUser();
        newUser.setId(2);

        when(userRepository.usernameExists(newUser.getUsername())).thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.createUser(newUser));
    }

    @Test
    public void createUser_Should_Call_Repository_When_Valid() {
        User mockUser = createUser();
        User newUser = createUser();
        newUser.setId(2);
        newUser.setUsername("newUser");

        when(userRepository.usernameExists(newUser.getUsername())).thenReturn(false);

        userService.createUser(newUser);

        verify(userRepository, times(1)).createUser(newUser);
    }

    @Test
    public void getAllUsers_Should_Call_Repository() {
        userService.getAllUsers();

        verify(userRepository, times(1)).getAllUsers();
    }

    @Test
    public void tryGetUser_Should_Call_Repository() {
        User user = createUser();

        userService.tryGetUser(user.getUsername(), user.getPassword());

        verify(userRepository, times(1)).tryGetUser(user.getUsername(), user.getPassword());
    }

    @Test
    public void getUserByUsername_Should_Call_Repository() {
        User user = createUser();

        userService.getUserByUsername(user.getUsername());

        verify(userRepository, times(1)).getUserByUsername(user.getUsername());
    }

    @Test
    public void userByUsername_Should_Call_Repository() {
        userService.userByUsername(Mockito.anyString());

        verify(userRepository, times(1)).userByUsername(Mockito.anyString());
    }
}
