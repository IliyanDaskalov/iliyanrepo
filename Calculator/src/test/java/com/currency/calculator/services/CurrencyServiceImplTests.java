package com.currency.calculator.services;

import com.currency.calculator.exceptions.DuplicateEntityException;
import com.currency.calculator.models.Currency;
import com.currency.calculator.repositories.contracts.CurrencyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.currency.calculator.services.Helpers.createCurrency;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceImplTests {

    @Mock
    CurrencyRepository currencyRepository;

    @InjectMocks
    CurrencyServiceImpl currencyService;

    @Test
    public void getAllCurrencies_Should_Call_Repository() {
        currencyService.getAllCurrencies();

        verify(currencyRepository, times(1)).getAllCurrencies();
    }

    @Test
    public void getCurrencyById_Should_Call_Repository() {
        Currency currency = createCurrency();

        currencyService.getCurrencyById(Mockito.anyInt());

        verify(currencyRepository, times(1)).getCurrencyById(Mockito.anyInt());
    }

    @Test
    public void getCurrencyByCode_Should_Call_Repository() {
        Currency currency = createCurrency();

        currencyService.getCurrencyByCode(Mockito.anyString());

        verify(currencyRepository, times(1)).getCurrencyByCode(Mockito.anyString());
    }

    @Test
    public void updateCurrency_Should_Call_Repository() {
        Currency currency = createCurrency();

        currencyService.updateCurrency(currency);

        verify(currencyRepository, times(1)).updateCurrency(currency);
    }

    @Test
    public void createCurrency_Should_Throws_When_Name_Exists() {
        Currency mockCurrency = createCurrency();
        Currency newMockCurrency = createCurrency();
        newMockCurrency.setId(2);

        when(currencyRepository.nameExists(newMockCurrency.getName())).thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> currencyService.createCurrency(newMockCurrency));
    }

    @Test
    public void createCurrency_Should_Throws_When_Code_Exists() {
        Currency mockCurrency = createCurrency();
        Currency newMockCurrency = createCurrency();
        newMockCurrency.setId(2);
        newMockCurrency.setName("NewName");

        when(currencyRepository.codeExists(newMockCurrency.getCode())).thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> currencyService.createCurrency(newMockCurrency));
    }

    @Test
    public void createCurrency_Should_Call_Repository_When_Valid() {
        Currency mockCurrency = createCurrency();
        Currency newMockCurrency = createCurrency();
        newMockCurrency.setId(2);
        newMockCurrency.setName("NewName");
        newMockCurrency.setCode("CDY");

        when(currencyRepository.nameExists(newMockCurrency.getName())).thenReturn(false);
        when(currencyRepository.codeExists(newMockCurrency.getCode())).thenReturn(false);

        currencyService.createCurrency(newMockCurrency);

        verify(currencyRepository, times(1)).createCurrency(newMockCurrency);
    }
}
