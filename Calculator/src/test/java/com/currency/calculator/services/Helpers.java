package com.currency.calculator.services;

import com.currency.calculator.models.Currency;
import com.currency.calculator.models.User;

public class Helpers {

    public static User createUser(){
        User user = new User();
        user.setUsername("MockUser");
        user.setPassword("MockPass");
        return user;
    }

    public static Currency createCurrency(){
        Currency currency = new Currency();
        currency.setId(1);
        currency.setName("Canadian Dollar");
        currency.setCode("CAD");
        currency.setUnits(1);
        currency.setValueBGN(1.31848);
        currency.setValueReturn(0.758449);
        return currency;
    }
}
