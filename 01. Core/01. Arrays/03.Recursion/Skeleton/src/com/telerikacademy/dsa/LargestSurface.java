package com.telerikacademy.dsa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LargestSurface {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] input = reader.readLine().split(" ");
        int[] rowsCols = new int[2];
        rowsCols[0] = Integer.parseInt(input[0]);
        rowsCols[1] = Integer.parseInt(input[1]);
        int rows = rowsCols[0];
        int cols = rowsCols[1];
        int[][] matrix = new int[rows][cols];

        for (int row = 0; row < rows; row++) {
            String[] fill = reader.readLine().split(" ");
            for (int col = 0; col < cols; col++) {
                matrix[row][col] = Integer.parseInt(fill[col]);
            }
        }
        System.out.println(getLargestSurface(matrix));
    }

    public static int getRegionSize(int[][] matrix, int row, int column, int target) {
        if (isOutOfMaze(matrix, row, column)) return 0;
        if (matrix[row][column] == -1) {
            return 0;
        }
        if (matrix[row][column] != target) {
            return 0;
        }

        matrix[row][column] = -1;
        int size = 1;

        size += getRegionSize(matrix, row - 1, column, target);
        size += getRegionSize(matrix, row + 1, column, target);
        size += getRegionSize(matrix, row, column + 1, target);
        size += getRegionSize(matrix, row, column - 1, target);

        return size;
    }

    public static int getLargestSurface(int[][] matrix) {
        int maxRegion = 0;
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                int target = matrix[row][column];
                if (target != -1) {
                    int size = getRegionSize(matrix, row, column, target);
                    maxRegion = Math.max(size, maxRegion);
                }
            }
        }
        return maxRegion;
    }

    private static boolean isOutOfMaze(int[][] maze, int row, int col) {
        return row < 0 || row >= maze.length || col < 0 || col >= maze[0].length;
    }
}





