package com.telerikacademy.dsa;

import java.util.Scanner;

public class BunnyEars2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(calculateEars(n));
    }

    public static int calculateEars(int bunnies) {
        if (bunnies == 0) {
            return 0;
        }

        if(bunnies %2 == 0){
            return 3 + calculateEars(bunnies - 1);
        } else {
            return 2 + calculateEars(bunnies - 1);
        }

    }
}
