package com.telerikacademy.dsa;

import java.util.Scanner;

public class BunnyEars {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(calculateEars(n));
    }

    public static int calculateEars(int bunnies) {
        if (bunnies == 0) {
            return 0;
        }
        return 2 + calculateEars(bunnies - 1);
    }
}
