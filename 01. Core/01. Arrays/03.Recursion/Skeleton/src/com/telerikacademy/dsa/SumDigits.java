package com.telerikacademy.dsa;

import java.util.Scanner;

public class SumDigits {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        System.out.println(calculateSum(number));
    }

    public static int calculateSum(String input) {
        if(input.length() == 0){
            return 0;
        }

        int leftNumber = Integer.parseInt(String.valueOf(input.charAt(0)));
        String rightPart = input.substring(1);

        return leftNumber + calculateSum(rightPart);
    }
}
