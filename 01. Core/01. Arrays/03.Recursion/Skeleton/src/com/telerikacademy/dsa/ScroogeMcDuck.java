package com.telerikacademy.dsa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ScroogeMcDuck {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] input = reader.readLine().split(" ");
        int[] rowsCols = new int[2];
        rowsCols[0] = Integer.parseInt(input[0]);
        rowsCols[1] = Integer.parseInt(input[1]);
        int rows = rowsCols[0];
        int cols = rowsCols[1];
        int[][] matrix = new int[rows][cols];
        int rowStart = -1;
        int colStart = -1;


        for (int row = 0; row < rows; row++) {
            String[] fillMatrix = reader.readLine().split(" ");
            for (int col = 0; col < cols; col++) {
                matrix[row][col] = Integer.parseInt(fillMatrix[col]);
                if (matrix[row][col] == 0) {
                    rowStart = row;
                    colStart = col;
                }
            }
        }

        System.out.println(countCoins(matrix, rowStart, colStart, 0));
    }

    private static int countCoins(int[][] maze, int row, int col, int count) {

        if (isOutOfMaze(maze, row, col)) return count;

        if ((isOutOfMaze(maze, row, col - 1) || maze[row][col - 1] == 0) && (isOutOfMaze(maze, row, col + 1) || maze[row][col + 1] == 0)
                && (isOutOfMaze(maze, row - 1, col) || maze[row - 1][col] == 0) && (isOutOfMaze(maze, row + 1, col) || maze[row + 1][col] == 0))
        {
            return count;
        }

        int temp = -1;
        String direction = "";

        if (!isOutOfMaze(maze, row, col - 1)) {
            temp = maze[row][col - 1];
            direction = "left";
        }
        if (!isOutOfMaze(maze, row, col + 1)) {
            if (temp < maze[row][col + 1]) {
                temp = maze[row][col + 1];
                direction = "right";
            }
        }
        if (!isOutOfMaze(maze, row - 1, col)) {
            if (temp < maze[row - 1][col]) {
                temp = maze[row - 1][col];
                direction = "up";
            }
        }
        if (!isOutOfMaze(maze, row + 1, col)) {
            if (temp < maze[row + 1][col]) {
                temp = maze[row + 1][col];
                direction = "down";
            }
        }

        switch (direction) {
            case "left":
                if (maze[row][col - 1] > 0) {
                    maze[row][col - 1] = maze[row][col - 1] - 1;
                }
                return countCoins(maze, row, col - 1, count + 1);
            case "right":
                if (maze[row][col + 1] > 0) {
                    maze[row][col + 1] = maze[row][col + 1] - 1;
                }
                return countCoins(maze, row, col + 1, count + 1);
            case "up":
                if (maze[row - 1][col] > 0) {
                    maze[row - 1][col] = maze[row - 1][col] - 1;
                }
                return countCoins(maze, row - 1, col, count + 1);
            case "down":
                if (maze[row + 1][col] > 0) {
                    maze[row + 1][col] = maze[row + 1][col] - 1;
                }
                return countCoins(maze, row + 1, col, count + 1);
            default:
                throw new IllegalArgumentException();
        }
    }

    private static boolean isOutOfMaze(int[][] maze, int row, int col) {
        return row < 0 || row >= maze.length || col < 0 || col >= maze[0].length;
    }

}
