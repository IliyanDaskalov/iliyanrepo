package com.telerikacademy.dsa;

import java.util.Scanner;

public class ArrayValuesTimes10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");
        int[] numbers = new int[input.length];
        int index = scanner.nextInt();

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(input[i]);
        }
        System.out.println(isTenTimes(numbers, index));
    }

    public static boolean isTenTimes(int[] numbers, int index) {
        if (index >= numbers.length - 1) {
            return false;
        }

        if (numbers[index] * 10 == numbers[index + 1]) {
            return true;
        }
        return isTenTimes(numbers, index + 1);
    }
}

