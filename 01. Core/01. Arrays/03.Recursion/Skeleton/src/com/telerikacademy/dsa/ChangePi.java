package com.telerikacademy.dsa;

import java.util.Scanner;

public class ChangePi {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(replacePi(input));
    }

    public static String replacePi(String input) {
        if (input.length() == 0) {
            return "0";
        }

        if (input.contains("pi")) {
            input = input.replaceFirst("pi", "3.14");
            return replacePi(input);
        } else {
            return input;
        }
    }
}