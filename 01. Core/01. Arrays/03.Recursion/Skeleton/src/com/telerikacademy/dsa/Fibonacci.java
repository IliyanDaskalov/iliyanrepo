package com.telerikacademy.dsa;

import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        long[] memo = new long[n + 1];
        System.out.println(fibonacci(memo, n));
    }

    public static long fibonacci(long[] memory, int number) {
        if (memory[number] == 0) {
            if (number == 0) {
                return 0;
            }
            if (number < 2) {
                return 1;
            }
            memory[number] = fibonacci(memory, number - 1) + fibonacci(memory, number - 2);
        }

        return memory[number];

    }
}
