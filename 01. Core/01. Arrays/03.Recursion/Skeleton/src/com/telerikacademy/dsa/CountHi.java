package com.telerikacademy.dsa;

import java.util.Scanner;

public class CountHi {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(count(input));
    }

    public static int count(String input) {
        if (input.length() == 0) {
            return 0;
        }

        int counter = 0;
        if(input.contains("hi")){
            counter++;
            String replacedInput = input.replaceFirst("hi","");
            return counter + count(replacedInput);
        }

        return counter;
    }
}

