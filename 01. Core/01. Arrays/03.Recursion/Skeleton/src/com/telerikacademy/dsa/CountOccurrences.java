package com.telerikacademy.dsa;

import java.util.Scanner;

public class CountOccurrences {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        System.out.println(count(number));
    }

    public static int count(String input) {
        if (input.length() == 0) {
            return 0;
        }

        int leftNumber = Integer.parseInt(String.valueOf(input.charAt(0)));
        String rightPart = input.substring(1);
        int counter = 0;
        if (leftNumber == 7) {
            counter++;

        }
        return counter + count(rightPart);
    }
}
