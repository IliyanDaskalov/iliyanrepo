package com.telerikacademy.dsa;

import java.util.Scanner;

public class PowerN {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int pow = scanner.nextInt();
        System.out.println(power(number,pow));
    }

    public static int power(int number, int pow) {
        if(pow == 0){
            return 1;
        }
        if(pow == 1){
            return number;
        }

      return number * power(number,pow-1);
    }
}

