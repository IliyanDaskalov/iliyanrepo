package com.telerikacademy.dsa;

import java.util.Scanner;

public class CountX {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        System.out.println(count(input));
    }

    public static int count(String input) {
        if (input.length() == 0) {
            return 0;
        }

        char leftChar = input.charAt(0);
        String rightPart = input.substring(1);
        int counter = 0;
        if (leftChar == 'x') {
            counter++;

        }
        return counter + count(rightPart);
    }
}
