package com.telerikacademy.dsa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Variations {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int length = Integer.parseInt(reader.readLine());
        String[] input = reader.readLine().split(" ");
        String[] symbols = new String[2];
        symbols[0] = input[1];
        symbols[1] = input[0];
        int a = symbols[0].charAt(0);
        int b = symbols[1].charAt(0);
        if(a < b){
            symbols[0] = input[1];
            symbols[1] = input[0];
        } else {
            symbols[0] = input[0];
            symbols[1] = input[1];
        }

        printAllVariations(symbols, length);

    }

    private static void printAllVariations(String[] symbols, int length) {
        printAllPossibleVariations(symbols, "",  length);
    }
    private static void printAllPossibleVariations(String[] symbols, String oldPart, int length) {
        if (length == 0) {
            System.out.println(oldPart);
            return;
        }

        for (int i = 0; i < 2; ++i) {
            String newPart = oldPart + symbols[i];
            printAllPossibleVariations(symbols, newPart,  length - 1);
        }
    }
}
