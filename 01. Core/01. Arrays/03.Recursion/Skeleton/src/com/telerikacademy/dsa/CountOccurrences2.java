package com.telerikacademy.dsa;

import java.util.Scanner;

public class CountOccurrences2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String number = scanner.nextLine();
        System.out.println(count(number));
    }

    public static int count(String input) {
        if (input.length() == 0) {
            return 0;
        }
        int counter = 0;
        int leftNumber = Integer.parseInt(String.valueOf(input.charAt(0)));
        String rightPart = input.substring(1);

        if(rightPart.length() > 0){
            int nextNumber = Integer.parseInt(String.valueOf(input.charAt(1)));
            if (leftNumber == 8 && nextNumber == 8) {
                counter += 2;
            } else if(leftNumber == 8){
                counter++;
            }
        } else {
            if(leftNumber == 8) {
                counter++;
            }
        }

        return counter + count(rightPart);
    }
}

