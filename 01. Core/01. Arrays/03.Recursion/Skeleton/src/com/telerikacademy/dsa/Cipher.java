package com.telerikacademy.dsa;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Cipher {
    public static void main(String[] args) throws IOException {
        Map<Character, String> myMap = new HashMap<>();
        List<String> cipher = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String numbers = reader.readLine();
        String code = reader.readLine();
        printCipherSorted(code, numbers, myMap, cipher);

    }

    private static void printCipherSorted(String code, String numbers, Map<Character, String> myMap, List<String> cipher) {
        parseToMap(code, myMap);
        searchCipher(numbers, myMap, cipher);
        cipher.sort(Comparator.naturalOrder());
        System.out.println(cipher.size());
        System.out.print(String.join(System.lineSeparator(), cipher));
    }


    private static void parseToMap(String code, Map<Character, String> map) {
        if (code.length() < 2) {
            return;
        }
        char letter = code.charAt(0);
        StringBuilder rightPart = new StringBuilder();
        for (int i = 1; i < code.length(); i++) {
            if (!Character.isDigit(code.charAt(i))) {
                break;
            }
            rightPart.append(code.charAt(i));
        }

        map.put(letter, rightPart.toString());
        parseToMap(code.substring(rightPart.length() + 1), map);

    }

    private static void searchCipher(String numbers, Map<Character, String> map, List<String> cipher) {
        int index = -1;

        for (int i = 0; i < numbers.length(); i++) {
            if(Character.isDigit(numbers.charAt(i))){
                index = i;
                break;
            }
        }

        if (index == -1) {
            cipher.add(numbers);
            return;
        }

        String rightPart = numbers.substring(index);

        for (Map.Entry<Character, String> entry : map.entrySet()) {
            if (rightPart.startsWith(entry.getValue())) {
                searchCipher(numbers.replaceFirst(entry.getValue(), entry.getKey() + ""), map, cipher);
            }
        }
    }

}
