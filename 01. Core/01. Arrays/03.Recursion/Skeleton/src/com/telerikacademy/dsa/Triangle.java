package com.telerikacademy.dsa;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(calculateBlocks(n));
    }

    public static int calculateBlocks(int rows) {
        if (rows == 0) {
            return 0;
        }
        if(rows == 1 ){
            return 1;
        }
        return rows + calculateBlocks(rows - 1);
    }
}
