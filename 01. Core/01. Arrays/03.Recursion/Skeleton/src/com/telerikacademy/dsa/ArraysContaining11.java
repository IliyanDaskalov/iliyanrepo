package com.telerikacademy.dsa;

import java.util.Scanner;

public class ArraysContaining11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.nextLine().split(",");
        int[] numbers = new int[input.length];
        int index = scanner.nextInt();

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(input[i]);
        }
        System.out.println(count11inArray(numbers, index));
    }

    public static int count11inArray(int[] numbers, int index) {
        if (index > numbers.length - 1) {
            return 0;
        }
        int counter = 0;
        if (numbers[index] == 11) {
            counter++;
        }
        return counter + count11inArray(numbers, index + 1);
    }
}

