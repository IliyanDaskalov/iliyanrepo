<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Files Utils

## 1. Directory Traversal

Write a recursive method `void traverseDirectories(String path)` which prints the names of all files in the given `path`  

### Example usage:

```java
traverseDirectores("C:\\Images\\");
```

### Possible output:

```
Images:
  img1.jpg
  img2.jpg
  Album 1:
    img3.png
    img4.jpg
    Album 1.1
      img5.jpg
      img6.png
  Album 2:
    img7.jpg
```

## 2. Find Files

Write method `List<String> findFiles(String path, String extension)` which return a collection of all files which have the given `extension` in the given directory.

### Example usage:  

```java
List<String> files = findFilesDFS("C:\\Images\\", ".jpg");
```

### Possible output: (if you use the directory structure from **Task 1**)

```
[ img1.jpg, img2.jpg, img4.jpg, img5.jpg, img7.jpg ]
```

### 3. File Exists 

Write a recursive method `boolean fileExists(String path, String fileName)`

### Example usage:

```java
boolean exists = fileExists("C:\\Images\\", "img6.png");
```

### Output: (if you use the directory structure from **Task 1**)

```
true
```

### 4. Directory Stats

Write a recursive method `Map<String, Integer> getDirectoryStats(String path)` which returns the number of files for each file extension.

### Example usage:

```java
Map<String, Integer> stats = getDirectoryStats("C:\\Images\\");
```

### Output: (if you use the directory structure from **Task 1**)

```
[
    { ".jpg", 6 },
    { ".png", 1 }
]
```