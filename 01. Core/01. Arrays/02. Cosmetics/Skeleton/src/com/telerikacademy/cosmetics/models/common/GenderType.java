package com.telerikacademy.cosmetics.models.common;

public enum GenderType {
    MEN,
    WOMEN,
    UNISEX;

    @Override
    public String toString() {
        switch (this) {
            case WOMEN:
                return "Women";
            case UNISEX:
                return "Unisex";
            case MEN:
                return "Men";
            default:
                throw new IllegalArgumentException();
        }
    }
}
