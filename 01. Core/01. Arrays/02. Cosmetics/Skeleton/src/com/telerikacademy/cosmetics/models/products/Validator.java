package com.telerikacademy.cosmetics.models.products;

public class Validator {

    public static void validateLength(String input, int min, int max, String exception) {
        String trimmedInput = input.trim();

        if (trimmedInput.length() < min
                || trimmedInput.length() > max)
            throw new IllegalArgumentException(exception);
    }

    public static void validateNotNull(String input, Object obj) {
        if (obj == null)
            throw new IllegalArgumentException(input + " cannot be null!");
    }


    public static void validateMMNonNegative(String input, int mm) {
        if (mm < 0) {
            throw new IllegalArgumentException(input + " cannot be negative!");
        }
    }


    public static void validatePrice(String input, double price,boolean canBeFree) {
        if(canBeFree){
            if (price < 0) {
                throw new IllegalArgumentException(input + " cannot be negative!");
            }
        } else {
            if (price <= 0) {
                throw new IllegalArgumentException(input + " should be more than 0");
            }
        }

    }
}
