package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public abstract class ProductBase implements Product {

    private static final String PRODUCT_NAME_INVALID_LENGTH_MESSAGE = "%s name length should be between %d and %d symbols.";
    private static final String PRODUCT_BRAND_INVALID_LENGTH_MESSAGE = "%s brand length should be between %d and %d symbols.";

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }


    private void setName(String name) {
        Validator.validateNotNull("Name", name);
        Validator.validateLength(name,
                minName(),
                maxName(),
                String.format(PRODUCT_NAME_INVALID_LENGTH_MESSAGE, getClass().getSimpleName(),minName(), maxName()));
        this.name = name;
    }

    private void setBrand(String brand) {
        Validator.validateNotNull("Brand", brand);
        Validator.validateLength(brand,
                minBrandName(),
                maxBrandName(),
                String.format(PRODUCT_BRAND_INVALID_LENGTH_MESSAGE, getClass().getSimpleName(), minBrandName(), maxBrandName()));
        this.brand = brand;
    }

    private void setPrice(double price) {
        Validator.validatePrice("Price", price, canProductBeFree());
        this.price = price;
    }

    private void setGender(GenderType gender) {
        Validator.validateNotNull("Gender", gender);
        this.gender = gender;
    }

    protected abstract int minName();

    protected abstract int maxName();

    protected abstract int minBrandName();

    protected abstract int maxBrandName();

    protected abstract boolean canProductBeFree();

    @Override
    public String print() {
        String result = String.format("#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s",
                name,
                brand,
                price,
                gender.toString());
        return result;
    }

}
