package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

public class ShampooImpl extends ProductBase implements Shampoo {

    private static final int SHAMPOO_MIN_NAME_LENGTH = 3;
    private static final int SHAMPOO_MAX_NAME_LENGTH = 10;
    private static final int SHAMPOO_MIN_BRAND_NAME_LENGTH = 2;
    private static final int SHAMPOO_MAX_BRAND_NAME_LENGTH = 10;

    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        setUsage(usage);

    }

    private void setMilliliters(int milliliters) {
        Validator.validateMMNonNegative("Milliliters", milliliters);
        this.milliliters = milliliters;
    }


    private void setUsage(UsageType usage) {
        Validator.validateNotNull("Usage", usage);
        this.usage = usage;
    }

    @Override
    protected int minName() {
        return SHAMPOO_MIN_NAME_LENGTH;
    }

    @Override
    protected int maxName() {
        return SHAMPOO_MAX_NAME_LENGTH;
    }

    @Override
    protected int minBrandName() {
        return SHAMPOO_MIN_BRAND_NAME_LENGTH;
    }

    @Override
    protected int maxBrandName() {
        return SHAMPOO_MAX_BRAND_NAME_LENGTH;
    }

    @Override
    protected boolean canProductBeFree() {
        return true;
    }

    @Override
    public int getMillilitres() {
        return milliliters;
    }

    @Override
    public UsageType getUsageType() {
        return usage;
    }

    @Override
    public String print() {
        String result = String.format("%s%n"+
                        " #Milliliters: %d%n"+
                        " #Usage: %s%n",
                super.print(),
                milliliters,
                usage.toString()
        );

        return result;
    }
}
