package com.telerikacademy.cosmetics;

import com.telerikacademy.cosmetics.core.CosmeticsEngine;

public class Startup {

    public static void main(String[] args) {
        CosmeticsEngine engine = new CosmeticsEngine();
        engine.start();
    }
}
/*

CreateCream MyMan Nivea 10 Men Rose
CreateToothpaste White Colgate 0 Men calcium,fluorid
CreateCategory Cream
CreateCategory Toothpastes
AddToCategory Cream MyMan
AddToCategory Toothpastes White
AddToShoppingCart MyMan
AddToShoppingCart White
ShowCategory Cream
ShowCategory Toothpastes
TotalPrice
RemoveFromCategory Cream MyMan
ShowCategory Cream
RemoveFromShoppingCart MyMan
TotalPrice
Exit

 */