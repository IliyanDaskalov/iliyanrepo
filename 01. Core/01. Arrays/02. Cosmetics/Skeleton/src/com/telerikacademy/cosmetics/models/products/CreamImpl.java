package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;

public class CreamImpl extends ProductBase implements Cream {

    private static final int CREAM_NAME_MIN_LENGTH = 3;
    private static final int CREAM_NAME_MAX_LENGTH = 15;
    private static final int CREAM_BRAND_NAME_MIN_LENGTH = 3;
    private static final int CREAM_BRAND_NAME_MAX_LENGTH = 15;
    private ScentType scent;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        setScent(scent);
    }

    private void setScent(ScentType scent) {
        Validator.validateNotNull("Scent",scent);
        this.scent = scent;
    }

    @Override
    public ScentType getScentType() {
        return scent;
    }

    @Override
    protected int minName() {
        return CREAM_NAME_MIN_LENGTH;
    }

    @Override
    protected int maxName() {
        return CREAM_NAME_MAX_LENGTH;
    }

    @Override
    protected int minBrandName() {
        return CREAM_BRAND_NAME_MIN_LENGTH;
    }

    @Override
    protected int maxBrandName() {
        return CREAM_BRAND_NAME_MAX_LENGTH;
    }

    @Override
    protected boolean canProductBeFree() {
        return false;
    }

    @Override
    public String print() {
        return String.format("%s%n #Scent: %s%n",
                super.print(),
                scent.toString());
    }
}
