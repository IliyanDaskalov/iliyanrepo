package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste{

    private static final int TOOTHPASTE_MIN_NAME_LENGTH = 3;
    private static final int TOOTHPASTE_MAX_NAME_LENGTH = 10;
    private static final int TOOTHPASTE_MIN_BRAND_NAME_LENGTH = 2;
    private static final int TOOTHPASTE_MAX_BRAND_NAME_LENGTH = 10;

    private List<String> ingredients;
    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);
        setIngredients(ingredients);

    }

    private void setIngredients(List<String> ingredients) {
        Validator.validateNotNull("Ingredients",ingredients);
        this.ingredients = new ArrayList<>(ingredients);
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    @Override
    protected int minName() {
        return TOOTHPASTE_MIN_NAME_LENGTH;
    }

    @Override
    protected int maxName() {
        return TOOTHPASTE_MAX_NAME_LENGTH;
    }

    @Override
    protected int minBrandName() {
        return TOOTHPASTE_MIN_BRAND_NAME_LENGTH;
    }

    @Override
    protected int maxBrandName() {
        return TOOTHPASTE_MAX_BRAND_NAME_LENGTH;
    }

    @Override
    protected boolean canProductBeFree() {
        return true;
    }

    @Override
    public String print() {
        String start = String.format("%s%n"+
                        " #Ingredients: [",
                super.print());


        StringBuilder result = new StringBuilder(start);
        for (String ingredient : ingredients) {
            result.append(ingredient);
            result.append(", ");
        }
        result.replace(result.length()-2,result.length(),"]");
        result.append(System.lineSeparator());
        return result.toString();
    }
}
