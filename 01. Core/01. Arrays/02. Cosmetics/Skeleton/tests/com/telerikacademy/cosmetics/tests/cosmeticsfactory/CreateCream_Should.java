package com.telerikacademy.cosmetics.tests.cosmeticsfactory;

import com.telerikacademy.cosmetics.commands.CreateCream;
import com.telerikacademy.cosmetics.core.CosmeticsRepositoryImpl;
import com.telerikacademy.cosmetics.core.contracts.Command;
import com.telerikacademy.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.cosmetics.core.factories.CosmeticsFactoryImpl;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;
import com.telerikacademy.cosmetics.models.contracts.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class CreateCream_Should {

    private Command testCommand;
    private Product testProduct;
    private CosmeticsRepository cosmeticsRepository;
    private CosmeticsFactory cosmeticsFactory;

    @BeforeEach
    public void before(){
        cosmeticsFactory = new CosmeticsFactoryImpl();
        cosmeticsRepository = new CosmeticsRepositoryImpl();
        testCommand = new CreateCream(cosmeticsRepository, cosmeticsFactory);
    }

    @Test
    public void returnInstanceOfTypeProduct(){

        Cream cream = cosmeticsFactory.createCream("name","brand",10, GenderType.MEN, ScentType.ROSE);

        Assertions.assertTrue(cream instanceof Product);
    }


}
