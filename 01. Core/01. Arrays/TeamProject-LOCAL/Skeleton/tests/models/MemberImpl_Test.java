package models;

import com.telerikacademy.oop.workitemmanagement.models.FeedbackImpl;
import com.telerikacademy.oop.workitemmanagement.models.MemberImpl;
import com.telerikacademy.oop.workitemmanagement.models.TeamImpl;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MemberImpl_Test {
    private Member member;

    @BeforeEach
    private void doBefore() {
        member = new MemberImpl("member");
    }

    @Test
    public void constructor_Should_CreateMember_When_ArgumentsAreValid(){

    }

    @Test
    public void constructor_Should_ThrowException_When_NameLengthLessThanMinValue(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> new TeamImpl("12"));
    }

    @Test
    public void constructor_Should_ThrowException_When_NameLengthMoreThanMaxValue(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> new TeamImpl("1234585839485039489"));
    }

    @Test
    public void constructor_Should_ThrowException_When_GivenNull(){
        Assertions.assertThrows(NullPointerException.class, () -> new TeamImpl(null));
    }

    @Test
    public void addWorkItem_Should_AddWorkItem_When_ArgumentsAreValid(){
        Feedback feedback = new FeedbackImpl("feedbackwithmorethan10charachters","shortDescription",10);
        member.addWorkItem(feedback);
    }

    @Test
    public void addWorkItem_Should_ThrowException_When_WorkItemIsNull(){
        Feedback feedback = null;

        Assertions.assertThrows(NullPointerException.class , () -> member.addWorkItem(feedback));
    }

    @Test
    public void addWorkItem_Should_ThrowException_When_WorkItemAlreadyExist(){
        Feedback feedback = new FeedbackImpl("feedbackwithmorethan10charachters","shortDescription",10);
        member.addWorkItem(feedback);

        Assertions.assertThrows(IllegalArgumentException.class, () -> member.addWorkItem(feedback));
    }

    @Test
    public void addActivity_Should_AddActivity_When_ArgumentsAreValid(){
        String validData = "valid Data";

        member.addActivity(validData);
    }

    @Test
    public void addActivity_Should_ThrowException_When_GivenNull(){
        Assertions.assertThrows(NullPointerException.class, ()->member.addActivity(null));
    }




}
