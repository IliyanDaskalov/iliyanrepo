package models;
import com.telerikacademy.oop.workitemmanagement.models.BoardImpl;
import com.telerikacademy.oop.workitemmanagement.models.MemberImpl;
import com.telerikacademy.oop.workitemmanagement.models.TeamImpl;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Board;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TeamImpl_Test {

    private Team team;

    @BeforeEach
    private void doBefore(){
        team = new TeamImpl("teamName");
    }

    @Test
    public void constructor_Should_CreateTeam_When_ArgumentsAreValid(){

    }

    @Test
    public void constructor_Should_ThrowException_When_NameLengthLessThanMinValue(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> new TeamImpl("12"));
    }

    @Test
    public void constructor_Should_ThrowException_When_NameLengthMoreThanMaxValue(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> new TeamImpl("1234585839485039489"));
    }

    @Test
    public void constructor_Should_ThrowException_When_GivenNull(){
        Assertions.assertThrows(NullPointerException.class, () -> new TeamImpl(null));
    }

    @Test
    public void addMemberToTeam_Should_ThrowException_When_MemberIsNull(){
        Assertions.assertThrows(NullPointerException.class,() -> team.addMemberToTeam(null));
    }

    @Test
    public void addMemberToTeam_Should_AddMember_When_ArgumentsAreValid(){
        Member member = new MemberImpl("member");

        team.addMemberToTeam(member);

    }

    @Test
    public void addMemberToList_Should_ThrowException_When_MemberAlreadyExist(){
        Member member = new MemberImpl("member");
        team.addMemberToTeam(member);

        Assertions.assertThrows(IllegalArgumentException.class, () -> team.addMemberToTeam(member));
    }

    @Test
    public void removeMemberFromList_Should_RemoveMember_When_ArgumentsAreValid(){
        Member member = new MemberImpl("member");
        team.addMemberToTeam(member);

        team.removeMemberFromTeam(member);
    }

    @Test
    public void removeMemberFromList_Should_ThrowException_When_MemberIsNull(){
        Assertions.assertThrows(NullPointerException.class,() -> team.removeMemberFromTeam(null));
    }

    @Test
    public void removeMemberFromList_Should_ThrowException_When_MemberNotExist(){
        Member member = new MemberImpl("member");

        Assertions.assertThrows(IllegalArgumentException.class,() -> team.removeMemberFromTeam(member));
    }

    @Test
    public void createBoard_Should_CreateBoard_When_ValidArguments(){
        Board board = new BoardImpl("BoardName");

        team.createBoard(board);
    }

    @Test
    public void createBoard_Should_ThrowException_When_BoardIsNull(){

        Assertions.assertThrows(NullPointerException.class, () -> team.createBoard(null));
    }

    @Test
    public void createBoard_Should_ThrowException_When_BoardAlreadyExist(){
        Board board = new BoardImpl("BoardName");
        team.createBoard(board);

        Assertions.assertThrows(IllegalArgumentException.class, () -> team.createBoard(board));
    }







}
