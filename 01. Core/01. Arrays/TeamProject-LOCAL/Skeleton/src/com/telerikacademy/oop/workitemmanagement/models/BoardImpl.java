package com.telerikacademy.oop.workitemmanagement.models;

import com.telerikacademy.oop.workitemmanagement.models.contracts.Activity;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Board;
import com.telerikacademy.oop.workitemmanagement.models.contracts.WorkItem;
import com.telerikacademy.oop.workitemmanagement.models.utils.ModelConstants;
import com.telerikacademy.oop.workitemmanagement.models.utils.Validator;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    private static final int BOARD_NAME_MIN_LENGTH = 5;
    private static final int BOARD_NAME_MAX_LENGTH = 15;

    private String name;
    private final List<WorkItem> workItemList;
    private final List<Activity> activityHistoryList;


    public BoardImpl(String name) {
        setName(name);
        workItemList = new ArrayList<>();
        activityHistoryList = new ArrayList<>();
        addActivity(String.format("Board %s was created", getName()));

    }

    private void setName(String name) {
        Validator.validateNotNull(name, "Board name");
        Validator.validateLength(name, BOARD_NAME_MIN_LENGTH, BOARD_NAME_MAX_LENGTH, "Board name");
        this.name = name;
    }


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<WorkItem> getWorkItemsList() {
        return new ArrayList<>(workItemList);
    }

    @Override
    public List<Activity> getActivityHistory() {
        return new ArrayList<>(activityHistoryList);
    }

    @Override
    public void addWorkItem(WorkItem workItem) {
        Validator.validateNotNull(workItem, "Work item");
        if (workItemList.contains(workItem)) {
            throw new IllegalArgumentException(String.format(ModelConstants.WORKITEM_EXIST_ERROR, workItem.getTitle()));
        }
        workItemList.add(workItem);
        addActivity(String.format("%s with name %s created!",
                workItem.getClass().getSimpleName().replace("Impl", ""),
                workItem.getTitle()));
    }

    @Override
    public void addActivity(String activity) {
        Validator.validateNotNull(activity, "Activity");
        Activity newActivity = new ActivityImpl(activity);
        activityHistoryList.add(newActivity);
    }
}
