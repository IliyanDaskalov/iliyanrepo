package com.telerikacademy.oop.workitemmanagement.models.enums;

public enum FeedbackStatus {
    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE;
}
