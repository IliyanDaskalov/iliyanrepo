package com.telerikacademy.oop.workitemmanagement.models.contracts;

import java.util.List;

public interface WorkItem {

    String getTitle();

    String getDescription();

    List<Comment> getCommentsList();

    void addComment(Comment comment);

    List<Activity> getHistoryList();

    void addActivity(String activity);

    String getID();

}
