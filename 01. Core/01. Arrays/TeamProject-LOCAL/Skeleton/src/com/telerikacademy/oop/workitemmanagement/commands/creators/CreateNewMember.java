package com.telerikacademy.oop.workitemmanagement.commands.creators;

import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;

import java.util.List;

import static com.telerikacademy.oop.workitemmanagement.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class CreateNewMember implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementFactory managementFactory;
    private final ManagementRepository managementRepository;


    private String name;

    public CreateNewMember(ManagementFactory managementFactory, ManagementRepository managementRepository) {
        this.managementFactory = managementFactory;
        this.managementRepository = managementRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        Member member = managementFactory.createMember(name);
        managementRepository.addMember(name, member);

        return String.format("Member with name %s was created.", name);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(
                    String.format(
                            INVALID_NUMBER_OF_ARGUMENTS,
                            EXPECTED_NUMBER_OF_ARGUMENTS,
                            parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            name = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateNewMember command parameters.");
        }
    }
}
