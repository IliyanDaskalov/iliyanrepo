package com.telerikacademy.oop.workitemmanagement.models;

import com.telerikacademy.oop.workitemmanagement.models.contracts.Activity;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;
import com.telerikacademy.oop.workitemmanagement.models.contracts.WorkItem;
import com.telerikacademy.oop.workitemmanagement.models.utils.ModelConstants;
import com.telerikacademy.oop.workitemmanagement.models.utils.Validator;

import java.util.ArrayList;
import java.util.List;

public class MemberImpl implements Member {

    private static final int MEMBER_NAME_MIN_LENGTH = 5;
    private static final int MEMBER_NAME_MAX_LENGTH = 15;

    private String name;
    private final List<WorkItem> workItemList;
    private final List<Activity> activityHistoryList;

    public MemberImpl(String name) {
        setName(name);
        workItemList = new ArrayList<>();
        activityHistoryList = new ArrayList<>();
        addActivity(String.format("Member %s was created", getName()));
        }

    private void setName(String name) {
        Validator.validateNotNull(name, "Member name");
        Validator.validateLength(name, MEMBER_NAME_MIN_LENGTH, MEMBER_NAME_MAX_LENGTH, "Member name");
        this.name = name;
    }


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<WorkItem> getWorkItemsList() {
        return new ArrayList<>(workItemList);
    }

    @Override
    public List<Activity> getActivityHistory() {
        return new ArrayList<>(activityHistoryList);
    }

    @Override
    public void addWorkItem(WorkItem workItem) {
        Validator.validateNotNull(workItem, "Work item");
        if (workItemList.contains(workItem)) {
            throw new IllegalArgumentException(String.format(ModelConstants.WORKITEM_EXIST_ERROR, workItem.getTitle()));
        }
        workItemList.add(workItem);
        addActivity(String.format("%s with name %s assigned to %s",
                workItem.getClass().getSimpleName().replace("Impl", ""),
                workItem.getTitle(),
                getName()));
    }

    @Override
    public void removeWorkItem(WorkItem workItem) {
        Validator.validateNotNull(workItem,"Work Item");
        if (!workItemList.contains(workItem)) {
            throw new IllegalArgumentException(String.format("No work item %s in %s workItemList", workItem.getTitle(),getName()));
        }
        workItemList.remove(workItem);
        addActivity(String.format("%s with name %s unassigned from %s",
                workItem.getClass().getSimpleName().replace("Impl", ""),
                workItem.getTitle(),
                getName()));
    }

    @Override
    public void addActivity(String activity) {
        Validator.validateNotNull(activity, "Activity");
        Activity newActivity = new ActivityImpl(activity);
        activityHistoryList.add(newActivity);

    }
}
