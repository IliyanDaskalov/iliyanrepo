package com.telerikacademy.oop.workitemmanagement.models.contracts;

import java.util.List;

public interface Board {

    String getName();

    List<WorkItem> getWorkItemsList();

    List<Activity> getActivityHistory();

    void addWorkItem(WorkItem workItem);

    void addActivity(String activity);
}
