package com.telerikacademy.oop.workitemmanagement.commands.creators;

import com.telerikacademy.oop.workitemmanagement.commands.CommandConstants;
import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.*;

import java.util.List;
import java.util.Optional;

public class CreateNewFeedbackInABoard implements Command {

    private final static int CORRECT_NUMBER_OF_ARGUMENTS = 5;

    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    private String title;
    private String description;
    private int rating;
    private Board board;
    private Team team;


    public CreateNewFeedbackInABoard(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }


    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
        parseParameters(parameters);
        if(doesFeedbackExist(title)){
            throw new IllegalArgumentException(String.format("Feedback with name '%s' already exist in board %s", title,board.getName()));
        }

        Feedback feedback = managementFactory.createFeedback(title,description,rating);
        board.addWorkItem(feedback);
        return String.format("Feedback %s was created on team %s's board %s",title,team.getName(),board.getName());

    }

    private void parseParameters(List<String> parameters) {
        try {
            title = parameters.get(0);
            description = parameters.get(1);
            rating = Integer.parseInt(parameters.get(2));
            findTeam(parameters.get(3));
            findBoard(parameters.get(4));


        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateNewFeedbackInABoard command parameters.");
        }
    }


    private void findTeam(String team) {
        if (!managementRepository.getTeams().containsKey(team)) {
            throw new IllegalArgumentException(String.format("%s, does not exist", team));
        }

        this.team = managementRepository.getTeams().get(team);
    }

    private void findBoard(String board) {
        Optional<Board> temp = this.team.getBoardsList().stream()
                .filter(board1 -> board1.getName().equals(board))
                .findFirst();
        if (temp.isPresent()) {
            this.board = temp.get();
        } else {
            throw new IllegalArgumentException(String.format("There is no %s board in team %s", board, this.team.getName()));
        }


    }

    private boolean doesFeedbackExist(String title){
        Optional<WorkItem> temp = this.board.getWorkItemsList().stream()
                .filter(workItem -> workItem.getTitle().equals(title))
                .findFirst();
        return temp.isPresent();
    }
}
