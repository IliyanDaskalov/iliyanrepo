package com.telerikacademy.oop.workitemmanagement.core.contracts;


import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Team;
import com.telerikacademy.oop.workitemmanagement.models.contracts.WorkItem;

import java.util.Map;

public interface ManagementRepository {

    Map<String, Team> getTeams();

    Map<String, Member> getMembers();

    Map<String, WorkItem> getWorkItem();

    void addTeam(String name, Team team);

    void addMember(String name, Member member);


    
}
