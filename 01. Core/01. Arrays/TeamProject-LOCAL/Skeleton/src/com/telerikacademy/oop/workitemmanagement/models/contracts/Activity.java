package com.telerikacademy.oop.workitemmanagement.models.contracts;

public interface Activity {

    String toString();
}
