package com.telerikacademy.oop.workitemmanagement.commands.changers;

import com.telerikacademy.oop.workitemmanagement.commands.CommandConstants;
import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.*;

import java.util.List;

public class LowerBugPriority implements Command {

    private final static int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    private WorkItem workItem;
    private Bug bug;

    public LowerBugPriority(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
        parseParameters(parameters);
//        Bug bug = managementFactory.(comment, member);
//        workItem.addComment(newComment);
        bug.lowerPriority();
        return String.format("Now comment added to %s", workItem.getTitle());


    }

    private void parseParameters(List<String> parameters) {
        try {
            getBug(parameters.get(0));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse LowerBugPriority command parameters.");
        }
    }

    private void getBug(String bugName) {
        if (managementRepository.getWorkItem().containsKey(bugName)) {
            this.workItem = managementRepository.getWorkItem().get(bugName);
        } else {
            throw new IllegalArgumentException(String.format("No bug %s exist", bugName));
        }
    }


}
