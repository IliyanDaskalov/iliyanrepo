package com.telerikacademy.oop.workitemmanagement.commands.creators;

import com.telerikacademy.oop.workitemmanagement.commands.CommandConstants;
import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.*;

import java.util.List;
import java.util.Optional;

public class CreateNewBugInABoard implements Command {

    private final static int CORRECT_NUMBER_OF_ARGUMENTS = 6;

    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    private String title;
    private String description;
    private String severity;
    private String priority;
    private Board board;
    private Team team;


    public CreateNewBugInABoard(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }


    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
        parseParameters(parameters);
        if (doesStoryExist(title)) {
            throw new IllegalArgumentException(String.format("Bug with name '%s' already exist in board %s", title, board.getName()));
        }

        Bug bug = managementFactory.createBug(title, description, severity, priority);
        board.addWorkItem(bug);
        return String.format("Bug %s was created on team %s's board %s", title, team.getName(), board.getName());

    }

    private void parseParameters(List<String> parameters) {
        try {
            title = parameters.get(0);
            description = parameters.get(1);
            severity = parameters.get(2);
            priority = parameters.get(3);
            findTeam(parameters.get(4));
            findBoard(parameters.get(5));


        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateNewBugInABoard command parameters.");
        }
    }


    private void findTeam(String team) {
        if (!managementRepository.getTeams().containsKey(team)) {
            throw new IllegalArgumentException(String.format("%s, does not exist", team));
        }

        this.team = managementRepository.getTeams().get(team);
    }

    private void findBoard(String board) {
        Optional<Board> temp = this.team.getBoardsList().stream()
                .filter(board1 -> board1.getName().equals(board))
                .findFirst();
        if (temp.isPresent()) {
            this.board = temp.get();
        } else {
            throw new IllegalArgumentException(String.format("There is no %s board in team %s", board, this.team.getName()));
        }


    }

    private boolean doesStoryExist(String title) {
        Optional<WorkItem> temp = this.board.getWorkItemsList().stream()
                .filter(workItem -> workItem.getTitle().equals(title))
                .findFirst();

        return temp.isPresent();
    }

}
