package com.telerikacademy.oop.workitemmanagement.commands.creators;

import com.telerikacademy.oop.workitemmanagement.commands.CommandConstants;
import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Team;

import java.util.List;

public class CreateNewTeam implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    private String name;

    public CreateNewTeam(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }

        try {
            this.name = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateNewTeam command parameters.");
        }

        if(managementRepository.getTeams().containsKey(name)){
            throw new IllegalArgumentException(String.format("Team with name \"%s\" already exists",name));
        }

        Team team = managementFactory.createTeam(name);
        managementRepository.addTeam(name, team);
        return String.format("Team %s was created", name);

    }
}
