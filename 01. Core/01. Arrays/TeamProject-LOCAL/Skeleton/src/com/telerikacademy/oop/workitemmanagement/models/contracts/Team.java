package com.telerikacademy.oop.workitemmanagement.models.contracts;

import java.util.List;

public interface Team {

    String getName();

    List<Board> getBoardsList();

    List<Member> getMembersList();

    void addMemberToTeam(Member member);

    void removeMemberFromTeam(Member member);

    void createBoard(Board board);
}
