package com.telerikacademy.oop.workitemmanagement.core.contracts;


import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;

public interface CommandFactory {
    
    Command createCommand(String commandTypeAsString, ManagementFactory factory, ManagementRepository agencyRepository);
    
}
