package com.telerikacademy.oop.workitemmanagement.models;

import com.telerikacademy.oop.workitemmanagement.models.contracts.Assignable;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;
import com.telerikacademy.oop.workitemmanagement.models.enums.BugSeverity;
import com.telerikacademy.oop.workitemmanagement.models.enums.BugStatus;
import com.telerikacademy.oop.workitemmanagement.models.enums.Priority;
import com.telerikacademy.oop.workitemmanagement.models.utils.Validator;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends WorkItemBase implements Bug{

    private Member assignee;
    private BugSeverity severity;
    private Priority priority;
    private BugStatus status;
    private final List<String> stepsToReproduce;


    public BugImpl(String title, String description, BugSeverity severity, Priority priority) {
        super(title, description);
        this.assignee = null;
        this.severity = severity;
        this.priority = priority;
        this.status = BugStatus.ACTIVE;
        stepsToReproduce = new ArrayList<>();
        addActivity(String.format("Bug %s was created", getTitle()));

    }

    private void setSeverity(BugSeverity severity) {
        addActivity(String.format("Bug severity changed from %s to %s", getSeverity(), severity));
        this.severity = severity;
    }


    private void setPriority(Priority priority) {
        addActivity(String.format("Bug priority changed from %s to %s", getPriority(), priority));
        this.priority = priority;
    }

    public void setStatus(BugStatus status) {
        addActivity(String.format("Bug status changed from %s to %s", getStatus(), status));
        this.status = status;
    }

    @Override
    public void setAssignee(Member member) {
        Validator.validateNotNull(member, "Assignee");
        if(this.assignee != null){
            this.assignee.removeWorkItem(this);
        }
        this.assignee = member;
        addActivity(String.format("%s assigned to %s",getTitle(),getAssigneeName()));
    }

    @Override
    public Member getAssignee() {
        return this.assignee;
    }

    @Override
    public String getAssigneeName() {
        return assignee.getName();
    }

    @Override
    public BugStatus getStatus() {
        return this.status;
    }

    @Override
    public void raiseStatus() {
        if (getStatus() != BugStatus.FIXED) {
            setStatus(BugStatus.values()[getStatus().ordinal() + 1]);
        } else {
            addActivity(String.format("Bug status already at %s", getStatus()));
        }
    }

    @Override
    public void lowerStatus() {
        if (getStatus() != BugStatus.ACTIVE) {
            setStatus(BugStatus.values()[getStatus().ordinal() - 1]);
        } else {
            addActivity(String.format("Bug status already at %s", getStatus()));
        }

    }

    @Override
    public BugSeverity getSeverity() {
        return this.severity;
    }

    @Override
    public void raiseSeverity() {
        if (getSeverity() != BugSeverity.CRITICAL) {
            setSeverity(BugSeverity.values()[getSeverity().ordinal() + 1]);
        } else {
            addActivity(String.format("Bug severity already at %s", getSeverity()));
        }
    }

    @Override
    public void lowerSeverity() {
        if (getSeverity() != BugSeverity.MINOR) {
            setSeverity(BugSeverity.values()[getSeverity().ordinal() - 1]);
        } else {
            addActivity(String.format("Bug severity already at %s", getSeverity()));
        }
    }

    @Override
    public Priority getPriority() {
        return this.priority;
    }

    @Override
    public void raisePriority() {
        if (getPriority() != Priority.HIGH) {
            setPriority(Priority.values()[getPriority().ordinal() + 1]);
        } else {
            addActivity(String.format("Bug priority already at %s", getPriority()));
        }
    }

    @Override
    public void lowerPriority() {
        if (getPriority() != Priority.HIGH) {
            setPriority(Priority.values()[getPriority().ordinal() - 1]);
        } else {
            addActivity(String.format("Bug priority already at %s", getPriority()));
        }
    }

    @Override
    public List<String> getSteps() {
        return new ArrayList<>(stepsToReproduce);
    }

    @Override
    public void addSteps(String step) {
        Validator.validateNotNull(step,"Steps to reproduce");
        stepsToReproduce.add(step);
    }
}
