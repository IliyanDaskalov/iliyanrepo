package com.telerikacademy.oop.workitemmanagement.models;

import com.telerikacademy.oop.workitemmanagement.models.contracts.Board;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Team;
import com.telerikacademy.oop.workitemmanagement.models.utils.ModelConstants;
import com.telerikacademy.oop.workitemmanagement.models.utils.Validator;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {

    private static final int TEAM_NAME_MIN = 5;
    private static final int TEAM_NAME_MAX = 15;

    private String name;
    private final List<Member> memberList;
    private final List<Board> boardList;

    public TeamImpl(String name) {
        setName(name);
        memberList = new ArrayList<>();
        boardList = new ArrayList<>();
    }

    private void setName(String name) {
        Validator.validateNotNull(name, "Team name");
        Validator.validateLength(name, TEAM_NAME_MIN, TEAM_NAME_MAX, "Team name");
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<Board> getBoardsList() {
        return new ArrayList<>(boardList);
    }

    @Override
    public List<Member> getMembersList() {
        return new ArrayList<>(memberList);
    }

    @Override
    public void addMemberToTeam(Member member) {
        Validator.validateNotNull(member, "Member");

        if (memberList.contains(member)) {
            throw new IllegalArgumentException(String.format(ModelConstants.MEMBER_EXIST_ERROR, member.getName()));
        }
        memberList.add(member);
    }

    @Override
    public void removeMemberFromTeam(Member member) {
        Validator.validateNotNull(member, "Member");
        if (!memberList.contains(member)) {
            throw new IllegalArgumentException(String.format(ModelConstants.MEMBER_DOES_NOT_EXIST_ERROR, member.getName()));
        }
        memberList.remove(member);
    }

    // TODO: 17/12/2020 check if we have to remove validate for null, fix if boardName is unique
    @Override
    public void createBoard(Board board) {
        Validator.validateNotNull(board, "Board");
        if (boardList.contains(board)) {
            throw new IllegalArgumentException(String.format(ModelConstants.BOARD_EXIST_ERROR, board.getName()));
        }
        boardList.add(board);

    }

}
