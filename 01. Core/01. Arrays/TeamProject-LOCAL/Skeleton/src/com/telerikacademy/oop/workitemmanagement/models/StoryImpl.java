package com.telerikacademy.oop.workitemmanagement.models;

import com.telerikacademy.oop.workitemmanagement.models.contracts.Assignable;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Story;
import com.telerikacademy.oop.workitemmanagement.models.enums.Priority;
import com.telerikacademy.oop.workitemmanagement.models.enums.StorySize;
import com.telerikacademy.oop.workitemmanagement.models.enums.StoryStatus;
import com.telerikacademy.oop.workitemmanagement.models.utils.Validator;

public class StoryImpl extends WorkItemBase implements Story {

    private Member assignee;
    private StorySize size;
    private Priority priority;
    private StoryStatus status;

    public StoryImpl(String title, String description, Priority priority, StorySize size) {
        super(title, description);
        this.assignee = null;
        this.size = size;
        this.priority = priority;
        this.status = StoryStatus.NOTDONE;
        addActivity(String.format("Story %s was created", getTitle()));
    }

    @Override
    public void setAssignee(Member member) {
        Validator.validateNotNull(member, "Assignee");
        if(this.assignee != null){
            this.assignee.removeWorkItem(this);
        }
        this.assignee = member;
        addActivity(String.format("%s assigned to %s",getTitle(),getAssigneeName()));
    }

    private void setStatus(StoryStatus status) {
        addActivity(String.format("Story status changed from %s to %s", getStatus(), status));
        this.status = status;
    }

    private void setPriority(Priority priority) {
        addActivity(String.format("Priority changed from %s to %s", getPriority(), priority));
        this.priority = priority;
    }

    private void setSize(StorySize size) {
        addActivity(String.format("Size changed from %s to %s", getSize(), size));
        this.size = size;

    }

    @Override
    public Member getAssignee() {
        return this.assignee;
    }

    @Override
    public String getAssigneeName() {
        return assignee.getName();
    }


    @Override
    public StoryStatus getStatus() {
        return this.status;
    }

    @Override
    public void raiseStatus() {
        if (getStatus() != StoryStatus.DONE) {
            setStatus(StoryStatus.values()[getStatus().ordinal() + 1]);
        } else {
            addActivity(String.format("Story status already at %s", getStatus()));
        }
    }

    @Override
    public void lowerStatus() {
        if (getStatus() != StoryStatus.NOTDONE) {
            setStatus(StoryStatus.values()[getStatus().ordinal() - 1]);
        } else {
            addActivity(String.format("Story status already at %s", getStatus()));
        }

    }

    @Override
    public Priority getPriority() {
        return this.priority;
    }

    @Override
    public void raisePriority() {
        if (getPriority() != Priority.HIGH) {
            setPriority(Priority.values()[getStatus().ordinal() + 1]);
        } else {
            addActivity(String.format("Priority status already at %s", getPriority()));
        }

    }

    @Override
    public void lowerPriority() {
        if (getPriority() != Priority.LOW) {
            setPriority(Priority.values()[getStatus().ordinal() - 1]);
        } else {
            addActivity(String.format("Priority status already at %s", getPriority()));
        }

    }

    @Override
    public StorySize getSize() {
        return this.size;
    }

    @Override
    public void raiseSize() {
        if (getSize() != StorySize.LARGE) {
            setSize(StorySize.values()[getStatus().ordinal() + 1]);
        } else {
            addActivity(String.format("Size status already at %s", getSize()));
        }

    }

    @Override
    public void lowerSize() {
        if (getSize() != StorySize.SMALL) {
            setSize(StorySize.values()[getStatus().ordinal() - 1]);
        } else {
            addActivity(String.format("Size status already at %s", getSize()));
        }

    }
}
