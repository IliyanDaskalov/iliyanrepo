package com.telerikacademy.oop.workitemmanagement.models.contracts;

import com.telerikacademy.oop.workitemmanagement.models.enums.Priority;
import com.telerikacademy.oop.workitemmanagement.models.enums.BugSeverity;
import com.telerikacademy.oop.workitemmanagement.models.enums.BugStatus;

import java.util.List;

public interface Bug extends WorkItem,Assignable{

    BugStatus getStatus();

    void raiseStatus();

    void lowerStatus();

    BugSeverity getSeverity();

    void raiseSeverity();

    void lowerSeverity();

    Priority getPriority();

    void raisePriority();

    void lowerPriority();

    List<String> getSteps();

    void addSteps(String step);

}
