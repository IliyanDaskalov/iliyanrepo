package com.telerikacademy.oop.workitemmanagement.models.enums;

public enum StoryStatus {
    NOTDONE,
    INPROGRESS,
    DONE;
}
