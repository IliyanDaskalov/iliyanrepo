package com.telerikacademy.oop.workitemmanagement.commands.contracts;

import java.util.List;

public interface Command {
    
    String execute(List<String> parameters);
    
}
