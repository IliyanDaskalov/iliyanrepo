package com.telerikacademy.oop.workitemmanagement.models.contracts;

public interface Assignable {

    void setAssignee(Member member);

    Member getAssignee();

    String getAssigneeName();

   }
