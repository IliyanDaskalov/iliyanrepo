package com.telerikacademy.oop.workitemmanagement.core.contracts;


import com.telerikacademy.oop.workitemmanagement.models.CommentImpl;
import com.telerikacademy.oop.workitemmanagement.models.contracts.*;

public interface ManagementFactory {

    Team createTeam(String name);

    Member createMember(String name);

    Board createBoard(String name);

    // TODO: 17/12/2020 unsure inputs about ID, status, etc.
    Bug createBug(String title, String description, String severity, String priority);

    Story createStory(String title, String description, String priority,String size);

    Feedback createFeedback(String title, String description, int rating);

    Comment createComment(String message, Member author);


}
