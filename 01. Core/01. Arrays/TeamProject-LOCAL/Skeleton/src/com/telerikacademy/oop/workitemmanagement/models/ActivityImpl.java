package com.telerikacademy.oop.workitemmanagement.models;

import com.telerikacademy.oop.workitemmanagement.models.contracts.Activity;
import com.telerikacademy.oop.workitemmanagement.models.utils.Validator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ActivityImpl implements Activity {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");
    private  String activityMessage;
    private final LocalDateTime date;

    public ActivityImpl(String activityMessage) {
        setActivityMessage(activityMessage);
        this.date = LocalDateTime.now();
    }

    private void setActivityMessage(String activityMessage) {
        Validator.validateNotNull(activityMessage,"Activity message");
        this.activityMessage = activityMessage;
    }

    private String getActivityMessage() {
        return activityMessage;
    }

    private LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return String.format("[%s] %s", getDate().format(formatter), getActivityMessage());
    }
}
