package com.telerikacademy.oop.workitemmanagement.models.enums;

public enum BugSeverity {
    CRITICAL,
    MAJOR,
    MINOR;
}
