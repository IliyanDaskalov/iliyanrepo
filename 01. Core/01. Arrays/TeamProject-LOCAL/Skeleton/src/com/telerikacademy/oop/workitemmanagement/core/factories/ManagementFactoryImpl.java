package com.telerikacademy.oop.workitemmanagement.core.factories;

import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.models.*;
import com.telerikacademy.oop.workitemmanagement.models.CommentImpl;
import com.telerikacademy.oop.workitemmanagement.models.contracts.*;
import com.telerikacademy.oop.workitemmanagement.models.enums.BugSeverity;
import com.telerikacademy.oop.workitemmanagement.models.enums.Priority;
import com.telerikacademy.oop.workitemmanagement.models.enums.StorySize;

public class ManagementFactoryImpl implements ManagementFactory {


    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Member createMember(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Board createBoard(String name) {
        return new BoardImpl(name);
    }

    @Override
    public Bug createBug(String title, String description, String severity, String priority) {
        return new BugImpl(title, description, getSeverity(severity), getPriority(priority));
    }

    @Override
    public Story createStory(String title, String description, String priority, String size) {
        return new StoryImpl(title, description, getPriority(priority), getSize(size));
    }


    @Override
    public Feedback createFeedback(String title, String description, int rating) {
        return new FeedbackImpl(title, description, rating);
    }

    @Override
    public Comment createComment(String message, Member author) {
        return new CommentImpl(message,author);
    }


    private Priority getPriority(String priority) {
        return Priority.valueOf(priority.toUpperCase());
    }

    private StorySize getSize(String size) {
        return StorySize.valueOf(size.toUpperCase());
    }

    private BugSeverity getSeverity(String severity) {
        return BugSeverity.valueOf(severity.toUpperCase());
    }

}
