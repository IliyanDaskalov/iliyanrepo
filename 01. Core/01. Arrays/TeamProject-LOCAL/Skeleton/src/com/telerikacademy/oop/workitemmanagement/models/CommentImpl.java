package com.telerikacademy.oop.workitemmanagement.models;

import com.telerikacademy.oop.workitemmanagement.models.contracts.Comment;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;
import com.telerikacademy.oop.workitemmanagement.models.utils.Validator;

public class CommentImpl implements Comment {

    private String message;
    private Member author;

    public CommentImpl(String message, Member author) {
        setMessage(message);
        setAuthor(author);
    }

    private void setMessage(String message) {
        Validator.validateNotNull(message, "Message");
        this.message = message;
    }

    private void setAuthor(Member author) {
        Validator.validateNotNull(author, "Author");
        this.author = author;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "message='" + message + '\'' +
                ", author=" + author +
                '}';
    }

    @Override
    public String getAuthorName() {
        return this.author.getName();
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
