package com.telerikacademy.oop.workitemmanagement.core.factories;

import com.telerikacademy.oop.workitemmanagement.commands.AddCommentToWorkItem;
import com.telerikacademy.oop.workitemmanagement.commands.AddPersonToTeam;
import com.telerikacademy.oop.workitemmanagement.commands.AssignWorkItemToPerson;
import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.commands.creators.*;
import com.telerikacademy.oop.workitemmanagement.commands.enums.CommandType;
import com.telerikacademy.oop.workitemmanagement.core.contracts.CommandFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;

public class CommandFactoryImpl implements CommandFactory {
    
    private static final String INVALID_COMMAND = "Invalid command name: %s!";


    @Override
    public Command createCommand(String commandName, ManagementFactory managementFactory, ManagementRepository managementRepository) {
        CommandType commandType = CommandType.valueOf(commandName.toUpperCase());

        switch (commandType){
            case CREATENEWTEAM:
                return new CreateNewTeam(managementRepository,managementFactory);
            case CREATENEWMEMBER:
                return new CreateNewMember(managementFactory, managementRepository);
            case CREATENEWBOARDINATEAM:
                return new CreateNewBoardInATeam(managementRepository,managementFactory);
            case CREATENEWBUGINBOARD:
                return new CreateNewBugInABoard(managementRepository,managementFactory);
            case CREATENEWSTORYINBOARD:
                return new CreateNewStoryInABoard(managementRepository,managementFactory);
            case CREATENEWFEEDBACKINBOARD:
               return new CreateNewFeedbackInABoard(managementRepository,managementFactory);
            case RAISEBUGPRIORITY:
                throw new IllegalArgumentException("Not Implemented");
            case LOWERBUGPRIORITY:
                throw new IllegalArgumentException("Not Implemented");
            case RAISEBUGSEVERITY:
                throw new IllegalArgumentException("Not Implemented");
            case LOWERBUGSEVERITY:
                throw new IllegalArgumentException("Not Implemented");
            case RAISESTORYPRIORITY:
                throw new IllegalArgumentException("Not Implemented");
            case LOWERSTORYPRIORITY:
                throw new IllegalArgumentException("Not Implemented");
            case RAISESTORYSIZE:
                throw new IllegalArgumentException("Not Implemented");
            case LOWERSTORYSIZE:
                throw new IllegalArgumentException("Not Implemented");
            case RAISEFEEDBACKRATING:
                throw new IllegalArgumentException("Not Implemented");
            case LOWERFEEDBACKRATING:
                throw new IllegalArgumentException("Not Implemented");
            case RAISESTATUSOFWORKITEM:
                throw new IllegalArgumentException("Not Implemented");
            case LOWERSTATUSOFWORKITEM:
                throw new IllegalArgumentException("Not Implemented");
            case ADDPERSONTOTEAM:
                return new AddPersonToTeam(managementRepository,managementFactory);
            case ADDCOMMENTTOWORKITEM:
                return new AddCommentToWorkItem(managementRepository,managementFactory);
            case ASSIGNWORKITEMTOPERSON:
                return new AssignWorkItemToPerson(managementRepository);
            case UNASSIGNWORKITEMFROMPERSON:
                throw new IllegalArgumentException("Not Implemented");
            case SHOWALLTEAMS:
                throw new IllegalArgumentException("Not Implemented");
            case SHOWALLPEOPLE:
                throw new IllegalArgumentException("Not Implemented");
            case SHOWALLTEAMMEMBERS:
                throw new IllegalArgumentException("Not Implemented");
            case SHOWALLTEAMBOARDS:
                throw new IllegalArgumentException("Not Implemented");
            case SHOWWORKITEMS:
                throw new IllegalArgumentException("Not Implemented");
            case SHOWTEAMSACTIVITY:
                throw new IllegalArgumentException("Not Implemented");
            case SHOWBOARDSACTIVITY:
                throw new IllegalArgumentException("Not Implemented");
            case SHOWPERSONSACTIVITY:
                throw new IllegalArgumentException("Not Implemented");

        }
        //TODO
        return null;
    }
}
