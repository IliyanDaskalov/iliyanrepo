package com.telerikacademy.oop.workitemmanagement.models;

import com.telerikacademy.oop.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.oop.workitemmanagement.models.enums.BugStatus;
import com.telerikacademy.oop.workitemmanagement.models.enums.FeedbackStatus;

public class FeedbackImpl extends WorkItemBase implements Feedback {

    private int rating;
    private FeedbackStatus status;

    public FeedbackImpl(String title, String description, int rating) {
        super(title, description);
        setRating(rating);
        this.status = FeedbackStatus.NEW;
        addActivity(String.format("Feedback %s was created", getTitle()));
    }

    private void setRating(int rating) {
        this.rating = rating;
    }

    private void setStatus(FeedbackStatus status) {
        addActivity(String.format("Feedback status changed from %s to %s", getStatus(), status));
        this.status = status;
    }

    @Override
    public FeedbackStatus getStatus() {
        return this.status;
    }

    @Override
    public void raiseStatus() {
        if (getStatus() != FeedbackStatus.DONE) {
            setStatus(FeedbackStatus.values()[getStatus().ordinal() + 1]);
        } else {
            addActivity(String.format("Feedback status already at %s", getStatus()));
        }
    }

    @Override
    public void lowerStatus() {
        if (getStatus() != FeedbackStatus.NEW) {
            setStatus(FeedbackStatus.values()[getStatus().ordinal() - 1]);
        } else {
            addActivity(String.format("Feedback status already at %s", getStatus()));
        }
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void changeRating(int newRating) {
        setRating(newRating);
    }
}
