package com.telerikacademy.oop.workitemmanagement;

import com.telerikacademy.oop.workitemmanagement.core.ManagementEngineImpl;
import com.telerikacademy.oop.workitemmanagement.models.TeamImpl;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Team;

import java.io.ByteArrayInputStream;

public class Startup {
    public static void userInput() {
        String input = "CreateNewTeam newTeamName\n" +
                "CreateNewMember SomeMemberName\n" +
                "CreateNewBoardInATeam NewBoard newTeamName\n" +
                "CreateNewStoryInBoard NewStoryName SomeRandomDescription Medium Small newTeamName NewBoard\n" +
                "CreateNewBugInBoard NewBugName SomeRandomDescription Critical HIGH newTeamName NewBoard\n" +
                "CreateNewFeedbackInBoard NewFeedBakName SomeRandomDescription 34 newTeamName NewBoard\n" +
                "AssignWorkItemToPerson NewStoryName SomeMemberName\n" +
                "AddPersonToTeam SomeMemberName newTeamName\n" +
                "exit";
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }

    public static void main(String[] args) {
        userInput();
        ManagementEngineImpl engine = new ManagementEngineImpl();
        engine.start();
    }
}
