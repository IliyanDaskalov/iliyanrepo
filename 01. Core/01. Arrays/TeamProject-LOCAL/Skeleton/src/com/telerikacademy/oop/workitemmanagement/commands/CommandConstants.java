package com.telerikacademy.oop.workitemmanagement.commands;

public class CommandConstants {

    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
}
