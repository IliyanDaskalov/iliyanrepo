package com.telerikacademy.oop.workitemmanagement.commands;

import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class AddCommentToWorkItem implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;

    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    private String comment;
    private Member member;
    private WorkItem workItem;

    public AddCommentToWorkItem(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }


    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
        parseParameters(parameters);
        Comment newComment = managementFactory.createComment(comment, member);
        workItem.addComment(newComment);
        return String.format("Now comment added to %s", workItem.getTitle());


    }

    private void parseParameters(List<String> parameters) {
        try {
            comment = parameters.get(0);
            getMember(parameters.get(1));
            getWorkItem(parameters.get(2));


        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateNewStoryInABoard command parameters.");
        }
    }

    private void getMember(String memberName) {
        if (managementRepository.getMembers().containsKey(memberName)) {
            this.member = managementRepository.getMembers().get(memberName);
        } else {
            throw new IllegalArgumentException(String.format("No Member %s exist", memberName));
        }
    }

    private void getWorkItem(String workItem) {
       managementRepository.getWorkItem().containsKey(workItem);
        throw new IllegalArgumentException(String.format("There is no workItem %s to add a comment to", workItem));
    }


}
