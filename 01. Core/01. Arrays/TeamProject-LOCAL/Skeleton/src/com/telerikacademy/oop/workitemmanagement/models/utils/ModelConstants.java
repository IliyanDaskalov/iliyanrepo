package com.telerikacademy.oop.workitemmanagement.models.utils;

public class ModelConstants {

    //Error Messages

        public static final String STRING_LENGTH_OUT_OF_BOUNDS_ERROR = "%s should be between %d and %d characters!";
        public static final String NULLPOINT_ERROR = "%s cannot be null!";
        public static final String MEMBER_EXIST_ERROR = "Member %s already exist in team!";
        public static final String MEMBER_DOES_NOT_EXIST_ERROR = "Member %s does not exist in team!";
        public static final String BOARD_EXIST_ERROR = "Board %s already exist in team!";
        public static final String WORKITEM_EXIST_ERROR = "WorkItem %s already exist on the Board!";
}
