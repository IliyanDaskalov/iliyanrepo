package com.telerikacademy.oop.workitemmanagement.models.enums;

public enum BugStatus {
    ACTIVE,
    FIXED;
}
