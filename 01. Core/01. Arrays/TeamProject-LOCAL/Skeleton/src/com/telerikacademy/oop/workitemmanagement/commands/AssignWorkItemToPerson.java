package com.telerikacademy.oop.workitemmanagement.commands;

import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.*;

import java.util.List;

public class AssignWorkItemToPerson implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;

    private Assignable workItem;
    private Member member;

    public AssignWorkItemToPerson(ManagementRepository managementRepository) {
        this.managementRepository = managementRepository;

    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
        parseParameters(parameters);
        member.addWorkItem((WorkItem) workItem);
        workItem.setAssignee(member);

        return String.format("%s assigned to %s",((WorkItem) workItem).getTitle(),member.getName());

    }

    private void parseParameters(List<String> parameters) {
        try {
            getMember(parameters.get(1));
            getWorkItem(parameters.get(0));
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateNewStoryInABoard command parameters.");
        }
    }

    private void getWorkItem(String workItem){
        for (Team team : managementRepository.getTeams().values()) {
            for (Board board : team.getBoardsList()) {
                for (WorkItem item : board.getWorkItemsList()) {
                    if (workItem.equals(item.getTitle()) && item instanceof Assignable) {
                        this.workItem = (Assignable) item;
                        return;
                    }
                }
            }
        }
        throw new IllegalArgumentException(String.format("There is no workItem %s to assign %s to", workItem, member.getName()));
    }

    private void getMember(String member){
        if (managementRepository.getMembers().containsKey(member)) {
            this.member = managementRepository.getMembers().get(member);
        } else {
            throw new IllegalArgumentException(String.format("No Member %s exist", member));
        }
    }
}
