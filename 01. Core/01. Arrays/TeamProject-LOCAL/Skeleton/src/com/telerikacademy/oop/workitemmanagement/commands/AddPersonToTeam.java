package com.telerikacademy.oop.workitemmanagement.commands;

import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.*;

import java.util.List;

public class AddPersonToTeam implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    private Member member;
    private Team team;

    public AddPersonToTeam(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }


    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
        parseParameters(parameters);
        Member newMember = managementFactory.createMember(member.getName());
        team.addMemberToTeam(newMember);
        return String.format("%s member added to team %s", member.getName(),team.getName());


    }

    private void parseParameters(List<String> parameters) {
        try {
            getMember(parameters.get(0));
            getTeam(parameters.get(1));


        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse AddPersonToTeam command parameters.");
        }
    }

    private void getMember(String memberName) {
        if (managementRepository.getMembers().containsKey(memberName)) {
            this.member = managementRepository.getMembers().get(memberName);
        } else {
            throw new IllegalArgumentException(String.format("No Member %s exist", memberName));
        }
    }

    private void getTeam(String teamName) {
        if (managementRepository.getTeams().containsKey(teamName)) {
            this.team = managementRepository.getTeams().get(teamName);
        } else {
            throw new IllegalArgumentException(String.format("There is no such %s team to add a member to", teamName));
        }

    }
}
