package com.telerikacademy.oop.workitemmanagement.models.contracts;

import com.telerikacademy.oop.workitemmanagement.models.enums.FeedbackStatus;

public interface Feedback extends WorkItem{

    FeedbackStatus getStatus();

    void raiseStatus();

    void lowerStatus();

    int getRating();

    void changeRating(int newRating);
}
