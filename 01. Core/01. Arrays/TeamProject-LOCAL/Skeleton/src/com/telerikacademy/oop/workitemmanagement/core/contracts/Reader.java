package com.telerikacademy.oop.workitemmanagement.core.contracts;

public interface Reader {
    
    String readLine();
    
}
