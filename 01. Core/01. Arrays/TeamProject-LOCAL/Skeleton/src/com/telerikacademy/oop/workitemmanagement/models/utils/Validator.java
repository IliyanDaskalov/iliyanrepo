package com.telerikacademy.oop.workitemmanagement.models.utils;

public class Validator {

    private Validator(){
    }

    public static void validateLength(String input, int min, int max, String message) {
        String trimmedInput = input.trim();

        if (trimmedInput.length() < min
                || trimmedInput.length() > max)
            throw new IllegalArgumentException(String.format(ModelConstants.STRING_LENGTH_OUT_OF_BOUNDS_ERROR,message,min,max));
    }

    public static void validateNotNull(Object obj, String message) {
        if (obj == null)
            throw new NullPointerException(String.format(ModelConstants.NULLPOINT_ERROR,message));

    }
}
