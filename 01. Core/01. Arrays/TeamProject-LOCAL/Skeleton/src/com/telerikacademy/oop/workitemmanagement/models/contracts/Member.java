package com.telerikacademy.oop.workitemmanagement.models.contracts;

import java.util.List;

public interface Member {

    String getName();

    List<WorkItem> getWorkItemsList();

    List<Activity> getActivityHistory();

    void addWorkItem(WorkItem workItem);

    void removeWorkItem(WorkItem workItem);

    void addActivity(String activity);
}
