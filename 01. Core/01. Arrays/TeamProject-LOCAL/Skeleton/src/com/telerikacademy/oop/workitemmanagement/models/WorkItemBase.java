package com.telerikacademy.oop.workitemmanagement.models;

import com.telerikacademy.oop.workitemmanagement.models.contracts.Activity;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Comment;
import com.telerikacademy.oop.workitemmanagement.models.contracts.WorkItem;
import com.telerikacademy.oop.workitemmanagement.models.utils.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class WorkItemBase implements WorkItem {

    private static final int WORKITEM_TITLE_MIN_LENGTH = 10;
    private static final int WORKITEM_TITLE_MAX_LENGTH = 50;
    private static final int WORKITEM_DESCRIPTION_MIN_LENGTH = 10;
    private static final int WORKITEM_DESCRIPTION_MAX_LENGTH = 500;

    private String title;
    private String description;
    private String id;
    private List<Comment> commentList;
    private List<Activity> activityList;

    public WorkItemBase(String title, String description) {
        setTitle(title);
        setDescription(description);
        setId();
        commentList = new ArrayList<>();
        activityList = new ArrayList<>();
    }

    private void setTitle(String title) {
        Validator.validateNotNull(title,"Title");
        Validator.validateLength(title, WORKITEM_TITLE_MIN_LENGTH, WORKITEM_TITLE_MAX_LENGTH, "Title");
        this.title = title;
    }

    private void setDescription(String description) {
        Validator.validateNotNull(description,"Description");
        Validator.validateLength(description, WORKITEM_DESCRIPTION_MIN_LENGTH, WORKITEM_DESCRIPTION_MAX_LENGTH, "Description");
        this.description = description;
    }

    private void setId() {
        this.id = ID.generateID();
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getID() {
        return this.id;
    }

    @Override
    public List<Comment> getCommentsList() {
        return new ArrayList<>(commentList);
    }

    @Override
    public void addComment(Comment comment) {
        Validator.validateNotNull(comment,"Comment");
        commentList.add(comment);
        addActivity(String.format("Commend added - %s - by %s",comment.getMessage(),comment.getAuthorName()));
    }

    @Override
    public List<Activity> getHistoryList() {
        return new ArrayList<>(activityList);
    }

    @Override
    public void addActivity(String activity) {
        Validator.validateNotNull(activity, "Activity");
        Activity newActivity = new ActivityImpl(activity);
        activityList.add(newActivity);
    }


    private static class ID {
        private static final List<String> idList = new ArrayList<>();

        public static String generateID(){
            String uniqueID;
            while(true){
                uniqueID = UUID.randomUUID().toString();
                if(!idList.contains(uniqueID)){
                    idList.add(uniqueID);
                    break;
                }
            }
            return uniqueID;
        }
    }
}
