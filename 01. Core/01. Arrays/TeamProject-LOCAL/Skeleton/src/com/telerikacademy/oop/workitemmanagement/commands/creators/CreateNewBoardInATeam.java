package com.telerikacademy.oop.workitemmanagement.commands.creators;

import com.telerikacademy.oop.workitemmanagement.commands.CommandConstants;
import com.telerikacademy.oop.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementFactory;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Board;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Team;

import java.util.List;

public class CreateNewBoardInATeam implements Command {

    private final static int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final ManagementRepository managementRepository;
    private final ManagementFactory managementFactory;

    private String name;
    private Team team;


    public CreateNewBoardInATeam(ManagementRepository managementRepository, ManagementFactory managementFactory) {
        this.managementRepository = managementRepository;
        this.managementFactory = managementFactory;
    }


    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS, CORRECT_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
        parseParameters(parameters);
        if (doesBoardExist(name)) {
            throw new IllegalArgumentException(String.format("Board with name '%s' already exist in team %s", name, team.getName()));
        }

        Board board = managementFactory.createBoard(name);
        team.createBoard(board);
        return String.format("Board %s was created in team %s", name, team.getName());

    }

    private void parseParameters(List<String> parameters) {
        try {
            name = parameters.get(0);
            findTeam(parameters.get(1));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateNewBoardInATeam command parameters.");
        }
    }


    private void findTeam(String team) {
        if (!managementRepository.getTeams().containsKey(team)) {
            throw new IllegalArgumentException(String.format("%s, does not exist", team));
        }
        if(!managementRepository.getTeams().containsKey(team)){
            throw new IllegalArgumentException("No such team exist");
        }
        this.team = managementRepository.getTeams().get(team);
    }


    private boolean doesBoardExist(String name) {
        List<Board> boards = this.team.getBoardsList();

        return boards.stream()
                .anyMatch(board -> board.getName().contains(name));

    }

}
