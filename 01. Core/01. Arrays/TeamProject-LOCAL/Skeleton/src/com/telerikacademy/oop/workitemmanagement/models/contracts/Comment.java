package com.telerikacademy.oop.workitemmanagement.models.contracts;

public interface Comment {

    String toString();

    String getAuthorName();

    String getMessage();
}
