package com.telerikacademy.oop.workitemmanagement.core.contracts;

public interface Engine {
    
    void start();
    
}
