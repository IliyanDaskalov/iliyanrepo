package com.telerikacademy.oop.workitemmanagement.models.enums;

public enum Priority {
    HIGH,
    MEDIUM,
    LOW;
}
