package com.telerikacademy.oop.workitemmanagement.core;
import com.telerikacademy.oop.workitemmanagement.core.contracts.ManagementRepository;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Member;
import com.telerikacademy.oop.workitemmanagement.models.contracts.Team;
import com.telerikacademy.oop.workitemmanagement.models.contracts.WorkItem;

import java.util.HashMap;
import java.util.Map;

public class ManagementRepositoryImpl implements ManagementRepository {

    // TODO: 17/12/2020 decide on final for the Map statements
    private Map<String, Team> teams;
    private Map<String, Member> members;
    private Map<String, WorkItem> workItems;

    public ManagementRepositoryImpl() {
        this.teams = new HashMap<>();
        this.members = new HashMap<>();
    }

    @Override
    public Map<String, Team> getTeams() {
        return new HashMap<>(teams);
    }

    @Override
    public Map<String, Member> getMembers() {
        return new HashMap<>(members);
    }

    @Override
    public Map<String, WorkItem> getWorkItem() {
        return new HashMap<>(workItems);
    }

    @Override
    public void addTeam(String name, Team team) {
        this.teams.put(name, team);
    }

    @Override
    public void addMember(String name, Member member) {
        this.members.put(name, member);
    }
    
}
