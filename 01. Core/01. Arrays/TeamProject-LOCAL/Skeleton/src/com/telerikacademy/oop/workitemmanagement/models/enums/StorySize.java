package com.telerikacademy.oop.workitemmanagement.models.enums;

public enum StorySize {
    LARGE,
    MEDIUM,
    SMALL;
}
