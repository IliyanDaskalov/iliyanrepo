package com.telerikacademy.oop.workitemmanagement.models.contracts;

import com.telerikacademy.oop.workitemmanagement.models.enums.Priority;
import com.telerikacademy.oop.workitemmanagement.models.enums.StorySize;
import com.telerikacademy.oop.workitemmanagement.models.enums.StoryStatus;

public interface Story extends WorkItem,Assignable{
    // TODO: 17/12/2020  Member or String for assignee?

    StoryStatus getStatus();

    void raiseStatus();

    void lowerStatus();

    Priority getPriority();

    void raisePriority();

    void lowerPriority();

    StorySize getSize();

    void raiseSize();

    void lowerSize();

}
