package com.telerikacademy.oop.cosmetics.exceptions;

public class IllegalParameterCountException extends IllegalArgumentException {
    public IllegalParameterCountException() {
    }

    public IllegalParameterCountException(String message) {
        super(message);
    }
}
