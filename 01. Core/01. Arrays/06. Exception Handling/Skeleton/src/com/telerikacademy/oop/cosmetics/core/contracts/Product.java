package com.telerikacademy.oop.cosmetics.core.contracts;

import com.telerikacademy.oop.cosmetics.models.GenderType;

public interface Product {
    String getName();

    String getBrand();

    double getPrice();

    GenderType getGender();

    String print();
}
