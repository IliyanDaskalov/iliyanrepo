package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.core.contracts.Product;

import java.util.InputMismatchException;

public class ProductImpl implements Product {
    private static final int MIN_PRODUCT_NAME_LENGTH = 3;
    private static final int MAX_PRODUCT_NAME_LENGTH = 10;
    private static final int MIN_BRAND_LENGTH = 2;
    private static final int MAX_BRAND_LENGTH = 10;
    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        Validator.nullValidator("Product name", name);
        Validator.validateLength(name, MIN_PRODUCT_NAME_LENGTH,
                MAX_PRODUCT_NAME_LENGTH,
                String.format("Product name should be between %d and %d symbols.",
                        MIN_PRODUCT_NAME_LENGTH,
                        MAX_PRODUCT_NAME_LENGTH));

        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        Validator.nullValidator("Brand", brand);
        Validator.validateLength(brand,
                MIN_BRAND_LENGTH,
                MAX_BRAND_LENGTH,
                String.format("Product brand should be between %d and %d symbols.",
                        MIN_BRAND_LENGTH,
                        MAX_BRAND_LENGTH));

        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        Validator.validatePrice(price);
        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    private void setGender(GenderType gender) {
        Validator.nullValidator("Gender", gender);
        this.gender = gender;
    }

    @Override
    public String print() {
        return String.format(
                "#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n",
                name,
                brand,
                price,
                gender);
    }
}
