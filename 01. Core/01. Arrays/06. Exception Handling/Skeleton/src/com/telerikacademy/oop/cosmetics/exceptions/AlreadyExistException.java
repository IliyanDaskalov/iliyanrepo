package com.telerikacademy.oop.cosmetics.exceptions;

public class AlreadyExistException extends IllegalArgumentException{
    public AlreadyExistException() {
    }

    public AlreadyExistException(String message) {
        super(message);
    }
}
