package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.Validator;

import java.util.List;

public class CreateCategory implements Command {
    private static final String CATEGORY_CREATED = "Category with name %s was created!";
    private static final int PARAMETER_COUNT = 1;

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateCategory(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        //TODO Validate parameters count *DONE*

        Validator.validateParameters(parameters,
                        PARAMETER_COUNT,
                        String.format("%s command expects %d parameters.",
                                this.getClass().getSimpleName(),
                                PARAMETER_COUNT));

        String categoryName = parameters.get(0);

        result = createCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createCategory(String categoryName) {
        //TODO Ensure category name is unique

        Validator.validateExistingCategory(productRepository,categoryName);

        Category category = productFactory.createCategory(categoryName);
        productRepository.getCategories().put(categoryName, category);

        return String.format(CATEGORY_CREATED, categoryName);
    }
}
