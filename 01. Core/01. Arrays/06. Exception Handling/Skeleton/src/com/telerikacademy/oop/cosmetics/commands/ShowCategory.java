package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.models.Validator;

import java.util.List;

public class ShowCategory implements Command {
    private static final int PARAMETER_COUNT = 1;
    private final ProductRepository productRepository;
    private String result;

    public ShowCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        //TODO Validate parameters count *DONE*

        Validator.validateParameters(parameters,
                PARAMETER_COUNT,
                String.format("%s command expects %d parameters.",
                        this.getClass().getSimpleName(),
                        PARAMETER_COUNT));

        String categoryName = parameters.get(0);

        result = showCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String showCategory(String categoryName) {
        //TODO Validate category exist *DONE*
        Validator.validateForNonExistentCategory(productRepository,categoryName);

        Category category = productRepository.getCategories().get(categoryName);

        return category.print();
    }
}
