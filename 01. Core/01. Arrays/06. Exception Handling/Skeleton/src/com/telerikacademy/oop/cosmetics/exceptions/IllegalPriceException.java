package com.telerikacademy.oop.cosmetics.exceptions;

public class IllegalPriceException extends IllegalArgumentException{
    private static final String DEFAULT_EXCEPTION_MESSAGE = "Price can't be negative.";
    public IllegalPriceException() {
        super(DEFAULT_EXCEPTION_MESSAGE);
    }

    public IllegalPriceException(String s) {
        super(s);
    }
}
