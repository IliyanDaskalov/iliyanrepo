package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.IllegalPriceException;
import com.telerikacademy.oop.cosmetics.exceptions.IllegalParameterCountException;
import com.telerikacademy.oop.cosmetics.exceptions.NonExistentException;
import com.telerikacademy.oop.cosmetics.exceptions.AlreadyExistException;


import java.util.List;

public class Validator {
    public static void validateLength(String input, int min, int max, String exception) {
        String trimmedInput = input.trim();

        if (trimmedInput.length() < min
                || trimmedInput.length() > max)
            throw new IllegalArgumentException(exception);
    }

    public static void nullValidator(String input, Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException(input + " cannot be null!");
        }
    }

    public static void validatePrice(double price) {
        if (price < 0) {
            throw new IllegalPriceException();
        }
    }

    public static void validateParameters(List<String> parameters, int max, String exception){
        if (parameters.size() != max) {
            throw new IllegalParameterCountException(exception);
        }
    }

    public static void validateForNonExistentCategory(ProductRepository productRepository, String categoryName){
        if (!productRepository.getCategories().containsKey(categoryName)) {
            throw new NonExistentException("Category " + categoryName + " does not exist.");
        }
    }

    public static void validateExistingCategory(ProductRepository productRepository, String categoryName){
        if (productRepository.getCategories().containsKey(categoryName)) {
            throw new AlreadyExistException("Category " + categoryName + " already exist.");
        }
    }

    public static void validateForNonExistentProduct(ProductRepository productRepository, String productName){
        if (!productRepository.getProducts().containsKey(productName)) {
            throw new NonExistentException("Product " + productName + " does not exist.");
        }
    }
    public static void validateExistingProduct(ProductRepository productRepository, String name){
        if (productRepository.getProducts().containsKey(name)) {
            throw new AlreadyExistException("Product " + name + " already exist.");
        }
    }

//    public static <T extends Enum<T>>  void validateEnums(String input, Enum<T> [] enumArray, String message){
//
//        input = input.toUpperCase();
//        boolean enumExists = false;
//
//        for (Enum<T> anEnum: enumArray) {
//            if(anEnum.toString().toUpperCase().equals(input)){
//                enumExists = true;
//            }
//        }
//
//        if(!enumExists){
//            throw new IllegalArgumentException(message);
//        }
//    }


}
