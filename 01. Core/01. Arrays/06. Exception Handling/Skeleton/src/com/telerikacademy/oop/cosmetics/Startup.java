package com.telerikacademy.oop.cosmetics;

import com.telerikacademy.oop.cosmetics.core.EngineImpl;

public class Startup {

    public static void main(String[] args) {
        EngineImpl engine = new EngineImpl();
        engine.start();
    }
}
/*

Command SomeCommand is not supported. TODO
Category name should be between 3 and 10 symbols.
Category with name Shampoos was created!
Category Shampoos already exist.
CreateProduct command expects 4 parameters.
Product brand should be between 2 and 10 symbols.
Third parameter should be price (real number).
Price can't be negative.
Forth parameter should be one of Men, Women or Unisex.
Product with name MyMan was created!
Product MyMan already exist.
Category Shampo does not exist. TODO
Product MyBoy does not exist. TODO
Product MyMan added to category Shampoos! TODO
Product with name MyWoman was created!
Product MyWoman added to category Shampoos! TODO
Category Shampo does not exist. TODO
#Category: Shampoos
#MyMan Nivea
 #Price: $10,99
 #Gender: Men
 ===
#MyWoman Nivea
 #Price: $17,99
 #Gender: Women
 ===
 */