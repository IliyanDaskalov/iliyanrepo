package com.telerikacademy.oop.cosmetics.exceptions;

public class NonExistentException extends IllegalArgumentException {
    public NonExistentException(String s) {
        super(s);
    }
}
