package com.telerikacademy.dsa;

import java.util.*;

public class BinarySearchTreeImpl<E extends Comparable<E>> implements BinarySearchTree<E> {
    private E value;
    private BinarySearchTreeImpl<E> left;
    private BinarySearchTreeImpl<E> right;

    public BinarySearchTreeImpl(E value) {
        this.value = value;
        left = null;
        right = null;
    }


    @Override
    public E getRootValue() {
        return value;
    }

    @Override
    public BinarySearchTree<E> getLeftTree() {
        return left;
    }

    @Override
    public BinarySearchTree<E> getRightTree() {
        return right;
    }

    @Override
    public void insert(E value) {
        insertRec(this, value);
    }

    private void insertRec(BinarySearchTreeImpl<E> current, E value) {
        if (value.compareTo(current.value) <= 0) {
            if (current.left == null) {
                current.left = new BinarySearchTreeImpl<>(value);
            } else {
                insertRec(current.left, value);
            }
        } else {
            if (current.right == null) {
                current.right = new BinarySearchTreeImpl<>(value);
            } else {
                insertRec(current.right, value);
            }
        }
    }

    @Override
    public boolean search(E value) {
        return searchRec(this, value);
    }

    private boolean searchRec(BinarySearchTreeImpl<E> current, E value) {
        if (current == null) {
            return false;
        }
        if (value.compareTo(current.value) == 0) {
            return true;
        } else if (value.compareTo(current.value) < 0) {
            return searchRec(current.left, value);
        }
        return searchRec(current.right, value);
    }

    @Override
    public List<E> inOrder() {
        List<E> list = new ArrayList<>();
        return inOrderRec(this, list);
    }

    private List<E> inOrderRec(BinarySearchTreeImpl<E> current, List<E> list) {

        if (current.left != null) {
            list = inOrderRec(current.left, list);
        }
        list.add(current.value);
        if (current.right != null) {
            list = inOrderRec(current.right, list);
        }
        return list;
    }


    @Override
    public List<E> postOrder() {
        List<E> list = new ArrayList<>();
        return postOrderRec(this, list);
    }

    private List<E> postOrderRec(BinarySearchTreeImpl<E> current, List<E> list) {

        if (current.left != null) {
            list = postOrderRec(current.left, list);
        }
        if (current.right != null) {
            list = postOrderRec(current.right, list);
        }
        list.add(current.value);
        return list;
    }

    @Override
    public List<E> preOrder() {
        List<E> list = new ArrayList<>();
        return preOrderRec(this, list);
    }

    private List<E> preOrderRec(BinarySearchTreeImpl<E> current, List<E> list) {

        list.add(current.value);
        if (current.left != null) {
            list = preOrderRec(current.left, list);
        }
        if (current.right != null) {
            list = preOrderRec(current.right, list);
        }
        return list;
    }

    @Override
    public List<E> bfs() {
        List<E> list = new ArrayList<>();
        Deque<BinarySearchTreeImpl<E>> queue = new ArrayDeque<>();
        queue.offer(this);
        while (!queue.isEmpty()) {
            var node = queue.poll();
            list.add(node.value);
            if (node.left != null) {
                queue.offer(node.left);
            }
            if (node.right != null) {
                queue.offer(node.right);
            }
        }
        return list;
    }

    @Override
    public int height() {
        return heightRec(this, 0);
    }

    private int heightRec(BinarySearchTreeImpl<E> current, int height) {
        int max = height;

        if (current.left != null) {
            max = Math.max(heightRec(current.left, height + 1), max);
        }
        if (current.right != null) {
            max = Math.max(heightRec(current.right, height + 1), max);
        }
        return max;
    }


    // Advanced task: implement remove method. To test, uncomment the commented tests in BinaryTreeImplTests
//    @Override
//    public boolean remove(E value) {
//        throw new UnsupportedOperationException();
//    }
}
