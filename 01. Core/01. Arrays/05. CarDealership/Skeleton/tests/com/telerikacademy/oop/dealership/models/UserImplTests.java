package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.enums.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.dealership.models.common.Constants.*;

public class UserImplTests {
    
    
    @Test
    public void Constructor_ShouldThrow_WhenUsernameIsNull() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl(null,
                        "firstName",
                        "lastName",
                        "password",
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenUsernameIsBelowMinLength() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl(new String(new char[USERNAME_LEN_MIN - 1]),
                        "firstName",
                        "lastName",
                        "password",
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenUsernameIsAboveMaxLength() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl(new String(new char[USERNAME_LEN_MAX + 1]),
                        "firstName",
                        "lastName",
                        "password",
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenUsernameDoesNotMatchPattern() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("U$$ernam3",
                        "firstName",
                        "lastName",
                        "password",
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenFirstNameIsNull() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("username",
                        null,
                        "lastName",
                        "password",
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenFirstnameIsBelowMinLength() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("Username",
                        new String(new char[FIRSTNAME_LEN_MIN - 1]),
                        "lastName",
                        "password",
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenFirstnameIsAboveMaxLength() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("Username",
                        new String(new char[FIRSTNAME_LEN_MAX + 1]),
                        "lastName",
                        "password",
                        Role.NORMAL));
    }
    
    
    @Test
    public void Constructor_ShouldThrow_WhenLastNameIsNull() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("username",
                        "firstName",
                        null,
                        "password",
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenLastNameIsBelowMinLength() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("username",
                        "firstName",
                        new String(new char[LASTNAME_LEN_MIN - 1]),
                        "password",
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenLastNameIsAboveMaxLength() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("username",
                        "firstName",
                        new String(new char[LASTNAME_LEN_MAX + 1]),
                        "password",
                        Role.NORMAL));
    }
    
    
    @Test
    public void Constructor_ShouldThrow_WhenPasswordIsNull() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("username",
                        "firstName",
                        "lastName",
                        null,
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPasswordIsBelowMinLength() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("username",
                        "firstName",
                        "lastName",
                        new String(new char[PASSWORD_LEN_MIN - 1]),
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPasswordIsAboveMaxLength() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("username",
                        "firstName",
                        "lastName",
                        new String(new char[PASSWORD_LEN_MAX + 1]),
                        Role.NORMAL));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPasswordDoesNotMatchPattern() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new UserImpl("Username",
                        "firstName",
                        "lastName",
                        "Pa-$$_w0rD",
                        Role.NORMAL));
    }
    
}
