package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.DealershipRepositoryImpl;
import com.telerikacademy.oop.dealership.core.contracts.CommandFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.core.factories.CommandFactoryImpl;
import com.telerikacademy.oop.dealership.core.factories.DealershipFactoryImpl;
import com.telerikacademy.oop.dealership.models.UserImpl;
import com.telerikacademy.oop.dealership.models.common.enums.Role;
import com.telerikacademy.oop.dealership.models.contracts.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class Logout_Tests {
    
    private DealershipRepository dealershipRepository;
    private DealershipFactory dealershipFactory;
    private CommandFactory commandFactory;
    
    @BeforeEach
    public void before() {
        this.commandFactory = new CommandFactoryImpl();
        this.dealershipFactory = new DealershipFactoryImpl();
        this.dealershipRepository = new DealershipRepositoryImpl();
    }
    
    @Test
    public void execute_ShouldLogoutUser() {
        // Arrange
        User userToLogIn = new UserImpl("pesho123", "petar", "petrov", "password", Role.NORMAL);
        dealershipRepository.setLoggedUser(userToLogIn);
        Command logout = new Logout(dealershipFactory, dealershipRepository);
        
        // Act
        logout.execute(new ArrayList<>());
        
        // Assert
        Assertions.assertNull(dealershipRepository.getLoggedUser());
    }
    
}
