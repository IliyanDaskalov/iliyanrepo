package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Comment;
import com.telerikacademy.oop.dealership.models.common.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CommentImpl_Tests {
    
    @Test
    public void CommentImpl_ShouldImplementCommentInterface() {
        CommentImpl comment = new CommentImpl("content", "author");
        Assertions.assertTrue(comment instanceof Comment);
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenContentIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CommentImpl(null, "author"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenContentIsBelow3() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CommentImpl(new String(new char[Constants.CONTENT_LEN_MIN - 1]), "author"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenContentIsAbove200() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CommentImpl(new String(new char[Constants.CONTENT_LEN_MAX + 1]), "author"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenAuthorIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CommentImpl(new String(new char[Constants.CONTENT_LEN_MIN + 2]), null));
    }
    
}
