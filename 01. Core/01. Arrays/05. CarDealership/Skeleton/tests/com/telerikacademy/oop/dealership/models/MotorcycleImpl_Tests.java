package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.contracts.Motorcycle;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.models.common.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MotorcycleImpl_Tests {
    
    @Test
    public void MotorcycleImpl_ShouldImplementMotorcycleInterface() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "category");
        Assertions.assertTrue(motorcycle instanceof Motorcycle);
    }
    
    @Test
    public void MotorcycleImpl_ShouldImplementVehicleInterface() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "category");
        Assertions.assertTrue(motorcycle instanceof Vehicle);
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl(null, "model", 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl(new String(new char[Constants.MAKE_NAME_LEN_MIN - 1]), "model", 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl(new String(new char[Constants.MAKE_NAME_LEN_MAX + 1]), "model", 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", null, 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", new String(new char[Constants.MODEL_NAME_LEN_MIN - 1]), 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenModelLengthIsAbove15() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", new String(new char[Constants.MODEL_NAME_LEN_MAX + 1]), 100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPriceIsNegative() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "model", -100, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenPriceIsAbove1000000() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "model", 1000001, "category"));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenCategoryIsNull() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "model", 100, null));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenCategoryLengthIsBelow_3() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "model", 100, new String(new char[Constants.CATEGORY_LEN_MIN - 1])));
    }
    
    @Test
    public void Constructor_ShouldThrow_WhenCategoryLengthIsAbove10() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MotorcycleImpl("make", "model", 100, new String(new char[Constants.CATEGORY_LEN_MAX + 1])));
    }
    
}
