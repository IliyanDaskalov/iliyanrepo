package com.telerikacademy.oop.dealership.core.contracts;

public interface Reader {
    
    String readLine();
    
}
