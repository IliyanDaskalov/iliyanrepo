package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Constants;
import com.telerikacademy.oop.dealership.models.common.Utils;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;
import com.telerikacademy.oop.dealership.models.contracts.Comment;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;
import jdk.jshell.execution.Util;

import java.util.ArrayList;
import java.util.List;

public class VehicleBase implements Vehicle {

    private int wheels;
    private VehicleType type;
    private String make;
    private String model;
    private double price;
    private List<Comment> comments;

    public VehicleBase(String make, String model, double price, int wheels, VehicleType type) {
        setMake(make);
        setModel(model);
        setPrice(price);
        this.wheels = wheels;
        this.type = type;
        setComments(comments);
    }

    private void setMake(String make) {
        Validator.validateArgumentIsNotNull(make, "Make");
        Validator.validateLength(make, Constants.MAKE_NAME_LEN_MIN, Constants.MAKE_NAME_LEN_MAX, "Make must be between 2 and 15 characters long!\n####################");
        this.make = make;
    }

    private void setModel(String model) {
        Validator.validateArgumentIsNotNull(model, "Model");
        Validator.validateLength(model, Constants.MODEL_NAME_LEN_MIN, Constants.MODEL_NAME_LEN_MAX, "Model must be between 1 and 15 characters long!\n####################");
        this.model = model;
    }

    private void setPrice(double price) {
        Validator.validateDecimalRange(price, 0, 1000000, "Price must be between 0.0 and 1000000.0!\n####################");
        this.price = price;
    }

    private void setComments(List<Comment> comments) {
        this.comments = new ArrayList<>();
    }
    // TODO: 14.12.2020 г. add setters for wheels and type and put them in constructor

    @Override
    public int getWheels() {
        return wheels;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public String getMake() {
        return make;
    }


    @Override
    public String getModel() {
        return model;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }


    @Override
    public void addComment(Comment commentToAdd) {
        Validator.validateArgumentIsNotNull(commentToAdd, "Comment");
        comments.add(commentToAdd);
    }

    @Override
    public void removeComment(Comment commentToRemove) {
        comments.remove(commentToRemove);
    }


    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        String result = String.format("%s:%n" +
                        "  Make: %s%n" +
                        "  Model: %s%n" +
                        "  Wheels: %d%n" +
                        "  Price: $%s%n",
                type,
                make,
                model,
                wheels,
                Utils.removeTrailingZerosFromDouble(price));
        return result.trim();
    }
}
