package com.telerikacademy.oop.dealership.core.contracts;

import com.telerikacademy.oop.dealership.models.contracts.User;

import java.util.List;

public interface DealershipRepository {
    
    List<User> getUsers();
    
    User getLoggedUser();
    
    void setLoggedUser(User newUser);
    
    void addUser(User userToAdd);
    
}
