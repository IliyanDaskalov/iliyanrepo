package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Utils;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;
import com.telerikacademy.oop.dealership.models.contracts.Car;

public class CarImpl extends VehicleBase implements Car {

    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, 4, VehicleType.CAR);
        setSeats(seats);
    }

    private void setSeats(int seats) {
        Validator.validateIntRange(seats,1,10,"Seats must be between 1 and 10!\n####################");
        this.seats = seats;
    }

    @Override
    public int getSeats() {
        return seats;
    }

    @Override
    public String toString() {
        String result = String.format("%s%n  Seats: %d%n",super.toString(),seats);

        return result.trim();
    }


}
