package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Constants;
import com.telerikacademy.oop.dealership.models.common.Utils;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.Role;
import com.telerikacademy.oop.dealership.models.contracts.Car;
import com.telerikacademy.oop.dealership.models.contracts.Comment;
import com.telerikacademy.oop.dealership.models.contracts.User;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class UserImpl implements User {

    private String username;
    private String firstName;
    private String lastName;
    private String password;
    private Role role;
    private List<Vehicle> vehicles;
    private List<Comment> comments;

    public UserImpl(String username, String firstName, String lastName, String password, Role role) {
        setUsername(username);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        setRole(role);
        setVehicles(vehicles);
    }

    private void setUsername(String username) {
        Validator.validateArgumentIsNotNull(username, "Username");
        Validator.validateLength(username, Constants.USERNAME_LEN_MIN, Constants.USERNAME_LEN_MAX, "Username must be between 2 and 20 characters long!\n####################");
        Validator.validatePattern(username, "^[A-Za-z0-9]+$", "Username contains invalid symbols!\n####################");
        this.username = username;
    }

    private void setFirstName(String firstName) {
        Validator.validateArgumentIsNotNull(firstName, "Firstname");
        Validator.validateLength(firstName, Constants.FIRSTNAME_LEN_MIN, Constants.FIRSTNAME_LEN_MAX, "Firstname must be between 2 and 20 characters long!\n####################");
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        Validator.validateArgumentIsNotNull(lastName, "Lastname");
        Validator.validateLength(lastName, Constants.LASTNAME_LEN_MIN, Constants.LASTNAME_LEN_MAX, "Lastname must be between 2 and 20 characters long!\n####################");
        this.lastName = lastName;
    }

    private void setPassword(String password) {
        Validator.validateArgumentIsNotNull(password, "Password");
        Validator.validateLength(password, Constants.PASSWORD_LEN_MIN, Constants.PASSWORD_LEN_MAX, "Password must be between 5 and 30 characters long!\n####################");
        Validator.validatePattern(password, "^[A-Za-z0-9@*_-]+$", "Password contains invalid symbols!\n####################");
        this.password = password;
    }

    private void setRole(Role role) {
        this.role = role;
    }

    private void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = new ArrayList<>();
    }


    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public List<Vehicle> getVehicles() {
        return new ArrayList<>(vehicles);
    }

    @Override
    public void addVehicle(Vehicle vehicle) {
        Validator.validateArgumentIsNotNull(vehicle, "Vehicles");

        if (role.equals(Role.ADMIN)) {
            throw new IllegalArgumentException("You are an admin and therefore cannot add vehicles!\n####################");
        }
        if (!role.equals(Role.VIP) && vehicles.size() >= 5) {
            throw new IllegalArgumentException("You are not VIP and cannot add more than 5 vehicles!\n####################");
        }

        vehicles.add(vehicle);
    }

    @Override
    public void removeVehicle(Vehicle vehicle) {
        Validator.validateArgumentIsNotNull(vehicle, "Vehicles");
        vehicles.remove(vehicle);
    }

    @Override
    public void addComment(Comment commentToAdd, Vehicle vehicleToAddComment) {
        Validator.validateArgumentIsNotNull(commentToAdd, "Comment");
        Validator.validateArgumentIsNotNull(vehicleToAddComment, "Vehicle");

        vehicleToAddComment.addComment(commentToAdd);

    }

    @Override
    public void removeComment(Comment commentToRemove, Vehicle vehicleToRemoveComment) {
        Validator.validateArgumentIsNotNull(commentToRemove, "Comment");
        vehicleToRemoveComment.removeComment(commentToRemove);

    }

    @Override
    public String printVehicles() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("--USER %s--", username)).append(System.lineSeparator());
        int counter = 1;

        for (Vehicle vehicle : vehicles) {
            builder.append(String.format("%d. %s%n", counter, vehicle.toString()));
            counter++;

            if (vehicle.getComments().isEmpty()) {
                builder.append(String.format("    --NO COMMENTS--%n"));
            } else {
                builder.append(String.format("    --COMMENTS--%n" +
                        "    ----------%n"));
            }
        }
        return builder.toString().trim();
    }

}
