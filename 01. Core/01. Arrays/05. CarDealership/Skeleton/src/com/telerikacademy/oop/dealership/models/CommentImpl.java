package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Constants;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.contracts.Comment;


public class CommentImpl implements Comment {

    private String content;
    private String author;

    public CommentImpl(String content, String author) {
        setContent(content);
        setAuthor(author);
    }

    private void setContent(String content) {
        Validator.validateArgumentIsNotNull(content,"Content");
        Validator.validateLength(content, Constants.CONTENT_LEN_MIN,Constants.CONTENT_LEN_MAX,"Content must be between 3 and 200 characters long!\n####################");
        this.content = content;
    }

    private void setAuthor(String author) {
        this.author = author;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getAuthor() {
        return author;
    }
}
