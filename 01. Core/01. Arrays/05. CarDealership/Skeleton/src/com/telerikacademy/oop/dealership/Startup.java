package com.telerikacademy.oop.dealership;

import com.telerikacademy.oop.dealership.core.DealershipEngineImpl;

import java.io.ByteArrayInputStream;

public class Startup {

    public static void userInput() {
        String input = "RegisterUser p Petar Petrov 123456\n" +
                "RegisterUser pesh0= Petar Petrov 123456\n" +
                "RegisterUser pesh0 Petar Petrov 1234\n" +
                "RegisterUser pesh0 Petar P 123456\n" +
                "RegisterUser pesh0 P Petrov 123456\n" +
                "RegisterUser pesho Petar Petrov 123456\n" +
                "AddVehicle Motorcycle K Z1000 9999 Race\n" +
                "AddVehicle Motorcycle Kawasaki Z1000 -1000 Race\n" +
                "AddVehicle Motorcycle Kawasaki Z1000 9999 N\n" +
                "AddVehicle Car Opel Vectra 5000 -1\n" +
                "AddVehicle Truck Volvo FH4 11800 200\n" +
                "AddVehicle Motorcycle Kawasaki Z 9999 Race\n" +
                "AddVehicle Car Opel Vectra 5000 5\n" +
                "AddVehicle Car Mazda 6 10000 5\n" +
                "AddVehicle Motorcycle Suzuki V-Strom 7500 CityEnduro\n" +
                "AddVehicle Car BMW Z3 11200 2\n" +
                "AddVehicle Car BMW Z3 11200 2\n" +
                "AddVehicle Car BMW Z3 11200 2\n" +
                "AddComment {{U}} pesho 1\n" +
                "AddComment {{Amazing speed and handling!}} pesho 1\n" +
                "ShowUsers\n" +
                "RegisterUser pesho Petar Petrov 123457\n" +
                "Logout\n" +
                "RegisterUser pesho Petar Petrov 123457\n" +
                "RegisterUser gosho Georgi Georgiev 123457 VIP\n" +
                "Logout\n" +
                "Login pesho 123456\n" +
                "Login gosho 123457\n" +
                "Logout\n" +
                "Login gosho 123457\n" +
                "AddComment {{I like this one! It is faster than all the rest!}} pesho 1\n" +
                "RemoveComment 1 1 pesho\n" +
                "RemoveComment 2 5 pesho\n" +
                "AddVehicle Motorcycle Suzuki GSXR1000 8000 Racing\n" +
                "AddVehicle Car Skoda Fabia 2000 5\n" +
                "AddVehicle Car BMW 535i 7200 5\n" +
                "AddVehicle Motorcycle Honda Hornet600 4150 Race\n" +
                "AddVehicle Car Mercedes S500L 15000 5\n" +
                "AddVehicle Car Opel Zafira 8000 5\n" +
                "AddVehicle Car Opel Zafira 7450 5\n" +
                "AddVehicle Truck Volvo FH4 11800 40\n" +
                "ShowUsers\n" +
                "Logout\n" +
                "RegisterUser ivancho Ivan Ivanov admin Admin\n" +
                "AddVehicle Car Skoda Fabia 2000 5\n" +
                "ShowUsers\n" +
                "ShowVehicles gosho\n" +
                "ShowVehicles ivanch0\n" +
                "AddComment {{Empty comment}} pencho 1\n" +
                "AddComment {{Empty comment}} pesho 20\n" +
                "RemoveComment 1 1 pesho\n" +
                "ShowVehicles pesho\n" +
                "Logout\n" +
                "Login pesho 123456\n" +
                "AddComment {{I dream of having this one one day.}} pesho 1\n" +
                "Logout\n" +
                "Login ivancho admin\n" +
                "AddComment {{What is the mileage on it?}} pesho 3\n" +
                "Logout\n" +
                "Login pesho 123456\n" +
                "AddComment {{This one passed my by on the highway today. So pretty!}} pesho 3\n" +
                "ShowVehicles pesho\n" +
                "ShowVehicles gosho\n" +
                "ShowVehicles ivancho\n" +
                "Logout\n" +
                "Login gosho 123457\n" +
                "RemoveComment 1 2 pesho\n" +
                "ShowVehicles pesho\n" +
                "Logout\n" +
                "Login pesho 123456\n" +
                "RemoveVehicle 1\n" +
                "ShowVehicles pesho\n" +
                "Exit\n";
        ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
    }


    public static void main(String[] args) {
        userInput();
        DealershipEngineImpl engine = new DealershipEngineImpl();
        engine.start();
    }
    
}
