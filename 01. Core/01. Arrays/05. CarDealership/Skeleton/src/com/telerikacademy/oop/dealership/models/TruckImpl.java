package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;
import com.telerikacademy.oop.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {

    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, 8, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }

    private void setWeightCapacity(int weightCapacity) {
        Validator.validateIntRange(weightCapacity,1,100,"Weight capacity must be between 1 and 100!\n####################");
        this.weightCapacity = weightCapacity;
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }
    @Override
    public String toString() {
        String result = String.format("%s%n  Weight Capacity: %dt%n",super.toString(),weightCapacity);

        return result.trim();
    }


}
