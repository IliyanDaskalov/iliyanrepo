package com.telerikacademy.oop.dealership.models;

import com.telerikacademy.oop.dealership.models.common.Constants;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.common.enums.VehicleType;
import com.telerikacademy.oop.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, 2, VehicleType.MOTORCYCLE);
        setCategory(category);
    }

    private void setCategory(String category) {
        Validator.validateArgumentIsNotNull(category, "Category");
        Validator.validateLength(category, Constants.CATEGORY_LEN_MIN, Constants.CATEGORY_LEN_MAX, "Category must be between 3 and 10 characters long!\n####################");
        this.category = category;
    }

    @Override
    public String getCategory() {
        return category;
    }
    @Override
    public String toString() {
        String result = String.format("%s%n  Category: %s%n",super.toString(),category);

        return result.trim();
    }


}
