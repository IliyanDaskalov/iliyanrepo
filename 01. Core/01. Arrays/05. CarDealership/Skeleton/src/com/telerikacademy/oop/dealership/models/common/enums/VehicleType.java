package com.telerikacademy.oop.dealership.models.common.enums;

public enum VehicleType {
    MOTORCYCLE,
    CAR,
    TRUCK;
}


