package com.telerikacademy.oop.dealership.core.factories;

import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.models.*;
import com.telerikacademy.oop.dealership.models.common.enums.Role;
import com.telerikacademy.oop.dealership.models.contracts.*;

public class DealershipFactoryImpl implements DealershipFactory {

    public Car createCar(String make, String model, double price, int seats) {
        return new CarImpl(make, model, price, seats);
    }

    public Motorcycle createMotorcycle(String make, String model, double price, String category) {
        return new MotorcycleImpl(make, model, price, category);
    }

    public Truck createTruck(String make, String model, double price, int weightCapacity) {
        return new TruckImpl(make, model, price, weightCapacity);
    }

    public User createUser(String username, String firstName, String lastName, String password, String role) {
        return new UserImpl(username, firstName, lastName, password, Role.valueOf(role.toUpperCase()));
    }

    public Comment createComment(String content, String author) {
        return new CommentImpl(content, author);
    }

}
