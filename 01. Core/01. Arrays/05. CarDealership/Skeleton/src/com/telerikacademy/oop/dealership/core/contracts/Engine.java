package com.telerikacademy.oop.dealership.core.contracts;

public interface Engine {
    
    void start();
    
}
