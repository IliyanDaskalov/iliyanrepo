package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.contracts.User;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.commands.constants.CommandConstants;

import java.util.List;

public class RemoveVehicle implements Command {

    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;

    public RemoveVehicle(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        int vehicleIndex = Integer.parseInt(parameters.get(0)) - 1;
        return removeVehicle(vehicleIndex);
    }

    private String removeVehicle(int vehicleIndex) {
        User loggedUser = dealershipRepository.getLoggedUser();
        Validator.validateIntRange(vehicleIndex, 0, loggedUser.getVehicles().size(), CommandConstants.REMOVED_VEHICLE_DOES_NOT_EXIST);

        Vehicle vehicle = loggedUser.getVehicles().get(vehicleIndex);

        loggedUser.removeVehicle(vehicle);

        return String.format(CommandConstants.VEHICLE_REMOVED_SUCCESSFULLY, loggedUser.getUsername());
    }

}
