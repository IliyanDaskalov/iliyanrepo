package com.telerikacademy.oop.dealership.core;

import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.models.contracts.User;

import java.util.ArrayList;
import java.util.List;

public class DealershipRepositoryImpl implements DealershipRepository {
    
    private final List<User> users;
    private User loggedUser;

    public DealershipRepositoryImpl() {
        this.users = new ArrayList<>();
        this.loggedUser = null;
    }
    
    public List<User> getUsers() {
        return new ArrayList<>(users);
    }
    
    public void addUser(User userToAdd) {
        this.users.add(userToAdd);
    }
    
    public User getLoggedUser() {
        return loggedUser;
    }
    
    public void setLoggedUser(User newUser) {
        this.loggedUser = newUser;
    }
    
}
