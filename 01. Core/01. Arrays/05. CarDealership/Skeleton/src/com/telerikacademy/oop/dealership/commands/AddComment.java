package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.models.common.Validator;
import com.telerikacademy.oop.dealership.models.contracts.Comment;
import com.telerikacademy.oop.dealership.models.contracts.User;
import com.telerikacademy.oop.dealership.models.contracts.Vehicle;
import com.telerikacademy.oop.dealership.commands.constants.CommandConstants;

import java.util.List;

public class AddComment implements Command {
    
    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;
    
    public AddComment(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        String content = parameters.get(0);
        String author = parameters.get(1);
        int vehicleIndex = Integer.parseInt(parameters.get(2)) - 1;
        
        return addComment(content, vehicleIndex, author);
    }
    
    private String addComment(String content, int vehicleIndex, String author) {
        Comment comment = dealershipFactory.createComment(content, dealershipRepository.getLoggedUser().getUsername());
        User user = dealershipRepository.getUsers().stream().filter(u -> u.getUsername().toLowerCase().equals(author.toLowerCase())).findFirst().orElse(null);
        
        if (user == null) {
            return String.format(CommandConstants.NO_SUCH_USER, author);
        }
        
        Validator.validateIntRange(vehicleIndex, 0, user.getVehicles().size(), CommandConstants.VEHICLE_DOES_NOT_EXIST);
        
        Vehicle vehicle = user.getVehicles().get(vehicleIndex);
        
        dealershipRepository.getLoggedUser().addComment(comment, vehicle);
        
        return String.format(CommandConstants.COMMENT_ADDED_SUCCESSFULLY, dealershipRepository.getLoggedUser().getUsername());
    }
    
}
