package com.telerikacademy.oop.dealership.commands;

import com.telerikacademy.oop.dealership.commands.contracts.Command;
import com.telerikacademy.oop.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.oop.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.oop.dealership.models.contracts.User;

import java.util.List;

import static com.telerikacademy.oop.dealership.commands.constants.CommandConstants.*;

public class Login implements Command {
    
    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;
    
    public Login(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        String username = parameters.get(0);
        String password = parameters.get(1);
        
        return login(username, password);
    }
    
    private String login(String username, String password) {
        if (dealershipRepository.getLoggedUser() != null) {
            return String.format(USER_LOGGED_IN_ALREADY, dealershipRepository.getLoggedUser().getUsername());
        }
        
        User userFound = dealershipRepository
                .getUsers()
                .stream()
                .filter(user -> user.getUsername().toLowerCase()
                        .equals(username.toLowerCase()))
                .findFirst()
                .orElse(null);
        
        if (userFound != null && userFound.getPassword().equals(password)) {
            dealershipRepository.setLoggedUser(userFound);
            return String.format(USER_LOGGED_IN, username);
        }
        
        return WRONG_USERNAME_OR_PASSWORD;
    }
    
}
