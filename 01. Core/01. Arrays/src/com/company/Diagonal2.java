package com.company;

import java.util.Scanner;

public class Diagonal2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        long [][] numbers = new long[n][n];

        for (int row = 0; row < n; row++) {
            for (int  col = 0; col < n; col++) {
                numbers[row][col] = (long) Math.pow(2, col + row);
            }
        }

        long result = 0;

        for (int  row = 0; row < n ; row++) {
            for (int  col = row; col < n; col++) {
                result += numbers[row][col];
            }
        }
        System.out.println(result);
    }
}
