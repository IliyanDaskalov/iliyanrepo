package com.company;

import java.util.Scanner;

public class LongestSequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        int[] numbers = new int[n];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(scanner.nextLine());
        }

        int counter = 1;
        int temp = 0;

        for (int i = 0; i < numbers.length - 1; i++) {

            if (numbers[i] == numbers[i + 1]) {
                counter ++;
                if (temp < counter) {
                    temp = counter;
                }
            } else {
                counter = 1;
            }

        }
        System.out.println(temp);
    }
}
