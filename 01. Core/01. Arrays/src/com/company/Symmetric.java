package com.company;

import java.util.Scanner;

public class Symmetric {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        String result = "";
        for (int i = 0; i < n; i++) {
            String input = scanner.nextLine();
            String[] array = input.split(" ");

            boolean check = true;

            for (int j = 0; j < array.length/2; j++) {
                if (!(array[j].equals(array[(array.length - j) - 1]))) {
                    check = false;
                    break;
                }
            }

            if(check){
                result += "Yes\n";
            } else {
                result += "No\n";
            }

        }
        System.out.println(result);
    }
}
