package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class ThreeGroups {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        String[] array = input.split(" ");
        int[] numbers = new int[array.length];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(array[i]);
        }

        ArrayList<Integer> remainder0  = new ArrayList<>();
        ArrayList<Integer> remainder1  = new ArrayList<>();
        ArrayList<Integer> remainder2  = new ArrayList<>();
        for (int number : numbers) {
            if (number % 3 == 0) {
                remainder0.add(number);
            } else if (number % 3 == 1) {
                remainder1.add(number);
            } else if (number % 3 == 2) {
                remainder2.add(number);
            }
        }
        for (int i = 0; i < remainder0.size(); i++) {
            System.out.print(remainder0.get(i) + " ");
        }
        System.out.println();
        for (int i = 0; i < remainder1.size(); i++) {
            System.out.print(remainder1.get(i) + " ");
        }
        System.out.println();
        for (int i = 0; i < remainder2.size(); i++) {
            System.out.print(remainder2.get(i) + " ");
        }

    }
}
