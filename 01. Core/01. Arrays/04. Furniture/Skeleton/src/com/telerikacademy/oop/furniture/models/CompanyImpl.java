package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Company;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.Constants;

import java.util.ArrayList;
import java.util.List;

public class CompanyImpl implements Company {


    private String name;
    private String registrationNumber;
    private List<Furniture> furnitures;

    public CompanyImpl(String name, String registrationNumber) {
        setName(name);
        setRegistrationNumber(registrationNumber);
        setFurnitures(furnitures);
    }

    private void setName(String name) {
        Validator.validateArgumentIsNotNull(name, "Name");
        Validator.validateLength(name,
                Constants.COMPANY_NAME_MIN_LENGTH,
                Constants.COMPANY_NAME_ERROR_MSG);
        this.name = name;
    }

    private void setRegistrationNumber(String registrationNumber) {

        if (registrationNumber.length() != Constants.REGISTRATION_LENGTH) {
            throw new IllegalArgumentException(Constants.COMPANY_REGISTRATION_NUMBER_ERROR_MSG);
        }
        if (!registrationNumber.chars().allMatch(Character::isDigit)) {
            throw new IllegalArgumentException(Constants.COMPANY_REGISTRATION_NUMBER_ERROR_MSG);
        }

        this.registrationNumber = registrationNumber;
    }

    private void setFurnitures(List<Furniture> furnitures) {
        this.furnitures = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public List<Furniture> getFurnitures() {
        return new ArrayList<>(furnitures);
    }

    public void add(Furniture furniture) {
        furnitures.add(furniture);
    }

    public String catalog() {
        StringBuilder strBuilder = new StringBuilder();

        strBuilder.append(String.format(
                "%s - %s - %s %s",
                name,
                registrationNumber,
                furnitures.isEmpty() ? "no" : String.format("%d", furnitures.size()),
                furnitures.size() == 1 ? "furniture" : "furnitures")).append(System.lineSeparator());

        for (Furniture furniture : furnitures) {
            strBuilder.append(String.format("%s%n", furniture.toString()));
        }

        return strBuilder.toString().trim();
    }

    public Furniture find(String model) {
        Validator.validateArgumentIsNotNull(model, "Find model");
        for (Furniture furniture : furnitures) {
            if (model.equals(furniture.getModel())) {
                return furniture;
            }
        }
        return null;
    }

    public void remove(Furniture furniture) {
        furnitures.remove(furniture);
    }

}
