package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.oop.furniture.models.enums.Constants;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;


public class ConvertibleChairImpl extends ChairImpl implements ConvertibleChair {

    private boolean isConverted;
    private final double initialHeight;

    public ConvertibleChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs){
        super(model, materialType, price, height, numberOfLegs);
        this.initialHeight = height;
    }

    @Override
    public boolean getConverted() {
        return isConverted;
    }

    @Override
    public void convert() {

        if(!isConverted){
            this.isConverted = true;
            super.height = Constants.CONVERTED_CHAIR_HEIGHT;
        } else {
            this.isConverted = false;
            super.height = initialHeight;
        }
    }

    @Override
    public String toString() {
        return String.format("%s, State: %s",super.toString() ,isConverted ? "Converted" : "Normal");
    }
}
