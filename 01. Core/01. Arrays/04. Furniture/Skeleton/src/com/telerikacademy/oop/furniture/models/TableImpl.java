package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Table;
import com.telerikacademy.oop.furniture.models.enums.Constants;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class TableImpl extends FurnitureImpl implements Table {

    private double length;
    private double width;

    public TableImpl(String model, MaterialType materialType, double price, double height, double length, double width) {
        super(model, materialType, price, height);
        setLength(length);
        setWidth(width);
    }

    private void setLength(double length) {
        Validator.validateDecimalRange(length, Constants.TABLE_LENGTH_ERROR_MSG);
        this.length = length;
    }

    private void setWidth(double width) {
        Validator.validateDecimalRange(width, Constants.TABLE_WIDTH_ERROR_MSG);
        this.width = width;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getArea() {
        return length * width;
    }

    @Override
    public String toString() {
        return String.format("%sLength: %.2f, Width: %.2f, Area: %.4f", super.toString(), length, width, getArea());

    }
}
