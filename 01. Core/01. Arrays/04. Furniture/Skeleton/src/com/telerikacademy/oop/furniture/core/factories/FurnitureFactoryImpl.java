package com.telerikacademy.oop.furniture.core.factories;

import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.models.*;
import com.telerikacademy.oop.furniture.models.contracts.Chair;
import com.telerikacademy.oop.furniture.models.contracts.Company;
import com.telerikacademy.oop.furniture.models.contracts.Table;
import com.telerikacademy.oop.furniture.models.enums.ChairType;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class FurnitureFactoryImpl implements FurnitureFactory {

    @Override
    public Company createCompany(String name, String registrationNumber) {
        return new CompanyImpl(name, registrationNumber);
    }

    @Override
    public Table createTable(String model, String materialType, double price, double height, double length, double width) {
        return new TableImpl(model, getMaterialType(materialType), price, height, length, width);
    }


    @Override
    public Chair createChair(String type, String model, String material, double price, double height, int numberOfLegs) {

        if (type.equalsIgnoreCase(ChairType.ADJUSTABLE.toString())) {
            return new AdjustableChairImpl(model, getMaterialType(material), price, height, numberOfLegs);
        } else if (type.equalsIgnoreCase(ChairType.CONVERTIBLE.toString())) {
            return new ConvertibleChairImpl(model, getMaterialType(material), price, height, numberOfLegs);
        }
        return new ChairImpl(model, getMaterialType(material), price, height, numberOfLegs);
    }

    private MaterialType getMaterialType(String material) {
        return MaterialType.valueOf(material.toUpperCase());
    }



}
