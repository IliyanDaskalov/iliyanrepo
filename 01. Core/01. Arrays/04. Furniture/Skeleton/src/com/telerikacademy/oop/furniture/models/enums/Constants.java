package com.telerikacademy.oop.furniture.models.enums;

public class Constants {

    public static final int FURNITURE_MODEL_MIN_NAME_LENGTH = 3;

    public static final int CHAIR_MIN_LEGS = 0;

    public static final double CONVERTED_CHAIR_HEIGHT = 0.10;

    public static final int COMPANY_NAME_MIN_LENGTH = 5;

    public static final int REGISTRATION_LENGTH = 10;


    public static final String FURNITURE_MODEL_ERROR_MSG =
            String.format("Model should be more than %d symbols.", FURNITURE_MODEL_MIN_NAME_LENGTH);
    public static final String FURNITURE_PRICE_ERROR_MSG = "Price should be positive.";
    public static final String FURNITURE_HEIGHT_ERROR_MSG = "Height cannot be negative.";
    public static final String TABLE_LENGTH_ERROR_MSG = "Length should be greater than 0.";
    public static final String TABLE_WIDTH_ERROR_MSG = "Width should be greater than 0.";
    public static final String CHAIR_LEGS_ERROR_MSG = "Numbers of chair legs should be greater than 0.";
    public static final String COMPANY_NAME_ERROR_MSG =
            String.format("Company name should be at least %d symbols.",COMPANY_NAME_MIN_LENGTH);
    public static final String COMPANY_REGISTRATION_NUMBER_ERROR_MSG = "Registration number is not valid";
}
