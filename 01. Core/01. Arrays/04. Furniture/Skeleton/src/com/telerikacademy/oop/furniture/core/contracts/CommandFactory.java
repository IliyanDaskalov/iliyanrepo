package com.telerikacademy.oop.furniture.core.contracts;

import com.telerikacademy.oop.furniture.commands.contracts.Command;

public interface CommandFactory {
    
    Command createCommand(String commandTypeAsString, FurnitureFactory factory, FurnitureRepository agencyRepository);
    
}
