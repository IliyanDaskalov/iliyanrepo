package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;

import java.util.List;

public class ShowCompanyCatalog implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    public ShowCompanyCatalog(FurnitureRepository furnitureRepository, FurnitureFactory furnitureFactory) {
        this.furnitureRepository = furnitureRepository;
        this.furnitureFactory = furnitureFactory;
    }

    private final FurnitureRepository furnitureRepository;
    private final FurnitureFactory furnitureFactory;

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS);
        }
        String companyToShowCatalog = parameters.get(0);
        return showCompanyCatalog(companyToShowCatalog);
    }

    private String showCompanyCatalog(String companyName) {
        if (!furnitureRepository.getCompanies().containsKey(companyName)) {
            return String.format(CommandConstants.COMPANY_NOT_FOUND_ERROR_MESSAGE, companyName);
        }

        return furnitureRepository.getCompanies().get(companyName).catalog();
    }

}
