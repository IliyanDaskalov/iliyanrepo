package com.telerikacademy.oop.furniture.core;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.contracts.*;
import com.telerikacademy.oop.furniture.core.factories.CommandFactoryImpl;
import com.telerikacademy.oop.furniture.core.factories.FurnitureFactoryImpl;
import com.telerikacademy.oop.furniture.core.providers.CommandParserImpl;
import com.telerikacademy.oop.furniture.core.providers.ConsoleReader;
import com.telerikacademy.oop.furniture.core.providers.ConsoleWriter;

import java.util.List;

public class FurnitureEngineImpl implements Engine {

    private static final String TERMINATION_COMMAND = "Exit";
    private static final String ERROR_EMPTY_COMMAND = "Command cannot be null or empty.";

    private final FurnitureFactory furnitureFactory;
    private final CommandParser commandParser;
    private final FurnitureRepository furnitureRepository;
    private final Writer writer;
    private final Reader reader;
    private final CommandFactory commandFactory;

    public FurnitureEngineImpl() {
        furnitureFactory = new FurnitureFactoryImpl();
        commandParser = new CommandParserImpl();
        writer = new ConsoleWriter();
        reader = new ConsoleReader();
        commandFactory = new CommandFactoryImpl();
        furnitureRepository = new FurnitureRepositoryImpl();
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);

            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException(ERROR_EMPTY_COMMAND);
        }

        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandName, furnitureFactory, furnitureRepository);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }

}
