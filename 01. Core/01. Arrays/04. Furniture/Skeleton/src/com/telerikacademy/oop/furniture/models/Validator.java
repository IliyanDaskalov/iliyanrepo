package com.telerikacademy.oop.furniture.models;

public class Validator {

    private Validator() {
    }

    public static void validateIntRange(int value, int min, String message) {
        if (value <= min ) {
            throw new IllegalArgumentException(message);
        }
    }


    public static void validateArgumentIsNotNull(Object arg, String message) {
        if (arg == null) {
            throw new IllegalArgumentException(message + " cannot be null!");
        }
    }


    public static void validateLength(String input, int min, String message) {
        String trimmedInput = input.trim();

        if (trimmedInput.length() < min)
            throw new IllegalArgumentException(message);
    }

    public static void validateDecimalRange(double value, String message) {
        if (value <= 0.00) {
            throw new IllegalArgumentException(message);
        }
    }


}
