package com.telerikacademy.oop.furniture.models.enums;

public enum ChairType {
    NORMAL,
    ADJUSTABLE,
    CONVERTIBLE;
}
