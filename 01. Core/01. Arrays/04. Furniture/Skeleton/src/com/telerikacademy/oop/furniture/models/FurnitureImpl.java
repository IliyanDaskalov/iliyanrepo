package com.telerikacademy.oop.furniture.models;

import com.sun.source.tree.BinaryTree;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.Constants;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public abstract class FurnitureImpl implements Furniture {

    private String model;
    private MaterialType materialType;
    private double price;
    protected double height;

    public FurnitureImpl(String model, MaterialType materialType, double price, double height) {
        setModel(model);
        setMaterialType(materialType);
        setPrice(price);
        setHeight(height);
    }

    private void setModel(String model) {
        Validator.validateArgumentIsNotNull(model, "Model");
        Validator.validateLength(model,
                Constants.FURNITURE_MODEL_MIN_NAME_LENGTH,
                Constants.FURNITURE_MODEL_ERROR_MSG);

        this.model = model;
    }

    private void setMaterialType(MaterialType materialType) {
        Validator.validateArgumentIsNotNull(materialType, "Material type");
        this.materialType = materialType;
    }

    private void setPrice(double price) {
        Validator.validateDecimalRange(price, Constants.FURNITURE_PRICE_ERROR_MSG);
        this.price = price;
    }

    private void setHeight(double height) {
        Validator.validateDecimalRange(height, Constants.FURNITURE_HEIGHT_ERROR_MSG);
        this.height = height;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public MaterialType getMaterialType() {
        return materialType;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public double getHeight() {
        return height;
    }



    @Override
    public String toString() {
        return String.format(
                "Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f, ",
                this.getClass().getSimpleName().replace("Impl", ""),
                model,
                materialType,
                price,
                height);
    }

}
