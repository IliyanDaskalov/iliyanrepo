package com.telerikacademy.oop.furniture.core.contracts;

public interface Reader {
    
    String readLine();
    
}
