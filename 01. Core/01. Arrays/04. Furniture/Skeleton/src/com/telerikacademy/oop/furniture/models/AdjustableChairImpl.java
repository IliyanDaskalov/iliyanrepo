package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.oop.furniture.models.enums.Constants;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class AdjustableChairImpl extends ChairImpl implements AdjustableChair {

    public AdjustableChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs){
        super(model, materialType, price, height, numberOfLegs);
    }

    @Override
    public void setHeight(double height) {
        Validator.validateDecimalRange(height, Constants.FURNITURE_HEIGHT_ERROR_MSG);
        super.height = height;
    }
}
