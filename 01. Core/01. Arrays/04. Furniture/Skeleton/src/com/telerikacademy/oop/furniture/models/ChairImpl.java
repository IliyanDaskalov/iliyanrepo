package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Chair;
import com.telerikacademy.oop.furniture.models.enums.ChairType;
import com.telerikacademy.oop.furniture.models.enums.Constants;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class ChairImpl extends FurnitureImpl implements Chair {

    private int numberOfLegs;
    private ChairType type;


    public ChairImpl(String model, MaterialType materialType, double price, double height, int numberOfLegs) {
        super(model, materialType, price, height);
        setNumberOfLegs(numberOfLegs);
    }

    private void setNumberOfLegs(int numberOfLegs) {
        Validator.validateIntRange(numberOfLegs,
                Constants.CHAIR_MIN_LEGS,
                Constants.CHAIR_LEGS_ERROR_MSG);
        this.numberOfLegs = numberOfLegs;
    }

    private void setType(ChairType type) {
        this.type = ChairType.NORMAL;
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    @Override
    public String toString() {
        return String.format("%sLegs: %d", super.toString(),numberOfLegs);
    }
}
