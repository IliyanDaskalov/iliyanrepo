package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.FurnitureRepositoryImpl;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.oop.furniture.core.factories.FurnitureFactoryImpl;
import com.telerikacademy.oop.furniture.models.ConvertibleChairImpl;
import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class ConvertChair_Tests {
    
    private Command testCommand;
    private Furniture testFurniture;
    private FurnitureRepository furnitureRepository;
    private FurnitureFactory furnitureFactory;
    
    @BeforeEach
    public void before() {
        furnitureFactory = new FurnitureFactoryImpl();
        furnitureRepository = new FurnitureRepositoryImpl();
        testCommand = new ConvertChair(furnitureRepository, furnitureFactory);
    }
    
    @Test
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange, Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(emptyList()));
    }
    
    @Test
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange, Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(asList(new String[2])));
    }
    
    @Test
    public void execute_should_convertChair_when_inputIsValid() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("model");
        testFurniture = new ConvertibleChairImpl("model", MaterialType.LEATHER, 4, 4, 4);
        furnitureRepository.addFurniture("model", testFurniture);
        
        // Act
        testCommand.execute(testList);
        
        // Assert
        Assertions.assertTrue(((ConvertibleChair) testFurniture).getConverted());
        
    }
    
}
