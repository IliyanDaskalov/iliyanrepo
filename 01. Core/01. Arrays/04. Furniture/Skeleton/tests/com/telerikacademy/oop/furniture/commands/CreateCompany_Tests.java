package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.FurnitureRepositoryImpl;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.oop.furniture.core.factories.FurnitureFactoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class CreateCompany_Tests {
    
    private Command testCommand;
    private FurnitureRepository furnitureRepository;
    private FurnitureFactory furnitureFactory;
    
    @BeforeEach
    public void before() {
        furnitureFactory = new FurnitureFactoryImpl();
        furnitureRepository = new FurnitureRepositoryImpl();
        testCommand = new CreateCompany(furnitureRepository, furnitureFactory);
    }
    
    @Test
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange, Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(emptyList()));
    }
    
    @Test
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange, Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(asList(new String[3])));
    }
    
    @Test
    public void execute_should_createCompany_when_inputIsValid() {
        // Arrange, Act
        testCommand.execute(asList("company", "1234567890"));
        
        // Assert
        Assertions.assertEquals(1, furnitureRepository.getCompanies().size());
    }
    
}
