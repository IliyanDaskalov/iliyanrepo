package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AdjustableChair_Tests {
    
    @Test
    public void setHeight_should_adjustChairHeight() {
        
        // Arrange
        AdjustableChair adjChair = new AdjustableChairImpl("model", MaterialType.LEATHER, 3, 3, 4);
        
        // Act
        adjChair.setHeight(7);
        
        // Assert
        Assertions.assertEquals(7, adjChair.getHeight(), 0.01);
    }
    
    @Test
    public void setHeight_should_throwError_when_heightLessThanZero() {
        
        // Arrange
        AdjustableChair adjChair = new AdjustableChairImpl("model", MaterialType.LEATHER, 2, 2, 4);
        
        //Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> adjChair.setHeight(-1));
        
    }
    
}
