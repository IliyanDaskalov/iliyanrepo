package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FurnitureBase_Tests {
    
    @Test
    public void constructor_should_throwError_when_modelIsNull() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ChairImpl(null, MaterialType.LEATHER, 2, 2, 4));
    }
    
    @Test
    public void constructor_should_throwError_when_modelLengthLessThanMinimum() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ChairImpl("T", MaterialType.LEATHER, 2, 2, 4));
    }
    
    @Test
    public void constructor_should_throwError_when_heightIsLessThanZero() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ChairImpl("Test", MaterialType.LEATHER, 2, -2, 4));
    }
    
    @Test
    public void constructor_should_throwError_when_priceIsLessThanZero() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ChairImpl("Test", MaterialType.LEATHER, -2, 2, 4));
    }
    
    
}
