package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.FurnitureRepositoryImpl;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.oop.furniture.core.factories.FurnitureFactoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;

public class CreateChair_Tests {
    
    private Command testCommand;
    private FurnitureRepository furnitureRepository;
    private FurnitureFactory furnitureFactory;
    
    @BeforeEach
    public void before() {
        furnitureFactory = new FurnitureFactoryImpl();
        furnitureRepository = new FurnitureRepositoryImpl();
        testCommand = new CreateChair(furnitureRepository, furnitureFactory);
    }
    
    @Test
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange, Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(asList(new String[5])));
    }
    
    @Test
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange, Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(asList(new String[7])));
    }
    
    @Test
    public void execute_should_createChair_when_inputIsValid() {
        // Arrange, Act
        testCommand.execute(asList("model", "leather", "4", "5", "3", "normal"));
        
        // Assert
        Assertions.assertEquals(1, furnitureRepository.getFurnitures().size());
        
    }
    
}
