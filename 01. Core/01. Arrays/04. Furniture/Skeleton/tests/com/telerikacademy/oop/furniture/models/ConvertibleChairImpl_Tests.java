package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConvertibleChairImpl_Tests {
    
    @Test
    public void convert_should_convertFalseToTrue() {
        // Arrange
        ConvertibleChair convChair = new ConvertibleChairImpl("model", MaterialType.LEATHER, 3, 3, 4);
        
        // Act
        convChair.convert();
        
        // Assert
        Assertions.assertTrue(convChair.getConverted());
    }
    
    @Test
    public void convert_should_convertTrueToFalse() {
        // Arrange
        ConvertibleChair convChair = new ConvertibleChairImpl("model", MaterialType.LEATHER, 3, 3, 4);
        convChair.convert();
        
        // Act
        convChair.convert();
        
        // Assert
        Assertions.assertFalse(convChair.getConverted());
    }
    
    @Test
    public void convert_should_convertHeightCorrectly() {
        // Arrange
        ConvertibleChair convChair = new ConvertibleChairImpl("model", MaterialType.LEATHER, 3, 3, 4);
        
        
        // Act
        convChair.convert();
        
        // Assert
        double expected = 0.1;
        Assertions.assertEquals(expected, convChair.getHeight(), 0.01);
    }
    
    @Test
    public void convert_should_restoreOriginalHeight() {
        // Arrange
        ConvertibleChair convChair = new ConvertibleChairImpl("model", MaterialType.LEATHER, 3, 3, 4);
        convChair.convert();
        
        // Act
        convChair.convert();
        
        // Assert
        Assertions.assertEquals(3, convChair.getHeight(), 0.01);
    }
    
}
