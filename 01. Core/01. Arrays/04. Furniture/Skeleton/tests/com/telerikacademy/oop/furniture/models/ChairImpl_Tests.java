package com.telerikacademy.oop.furniture.models;


import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ChairImpl_Tests {
    
    @Test
    public void constructor_should_throwError_when_numberOfLegsIsLessThanZero() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ChairImpl("model", MaterialType.LEATHER, 2, 2, -4));
    }
    
}
