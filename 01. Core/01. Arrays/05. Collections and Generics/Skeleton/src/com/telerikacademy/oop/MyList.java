package com.telerikacademy.oop;

public interface MyList<T> extends Iterable<T> {

    int size();

    int capacity();

    T get(int index);

    void add(T element);

    boolean contains(T element);

    int indexOf(T element);

    int lastIndexOf(T element);

    boolean remove(T element);

    boolean removeAt(int index);

    void clear();

    void swap(int from, int to);

    void print();
}
