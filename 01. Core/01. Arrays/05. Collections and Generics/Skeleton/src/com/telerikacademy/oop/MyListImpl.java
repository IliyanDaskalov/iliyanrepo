package com.telerikacademy.oop;

import java.util.Arrays;
import java.util.Iterator;

public class MyListImpl<T> implements MyList<T> {

    private static final int DEFAULT_CAPACITY = 8;

    private T[] elements;
    private int size;

    public MyListImpl() {
        this(DEFAULT_CAPACITY);
    }

    public MyListImpl(int capacity) {
        this.elements = (T[]) new Object[capacity];
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int capacity() {
        return this.elements.length;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size()) {
            throw new ArrayIndexOutOfBoundsException("Invalid index.");
        }

        return elements[index];
    }

    @Override
    public void add(T element) {
        if (capacity() == size()) {
            this.elements = Arrays.copyOf(this.elements, capacity() * 2);
        }

        this.elements[size] = element;
        size++;
    }

    @Override
    public boolean contains(T element) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int indexOf(T element) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(T element) {
        for (int i = size - 1; i >= 0; i--) {
            if (elements[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean remove(T element) {
        return removeAt(indexOf(element));
    }

    @Override
    public boolean removeAt(int index) {

        if(index < 0 || index >= size())  return false;

        for (int i = index; i < size() ; i++) {
            this.elements[i] = elements[i +1];
        }
        this.elements[size() - 1] = null;
        size--;
        return true;
    }

    @Override
    public void clear() {
        this.size = 0;
        this.elements = (T[]) new Object[DEFAULT_CAPACITY];

    }

    @Override
    public void swap(int from, int to) {
        T temporary = get(from);
        this.elements[from] = get(to);
        this.elements[to] = temporary;

    }

    @Override
    public void print() {
        StringBuilder result = new StringBuilder("[");

        for (int i = 0; i < size() - 1; i++) {
            result.append(get(i)).append(", ");
        }
       // System.out.println(result.append(elements[size() - 1]).append("]").toString());
        result.append(elements[size()-1 ]).append("]");
        System.out.println(result.toString());
    }

    @Override
    public Iterator<T> iterator() {
        return new MyListIterator();
    }

    private class MyListIterator implements Iterator<T> {
        int currentIndex;

        MyListIterator() {
            currentIndex = 0;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public T next() {
            T currentElement = elements[currentIndex];
            currentIndex++;
            return currentElement;
        }
    }
}
