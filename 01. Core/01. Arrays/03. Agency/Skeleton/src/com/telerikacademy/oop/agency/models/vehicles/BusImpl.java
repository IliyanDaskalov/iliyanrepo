package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleImpl implements Bus {

    private static final int MIN_BUS_PASSENGERS = 10;
    private static final int MAX_BUS_PASSENGERS = 50;

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer);
    }

    @Override
    protected int minPassengers() {
        return MIN_BUS_PASSENGERS;
    }

    @Override
    protected int maxPassengers() {
        return MAX_BUS_PASSENGERS;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.LAND;
    }

    @Override
    public String toString() {
        return String.format("Bus ----%n%s",super.toString());
    }
}
