package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.Validator;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleImpl implements Vehicle {

    private static final int MIN_PASSENGERS = 1;
    private static final int MAX_PASSENGERS = 800;
    private static final double MIN_PRICE_PER_KM = 0.10;
    private static final double MAX_PRICE_PER_KM = 2.50;
    private static final String DEFAULT_PRICE_PER_KM_ERROR_MESSAGE = "A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!";
    private static final String DEFAULT_PASSENGERS_CAPACITY_ERROR_MESSAGE = "A %s cannot have less than %d passengers or more than %d passengers.";

    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;


    public VehicleImpl(int passengerCapacity, double pricePerKilometer) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
    }

    private void setPassengerCapacity(int passengerCapacity) {
        Validator.validateNumber(passengerCapacity,
                    minPassengers(),
                    maxPassengers(),
                    String.format(DEFAULT_PASSENGERS_CAPACITY_ERROR_MESSAGE,
                            this.getClass().getInterfaces()[0].getSimpleName().toLowerCase(),
                            minPassengers(),
                            maxPassengers()));

        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        Validator.validateNumber(pricePerKilometer,
                        MIN_PRICE_PER_KM,
                        MAX_PRICE_PER_KM,
                        String.format(DEFAULT_PRICE_PER_KM_ERROR_MESSAGE,
                            MIN_PRICE_PER_KM,
                            MAX_PRICE_PER_KM));

        this.pricePerKilometer = pricePerKilometer;
    }

    private void setType(VehicleType type) {
        Validator.validateNotNull("Vehicle type", type);
        this.type = type;
    }



    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    protected int minPassengers(){
        return MIN_PASSENGERS;
    }
    protected int maxPassengers(){
        return MAX_PASSENGERS;
    }


    @Override
    public String toString() {
        return  String.format(
                "Passenger capacity: %d%n"  +
                "Price per kilometer: %.2f%n"  +
                "Vehicle type: %s%n",
                passengerCapacity,
                pricePerKilometer,
                getType());
    }
}
