package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.Validator;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleImpl implements Airplane {

    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer);
        setHasFreeFood(hasFreeFood);
    }

    private void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.AIR;
    }
    @Override
    public String toString() {
        return String.format("Airplane ----%n%s" +
                "Has free food: %s%n",super.toString(),hasFreeFood());
    }
}
