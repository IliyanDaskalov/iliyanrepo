package com.telerikacademy.oop.agency.models;

public class Validator {

    private Validator() {
    }

    public static void validateNotNull(String input, Object obj) {
        if (obj == null)
            throw new IllegalArgumentException(input + " cannot be null!");
    }

    public static void validateNumber(double number, double min, double max, String message) {
        if (number < min || number > max) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateLength(String input, int min, int max, String message) {
        String trimmedInput = input.trim();

        if (trimmedInput.length() < min
                || trimmedInput.length() > max)
            throw new IllegalArgumentException(message);
    }

    public static void validatePrice(double price, String message) {
        if (price < 0) {
            throw new IllegalArgumentException(message);
        }
    }
}
