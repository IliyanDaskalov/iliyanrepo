package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

public class JourneyImpl implements Journey {

    private static final int MIN_START_LOCATION_LENGTH = 5;
    private static final int MAX_START_LOCATION_LENGTH = 25;
    private static final String DEFAULT_START_LOCATION_LENGTH_ERROR_MESSAGE = "The StartingLocation's length cannot be less than %d or more than %d symbols long.";
    private static final int MIN_DESTINATION_LENGTH = 5;
    private static final int MAX_DESTINATION_LENGTH = 25;
    private static final String DEFAULT_DESTINATION_LENGTH_ERROR_MESSAGE = "The Destination's length cannot be less than %d or more than %d symbols long.";
    private static final int MIN_DISTANCE = 5;
    private static final int MAX_DISTANCE = 5000;
    private static final String DEFAULT_DISTANCE_ERROR_MESSAGE = "Distance cannot be less than %d and more than %d.";

    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;

    public JourneyImpl(String startLocation, String destination, int distance, Vehicle vehicle) {
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        setVehicle(vehicle);
    }

    private void setStartLocation(String startLocation) {
        Validator.validateNotNull("Start location",startLocation);

        Validator.validateLength(startLocation,
                    MIN_START_LOCATION_LENGTH,
                    MAX_START_LOCATION_LENGTH,
                    String.format(DEFAULT_START_LOCATION_LENGTH_ERROR_MESSAGE,
                            MIN_START_LOCATION_LENGTH,
                            MAX_START_LOCATION_LENGTH));

        this.startLocation = startLocation;
    }

    private void setDestination(String destination) {
        Validator.validateNotNull("Destination", destination);
        Validator.validateLength(destination,
                    MIN_DESTINATION_LENGTH,
                    MAX_DESTINATION_LENGTH,
                    String.format(DEFAULT_DESTINATION_LENGTH_ERROR_MESSAGE,
                            MIN_DESTINATION_LENGTH,
                            MAX_DESTINATION_LENGTH));

        this.destination = destination;
    }

    private void setDistance(int distance) {
        Validator.validateNumber(distance,
                    MIN_DISTANCE,
                    MAX_DISTANCE,
                    String.format(DEFAULT_DISTANCE_ERROR_MESSAGE,
                            MIN_DISTANCE,
                            MAX_DISTANCE));

        this.distance = distance;
    }

    private void setVehicle(Vehicle vehicle) {
        Validator.validateNotNull("Vehicle", vehicle);
        this.vehicle = vehicle;
    }

    @Override
    public String getDestination() {
        return destination;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    @Override
    public String getStartLocation() {
        return startLocation;
    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }

    @Override
    public double calculateTravelCosts() {
        return distance * (vehicle.getPricePerKilometer());
    }


    @Override
    public String toString() {
        return String.format("Journey ----%n" +
                        "Start location: %s%n" +
                        "Destination: %s%n" +
                        "Distance: %d%n" +
                        "Vehicle type: %s%n" +
                        "Travel costs: %.2f%n",
                startLocation,
                destination,
                distance,
                vehicle.getType(),
                calculateTravelCosts());
    }
}
