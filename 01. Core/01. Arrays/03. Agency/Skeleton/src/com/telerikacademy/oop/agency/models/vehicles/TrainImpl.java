package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.Validator;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleImpl implements Train {

    private static final int MIN_TRAIN_PASSENGERS = 30;
    private static final int MAX_TRAIN_PASSENGERS = 150;
    private static final int MIN_CARTS = 1;
    private static final int MAX_CARTS = 15;
    public static final String DEFAULT_CARTS_ERROR_MESSAGE = "A train with lower than %d or higher than %d carts cannot exist!";

    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer);
        setCarts(carts);
    }

    @Override
    protected int minPassengers() {
        return MIN_TRAIN_PASSENGERS;
    }

    @Override
    protected int maxPassengers() {
        return MAX_TRAIN_PASSENGERS;
    }


    private void setCarts(int carts) {
        Validator.validateNumber(carts,
                    MIN_CARTS,
                    MAX_CARTS,
                    String.format(DEFAULT_CARTS_ERROR_MESSAGE,
                        MIN_CARTS,
                        MAX_CARTS));

        this.carts = carts;
    }

    @Override
    public VehicleType getType() {
        return VehicleType.LAND;
    }

    @Override
    public int getCarts() {
        return carts;
    }

    @Override
    public String toString() {
        return String.format("Train ----%n%s" +
                "Carts amount: %d%n",super.toString(),carts);
    }
}
