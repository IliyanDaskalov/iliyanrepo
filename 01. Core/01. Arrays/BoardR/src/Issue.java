import java.time.LocalDate;
import java.util.Objects;

public class Issue  extends  BoardItem{

    private String description;

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate);
        setDescription(description);
    }

    public String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        this.description = Objects.requireNonNullElse(description, "No description");
    }

    @Override
    public void revertStatus() {
        if (getStatus() != initialStatus()) {
            setStatus(initialStatus());
        } else {
            logEvent(String.format("Issue status already at %s", getStatus()));
        }
    }

    @Override
    public void advanceStatus() {
        if (getStatus() != finalStatus()) {
            setStatus(finalStatus());
        } else {
            logEvent(String.format("Issue status already at %s", getStatus()));
        }
    }

    @Override
    protected void setStatus(Status status) {
        logEvent(String.format("Issue status set to %s", status));
        super.setStatus(status);
    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();

        return String.format("Issue: %s, Description: %s", baseInfo, this.getDescription());
    }
}
