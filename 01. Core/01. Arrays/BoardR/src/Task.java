import java.time.LocalDate;

public class Task extends BoardItem{

    private static final int MIN_ASSIGNEE_LENGTH = 5;
    private static final int MAX_ASSIGNEE_LENGTH = 30;
    private String assignee;

    public Task(String title, String assignee,LocalDate dueDate) {
        super(title, dueDate);
        validateAssignee(assignee);
        this.assignee = assignee;

    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        validateAssignee(assignee);
        logEvent(String.format("Assignee changed from %s to %s", getAssignee(), assignee));
        this.assignee = assignee;
    }

    private void validateAssignee(String assignee){
        if(assignee == null){
            throw new IllegalArgumentException("Assignee input cannot be null!");
        }
        if(assignee.trim().length() < MIN_ASSIGNEE_LENGTH || assignee.trim().length() >MAX_ASSIGNEE_LENGTH){
            throw new IllegalArgumentException("Assignee input should be between 5 and 30 symbols.");
        }
    }

    @Override
    public void revertStatus() {
        if (getStatus() != Status.OPEN) {
            setStatus(Status.values()[getStatus().ordinal() - 1]);
        } else {
            logEvent(String.format("Task status already at %s", getStatus()));
        }
    }

    @Override
    public void advanceStatus() {
        if (getStatus() != finalStatus()) {
            setStatus(Status.values()[getStatus().ordinal() + 1]);
        } else {
            logEvent(String.format("Task status already at %s", getStatus()));
        }
    }

    @Override
    protected void setStatus(Status status) {
        logEvent(String.format("Task changed from %s to %s", getStatus(), status));
        super.setStatus(status);
    }

    @Override
     Status initialStatus() {
        return Status.TODO;
    }

}
