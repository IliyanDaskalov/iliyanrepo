import java.util.ArrayList;
import java.util.List;

public class Board {
    private static final List<BoardItem> items = new ArrayList<>();

    private Board() {

    }

    private static List<BoardItem> getItems() {
        return new ArrayList<>(items);
    }

    public static void addItem(BoardItem item){
        if (getItems().contains(item)) {
            throw new IllegalArgumentException("Item already in the list");
        }
        items.add(item);
    }

    public static int totalItems(){
        return items.size();
    }

//    public static void displayHistory() {
//        for (BoardItem item : items) {
//            System.out.println(item.getHistory());
//        }
//    }

    public static void displayHistory(Logger logger) {
        for (BoardItem item : items) {
            logger.log(item.getHistory());
        }
    }

}
