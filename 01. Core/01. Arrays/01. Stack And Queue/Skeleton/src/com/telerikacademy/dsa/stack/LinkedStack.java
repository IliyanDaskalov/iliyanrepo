package com.telerikacademy.dsa.stack;


import com.telerikacademy.dsa.Node;

import java.util.NoSuchElementException;

public class LinkedStack<E> implements Stack<E> {
    private Node<E> top;
    private int size;

    public LinkedStack() {
        this.top = null;
        this.size = 0;
    }


    @Override
    public void push(E element) {
        Node<E> temp = new Node<>(element);
        if (isEmpty()) {
            top = temp;
            temp.next = null;
        } else {
            temp.next = top;
            top = temp;
        }
        size++;

    }

    @Override
    public E pop() {
        if(isEmpty()){
            throw new NoSuchElementException("Linked list is empty.");
        }
        var result = top;
        top = top.next;
        result.next = null;
        size--;
        return result.data;
    }

    @Override
    public E peek() {
        if(isEmpty()){
            throw new NoSuchElementException("Linked list is empty.");
        }

        return top.data;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return top == null;
    }
}
