package com.telerikacademy.dsa.stack;

import java.util.NoSuchElementException;

public class ArrayStack<E> implements Stack<E> {
    public static final int INITIAL_CAPACITY = 8;
    private E[] data;
    private int top;

    public ArrayStack() {
        this.data = (E[]) new Object[INITIAL_CAPACITY];
        this.top = -1;
    }

    @Override
    public void push(E element) {
        if (top >= data.length) {
            doubleCapacity();
        }
        top++;
        data[top] = element;
    }

    @Override
    public E pop() {
        if (isEmpty()) {
            throw new NoSuchElementException("Array is empty.");
        }
        E result = data[top];
        top--;
        return result;
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("Array is empty.");
        }
        return data[top];
    }

    @Override
    public int size() {
        if (top == -1) {
            return 0;
        } else {
            return top + 1;
        }

    }

    @Override
    public boolean isEmpty() {
        return top == -1;
    }

    private void doubleCapacity() {
        int newCapacity = 2 * INITIAL_CAPACITY;
        E[] newData = (E[]) new Object[newCapacity];

        System.arraycopy(data, 0, newData, 0, INITIAL_CAPACITY);
        data = newData;
    }
}
