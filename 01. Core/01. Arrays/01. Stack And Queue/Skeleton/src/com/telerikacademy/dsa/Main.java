package com.telerikacademy.dsa;

import com.telerikacademy.dsa.stack.ArrayStack;
import com.telerikacademy.dsa.stack.LinkedStack;

public class Main {
    public static void main(String[] args) {

        var linkedStack = new LinkedStack<Integer>();

        linkedStack.push(1);
        linkedStack.push(2);
        linkedStack.push(3);
        linkedStack.push(5);

        linkedStack.pop();

        System.out.println(linkedStack.peek());
        System.out.println(linkedStack.size());
        System.out.println("#####################");

        var arrayStack = new ArrayStack<Integer>();

        arrayStack.push(1);
        arrayStack.push(2);
        arrayStack.push(5);
        arrayStack.push(8);

        arrayStack.pop();
        arrayStack.pop();

        System.out.println(arrayStack.peek());
        System.out.println(arrayStack.size());
    }
}
