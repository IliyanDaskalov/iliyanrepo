<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# DSA Workshop (Stack and Queue)

## Stack

Stack is a **LIFO** (last in first out) data structure.

### Real life stacks

- A stack of dishes
- A pile of t-shirts

### Some practical use-cases of a stack:

- Reversing order
- "Undo" functionality
- Several important algorithms (like DFS)
- The call stack - keeping track of currently active methods

### Common operations on a stack

- `push(element)` - add an element to the stack
- `pop()` - remove an element from the top of the stack
- `peek()` - get the value of the first element, without removing it

## Queue

Queue is a **FIFO** (first in first out) data structure.

### Some practical use-cases of a queue:

- Breadth-First Search in a tree/graph
- In a call center - to manage the people who need to be helped
- Printing documents - the printer can only print one at a time, the others are next in line (First in, first out)

### Common operations on a queue

- `enqueue(element)` - adding an element to the queue
- `dequeue()`  - removing an element from the queue
- `peek()` - get the value of the first element, without removing it

## Stack vs Queue

![picture](Images/StackVsQueue.png)

## Task

Your task is to implement a stack and a queue, using the skeleton provided.

### Guidelines:

1. Implement all methods from the Stack and Queue interfaces.
1. You can use either array or linked implementation using a *Node* class.
   - Start with ArrayStack, then LinkedStack.
   - Continue with LinkedQueue.
   - ArrayQueue is advanced task.
   - You are not allowed to use ArrayList and LinkedList classes.
   - There is a sample *Node* class in the skeleton.
1. All tests should pass.
   - Select correct implementation in tests class.

## Practice

### Tasks for homework:

 - [Valid Parentheses](https://leetcode.com/problems/valid-parentheses/)
 - [Next Greater Element](https://leetcode.com/problems/next-greater-element-i/)
 - [Implement Queue Using Stack](https://leetcode.com/problems/implement-queue-using-stacks/description/)

### Tasks that we will solve together:

 - [Backspace String Compare](https://leetcode.com/problems/backspace-string-compare/)
 - [Lemonade Change](https://leetcode.com/problems/lemonade-change/)
 - [Baseball Game](https://leetcode.com/problems/baseball-game/)
 - [Asteroid Collision](https://leetcode.com/problems/asteroid-collision/)
