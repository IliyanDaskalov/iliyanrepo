package com.telerikacademy;

public enum Genre {
    DRAMA, COMEDY, ACTION, HORROR, FANTASY
}
