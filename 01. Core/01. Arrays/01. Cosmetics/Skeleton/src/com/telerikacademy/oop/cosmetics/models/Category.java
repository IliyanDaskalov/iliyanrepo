package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private static final int MIN_CATEGORY_NAME_LENGTH = 2;
    private static final int MAX_CATEGORY_NAME_LENGTH = 15;

    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        products = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Category name cannot be null!");
        }
        if (name.trim().length() < MIN_CATEGORY_NAME_LENGTH || name.trim().length() > MAX_CATEGORY_NAME_LENGTH) {
            throw new IllegalArgumentException("Category name should be between 2 and 15 symbols");
        }
        this.name = name;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        if(product == null){
            throw new IllegalArgumentException("Product from category cannot be null");
        }
        products.add(product);
    }

    public void removeProduct(Product product) {
        if(product == null){
            throw new IllegalArgumentException("Product from category cannot be null");
        }

        if(!products.contains(product)) {
            throw new IllegalArgumentException("Product from category not found");
        }

        products.remove(product);

    }
//#Category: Shampoos
// #MyMan Nivea
// #Price: $10.99
// #Gender: MEN
// ===
    public String print() {
        StringBuilder printCategory = new StringBuilder(String.format("#Category: [%s]%n",getName()));
        if(products.isEmpty()){
            printCategory.append("#No product in this category");
        }
        for (Product product: products) {
           printCategory.append(product.print());
        }
        return printCategory.toString();
    }

}
