package com.telerikacademy.oop.cosmetics.core.contracts;

import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.cart.ShoppingCart;
import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.Map;

public interface CosmeticsRepository {
    
    ShoppingCart getShoppingCart();
    
    Map<String, Category> getCategories();
    
    Map<String, Product> getProducts();
    
}
