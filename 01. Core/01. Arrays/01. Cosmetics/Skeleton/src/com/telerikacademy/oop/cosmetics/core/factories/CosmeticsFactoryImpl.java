package com.telerikacademy.oop.cosmetics.core.factories;

import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.cart.ShoppingCart;
import com.telerikacademy.oop.cosmetics.models.common.GenderType;
import com.telerikacademy.oop.cosmetics.models.products.Product;

public class CosmeticsFactoryImpl implements CosmeticsFactory {
    
    public Category createCategory(String name) {
        return new Category(name);
    }
    
    public Product createProduct(String name, String brand, double price, String gender) {
        Product product = new Product(name,brand,price, GenderType.valueOf(gender.toUpperCase()));
        return product;
    }
    
    public ShoppingCart createShoppingCart() {
        return new ShoppingCart();
    }
    
}
