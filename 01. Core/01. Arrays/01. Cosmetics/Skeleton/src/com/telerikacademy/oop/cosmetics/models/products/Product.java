package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {

    private static final int MIN_NAME_LENGTH = 3;
    private static final int MAX_NAME_LENGTH = 10;
    private static final int MIN_BRAND_NAME_LENGTH = 2;
    private static final int MAX_BRAND_NAME_LENGTH = 10;

    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {
        // finish the constructor and validate data
        //    setPrice(price);
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }


    public String getName() {
        return name;
    }

    private void setName(String name) {
        if(name == null){
            throw new IllegalArgumentException("Name cannot be null!");
        }
        if(name.trim().length() < MIN_NAME_LENGTH || name.trim().length() > MAX_NAME_LENGTH){
            throw new IllegalArgumentException("Name should be between 3 and 10 symbols");
        }
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        if(brand == null){
            throw new IllegalArgumentException("Brand name cannot be null!");
        }
        if(brand.trim().length() < MIN_BRAND_NAME_LENGTH || brand.trim().length() > MAX_BRAND_NAME_LENGTH){
            throw new IllegalArgumentException("Brand name should be between 2 and 10 symbols");
        }
        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if(price <= 0.00){
            throw new IllegalArgumentException("Product price cannot be negative!");
        }
        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }

    public String print() {
        return String.format("#[%s] [%s]%n" +
                "#Price: [%s]%n" +
                "#Gender: [%s]%n" +
                "===",getName(),getBrand(),getPrice(),getGender());
        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="
    }

}
