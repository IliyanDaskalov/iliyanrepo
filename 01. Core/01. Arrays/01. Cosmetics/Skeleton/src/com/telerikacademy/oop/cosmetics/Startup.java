package com.telerikacademy.oop.cosmetics;

import com.telerikacademy.oop.cosmetics.core.CosmeticsEngine;
import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.cart.ShoppingCart;
import com.telerikacademy.oop.cosmetics.models.common.GenderType;
import com.telerikacademy.oop.cosmetics.models.products.Product;

public class Startup {
    
    public static void main(String[] args) {
        CosmeticsEngine engine = new CosmeticsEngine();
        engine.start();

    }
    //Product with name MyMan was created!
    //Category with name Shampoos was created!
    //Product MyMan added to category Shampoos!
    //Product MyMan was added to the shopping cart!
    //#Category: Shampoos
    // #MyMan Nivea
    // #Price: $10.99
    // #Gender: MEN
    // ===
    //$10.99 total price currently in the shopping cart!
    //Product MyMan removed from category Shampoos!
    //#Category: Shampoos
    // #No product in this category
    //Product MyMan was removed from the shopping cart!
    //No product in shopping cart!
    
}
