package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;

import java.util.List;

public class ShowCategory implements Command {
    private static final int EXPECTED_PARAMETERS_COUNT = 1;
    private static final String INVALID_PARAMETERS_COUNT_MESSAGE = String.format(
            "ShowCategory command expects %d parameters.",
            EXPECTED_PARAMETERS_COUNT);

    private static final String CATEGORY_NOT_EXIST_MESSAGE = "Category %s does not exist.";

    private final ProductRepository productRepository;
    private String result;

    public ShowCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() < EXPECTED_PARAMETERS_COUNT) {
            throw new InvalidUserInputException(INVALID_PARAMETERS_COUNT_MESSAGE);
        }

        String categoryName = parameters.get(0);

        result = showCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String showCategory(String categoryName) {
        if (!productRepository.getCategories().containsKey(categoryName)) {
            throw new InvalidUserInputException(String.format(CATEGORY_NOT_EXIST_MESSAGE, categoryName));
        }

        Category category = productRepository.getCategories().get(categoryName);

        return category.print();
    }
}
