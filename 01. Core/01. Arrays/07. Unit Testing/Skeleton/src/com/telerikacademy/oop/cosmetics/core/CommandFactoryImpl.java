package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.commands.*;
import com.telerikacademy.oop.cosmetics.commands.CommandType;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CommandFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;

public class CommandFactoryImpl implements CommandFactory {
    private static final String COMMAND_NOT_SUPPORTED_MESSAGE = "Command %s is not supported.";

    @Override
    public Command createCommand(String commandTypeValue, ProductFactory productFactory, ProductRepository productRepository) {
        try {
            CommandType commandType = CommandType.valueOf(commandTypeValue.toUpperCase());

            switch (commandType) {
                case CREATECATEGORY:
                    return new CreateCategory(productRepository, productFactory);
                case CREATEPRODUCT:
                    return new CreateProduct(productRepository, productFactory);
                case ADDPRODUCTTOCATEGORY:
                    return new AddProductToCategory(productRepository);
                case SHOWCATEGORY:
                    return new ShowCategory(productRepository);
                default:
                    // This exception means that the CommandType enum and command classes are inconsistent.
                    // This exception is not handled because it indicates that code should be fixed.
                    throw new UnsupportedOperationException(String.format(COMMAND_NOT_SUPPORTED_MESSAGE, commandTypeValue));
            }
        } catch (IllegalArgumentException e) {
            // This exception means that the user has entered not existing command.
            throw new InvalidUserInputException(String.format(COMMAND_NOT_SUPPORTED_MESSAGE, commandTypeValue));
        }
    }
}
