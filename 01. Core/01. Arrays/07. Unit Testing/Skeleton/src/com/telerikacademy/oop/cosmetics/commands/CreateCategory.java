package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;

import java.util.List;

public class CreateCategory implements Command {
    private static final int EXPECTED_PARAMETERS_COUNT = 1;
    private static final String INVALID_PARAMETERS_COUNT_MESSAGE = String.format(
            "CreateCategory command expects %d parameters.",
            EXPECTED_PARAMETERS_COUNT);

    private static final String CATEGORY_EXIST_MESSAGE = "Category %s already exist.";
    private static final String CATEGORY_CREATED = "Category with name %s was created!";

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateCategory(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() < EXPECTED_PARAMETERS_COUNT) {
            throw new InvalidUserInputException(INVALID_PARAMETERS_COUNT_MESSAGE);
        }

        String categoryName = parameters.get(0);

        result = createCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createCategory(String categoryName) {
        if (productRepository.getCategories().containsKey(categoryName)) {
            throw new DuplicateEntityException(String.format(CATEGORY_EXIST_MESSAGE, categoryName));
        }

        Category category = productFactory.createCategory(categoryName);
        productRepository.getCategories().put(categoryName, category);

        return String.format(CATEGORY_CREATED, categoryName);
    }
}
