package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;

import java.util.HashMap;
import java.util.Map;

public class ProductRepositoryImpl implements ProductRepository {
    private final Map<String, Category> categories;
    private final Map<String, Product> products;

    public ProductRepositoryImpl() {
        categories = new HashMap<>();
        products = new HashMap<>();
    }

    public Map<String, Category> getCategories() {
        return categories;
    }

    public Map<String, Product> getProducts() {
        return products;
    }
}
