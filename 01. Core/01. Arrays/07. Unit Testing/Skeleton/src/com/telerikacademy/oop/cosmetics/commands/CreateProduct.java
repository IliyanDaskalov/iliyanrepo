package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;

import java.util.List;

public class CreateProduct implements Command {
    private static final int EXPECTED_PARAMETERS_COUNT = 4;
    private static final String INVALID_PARAMETERS_COUNT_MESSAGE = String.format(
            "CreateProduct command expects %d parameters.",
            EXPECTED_PARAMETERS_COUNT);

    private static final String PRODUCT_EXIST_MESSAGE = "Product %s already exist.";
    private static final String INVALID_PRICE_MESSAGE = "Third parameter should be price (real number).";
    private static final String INVALID_GENDER_MESSAGE = "Forth parameter should be one of Men, Women or Unisex.";
    private static final String PRODUCT_CREATED = "Product with name %s was created!";

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateProduct(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() < EXPECTED_PARAMETERS_COUNT) {
            throw new InvalidUserInputException(INVALID_PARAMETERS_COUNT_MESSAGE);
        }

        try {
            String name = parameters.get(0);
            String brand = parameters.get(1);
            double price = Double.parseDouble(parameters.get(2));
            GenderType gender = GenderType.valueOf(parameters.get(3).toUpperCase());

            result = createProduct(name, brand, price, gender);
        } catch (NumberFormatException e) {
            throw new InvalidUserInputException(INVALID_PRICE_MESSAGE);
        } catch (IllegalArgumentException e) {
            throw new InvalidUserInputException(INVALID_GENDER_MESSAGE);
        }
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        if (productRepository.getProducts().containsKey(name)) {
            throw new DuplicateEntityException(String.format(PRODUCT_EXIST_MESSAGE, name));
        }

        Product product = productFactory.createProduct(name, brand, price, gender);
        productRepository.getProducts().put(name, product);

        return String.format(PRODUCT_CREATED, name);
    }
}
