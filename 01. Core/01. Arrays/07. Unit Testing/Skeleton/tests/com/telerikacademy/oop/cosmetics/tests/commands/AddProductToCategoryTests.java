package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategory;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddProductToCategoryTests {

    private ProductRepository productRepository;
    private AddProductToCategory testCommand;
    private List<String> arguments;

    @BeforeEach
    public void before(){
        productRepository = new ProductRepositoryImpl();
        testCommand = new AddProductToCategory(productRepository);
        arguments = new ArrayList<>();
    }

    @Test
    public void execute_Should_AddProductToCategory_When_ValidParameters(){
        // Arrange
        arguments.add("category");
        arguments.add("name");

        // Act
        testCommand.execute(arguments);

        // Assert
//        Assertions.assertEquals(1, productRepository.getCategories().size());
        Assertions.assertTrue(productRepository.getCategories().containsKey("category"));
//        Assertions.assertTrue(productRepository.getProducts().containsKey("name"));
    }


}
