package com.telerikacademy.oop.cosmetics.tests.core;

import org.junit.jupiter.api.Test;

public class CommandParserImplTests {

    @Test
    public void parseCommand_Should_ReturnCommand_WhenParametersNotExist() {

    }

    @Test
    public void parseCommand_Should_ReturnCommand_WhenParametersExist() {

    }

    @Test
    public void parseParameters_Should_ReturnEmptyList_WhenParametersNotExist() {

    }

    @Test
    public void parseParameters_Should_ReturnList_WhenParametersExist() {

    }
}
