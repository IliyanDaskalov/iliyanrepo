package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CategoryImplTests {

    private Product testProduct;

    @BeforeEach
    public void before() {
        testProduct = new ProductImpl("name", "brand", 10, GenderType.MEN);
    }

    @Test
    public void constructor_Should_CreateCategory_When_ArgumentsAreValid() {
        // Arrange, Act, Assert
        Category category = new CategoryImpl("category");
    }

    @Test
    public void constructor_Should_ThrowException_When_NameLengthLessThanMinValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new CategoryImpl("na"));
    }

    @Test
    public void constructor_Should_ThrowException_When_nameLengthMoreThanMaxValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new CategoryImpl(new String(new char[50])));
    }

    @Test
    public void addProduct_Should_AddProductToList() {
        //Arrange
        Category category = new CategoryImpl("category");


        //Act
        category.addProduct(testProduct);

        //Assert
        Assertions.assertEquals(1, category.getProducts().size());
    }

    @Test
    public void removeProduct_Should_RemoveProductFromList_When_ProductExist() {

        //Arrange
        Category category = new CategoryImpl("category");
        category.addProduct(testProduct);

        //Act
        category.removeProduct(testProduct);

        //Assert
        Assertions.assertEquals(0, category.getProducts().size());
    }

    @Test
    public void removeProduct_Should_NotRemoveProductFromList_When_ProductNotExist() {

        //Arrange
        Category category = new CategoryImpl("category");


        //Act
        category.removeProduct(testProduct);

        //Assert
        Assertions.assertEquals(0, category.getProducts().size());

    }
}
