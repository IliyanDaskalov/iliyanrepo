package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.AddProductToCategory;
import com.telerikacademy.oop.cosmetics.commands.ShowCategory;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowCategoryTests {

    private ProductRepository productRepository;
    private ProductFactory productFactory;
    private ShowCategory testCommand;
    private List<String> arguments;

    @BeforeEach
    public void before(){
        productRepository = new ProductRepositoryImpl();
        testCommand = new ShowCategory(productRepository);
        arguments = new ArrayList<>();
    }

    @Test
    public void execute_Should_ShowCategory_When_ValidParameters() {
        // Arrange
        arguments.add("category");

        // Act
        testCommand.execute(arguments);

        // Assert
        //Assertions.assertEquals(1, productRepository.getCategories().size());
        Assertions.assertTrue(productRepository.getCategories().containsKey("category"));
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {
        Assertions.assertThrows(InvalidUserInputException.class, ()-> testCommand.execute(arguments));
    }


}
