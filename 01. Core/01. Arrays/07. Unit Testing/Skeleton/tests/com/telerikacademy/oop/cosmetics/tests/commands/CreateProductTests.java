package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateProduct;
import com.telerikacademy.oop.cosmetics.core.ProductFactoryImpl;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateProductTests {
    private CreateProduct testCommand;
    private ProductRepository productRepository;
    private ProductFactory productFactory;
    private List<String> arguments;

    @BeforeEach
    public void before() {
        productFactory = new ProductFactoryImpl();
        productRepository = new ProductRepositoryImpl();
        testCommand = new CreateProduct(productRepository, productFactory);
        arguments = new ArrayList<>();
    }

    @Test
    public void execute_Should_ThrowException_When_PassedFewerParametersThanExpected() {
        // Arrange
        arguments.add("1");

        // Act & Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> testCommand.execute(arguments));
    }

    @Test
    public void execute_Should_ThrowException_When_PassedMoreArgumentsThanExpected() {
        // Arrange
        arguments.add("5");
        arguments.add("2");
        arguments.add("5");
        arguments.add("6");
        arguments.add("1");

        // Act & Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> testCommand.execute(arguments));
    }


    @Test
    public void execute_Should_CreateProduct_When_passedValidInput() {

        // Arrange
        arguments.add("name");
        arguments.add("brand");
        arguments.add("10");
        arguments.add("Men");

        // Act
        testCommand.execute(arguments);

        // Assert
        Assertions.assertEquals(1, productRepository.getProducts().size());

    }


    @Test
    public void execute_Should_ThrowException_When_DuplicateProductName() {
        productRepository.getProducts().put("name", new ProductImpl("name", "brand", 10, GenderType.MEN));
        arguments.add("name");
        arguments.add("brand");
        arguments.add("10");
        arguments.add("Men");
        Assertions.assertThrows(DuplicateEntityException.class, () -> testCommand.execute(arguments));
    }
}
