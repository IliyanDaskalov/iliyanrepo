package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProductImplTests {


    @Test
    public void constructor_Should_Throw_When_NameLengthLessThanMinValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ProductImpl("na", "brand", 10, GenderType.MEN));
    }

    @Test
    public void constructor_Should_Throw_When_BrandLengthLessThanMinValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ProductImpl("name", "b", 10, GenderType.MEN));
    }

    @Test
    public void constructor_Should_Throw_When_NameLengthMoreThanMaxValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ProductImpl(new String(new char[54]), "brand", 10, GenderType.MEN));
    }

    @Test
    public void constructor_Should_Throw_When_BrandLengthMoreThanMaxValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ProductImpl("name", new String(new char[54]), 10, GenderType.MEN));
    }

    @Test
    public void constructor_Should_Throw_When_PriceIsNegative(){
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new ProductImpl("name", "brand", -1, GenderType.MEN));
    }
}
