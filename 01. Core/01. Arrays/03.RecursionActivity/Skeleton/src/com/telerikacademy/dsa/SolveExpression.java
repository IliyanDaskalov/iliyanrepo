package com.telerikacademy.dsa;

public class SolveExpression {
    public static void main(String[] args) {
        System.out.println(solve("45+(55)"));
        System.out.println(solve("45+(24*(12+3))"));
        System.out.println(solve("((45))"));
        System.out.println(solve("12*(35-(46*(5+15)))"));

    }

    public static int solve(String expression) {
        if (expression.length() == 0) {
            return 0;
        }
        if (expression.charAt(0) == '(') {
            return solve(expression.substring(1, expression.length() - 1));
        }

        int operatorIndex = -1;
        for (int i = 0; i < expression.length(); i++) {
            if (!Character.isDigit(expression.charAt(i))) {
                operatorIndex = i;
                break;
            }
        }

        if (operatorIndex == -1) {
            return Integer.parseInt(expression);
        }

        int tempLeft = Integer.parseInt(expression.substring(0, operatorIndex));
        String tempRight = expression.substring(operatorIndex + 1);
        char operator = expression.charAt(operatorIndex);

        switch (operator) {
            case '+':
                return tempLeft + solve(tempRight);
            case '-':
                return tempLeft - solve(tempRight);
            case '*':
                return tempLeft * solve(tempRight);
            case '/':
                return tempLeft / solve(tempRight);
            default:
                throw new IllegalArgumentException();
        }
    }
}
