package com.telerikacademy.dsa;

import java.util.ArrayList;
import java.util.List;

public class HtmlNode {
    private String name;
    private List<HtmlAttribute> attributes;
    private List<HtmlNode> childNodes;

    public HtmlNode(String name) {
        this.name = name;
        attributes = new ArrayList<>();
        childNodes = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HtmlAttribute> getAttributes() {
        return attributes;
    }

    public List<HtmlNode> getChildNodes() {
        return childNodes;
    }
    
    public String open() {
        StringBuilder sb = new StringBuilder("<" + name);
        for (HtmlAttribute attribute : attributes) {
            sb.append(" ")
                    .append(attribute.getName())
                    .append("=")
                    .append("\"")
                    .append(attribute.getValue())
                    .append("\"");
        }
        return sb.append(">").toString();
    }
    public String close() {
        return String.format("</%s>", name);
    }
}
