public class Main {
    public static void main(String[] args) {
        ForumPost post1 = new ForumPost("Steven", "How to find use for every Microsoft product.");
        ForumPost post2 = new ForumPost("Edward", "Ford Focus for sale. First owner. Good mileage.", 300);
        post1.replies.add("I like Google!");
        post1.replies.add("Ugh, Microsoft... :(");

        System.out.print(post1.format());
        System.out.println();
        System.out.print(post2.format());

    }
}
