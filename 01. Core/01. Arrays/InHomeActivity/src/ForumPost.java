import java.util.ArrayList;

public class ForumPost {

    String author;
    String text;
    int upVotes;
    ArrayList<String> replies;

    public ForumPost(String author, String text, int upVotes) {
        this.author = author;
        this.text = text;
        this.upVotes = upVotes;
        this.replies = new ArrayList<>();
    }

    public ForumPost(String author, String text) {
        this("Steven", "How to find use for every Microsoft product.", 0);
    }


    public String format() {
        String format = "";

        for (int i = 0; i < replies.size(); i++) {
            format += String.format("- %s%n", replies.get(i));
        }

        return String.format("%s / by %s, %d votes. %n", text, author, upVotes) + format;
    }


}
