import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

public class GottaCatchEmAll {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Pokemon> list = new ArrayList<>();
        Map<String, TreeSet<Pokemon>> typeMap = new TreeMap<>();
        String[] input = reader.readLine().split(" ");
        StringBuilder builder = new StringBuilder();

        while (!input[0].equalsIgnoreCase("end")) {
            switch (input[0].toUpperCase()) {
                case "ADD":
                    Pokemon pokemon = new Pokemon(input[1], input[2], Integer.parseInt(input[3]), Integer.parseInt(input[4]));
                    int position = Integer.parseInt(input[4]) - 1;
                    list.add(position, pokemon);

                    if (!typeMap.containsKey(input[2])) {
                        typeMap.put(input[2], new TreeSet<>());
                    }
                    typeMap.get(input[2]).add(pokemon);

                    builder.append(String.format("Added pokemon %s to position %d%n", input[1], Integer.parseInt(input[4])));
                    break;
                case "FIND":
                    String type = input[1];
                    builder.append(String.format("Type %s: ", input[1]));
                    if (typeMap.containsKey(type)) {
                        typeMap.get(type).stream().limit(5)
                                .forEach(pokemon1 -> builder.append(String.format("%s(%d); ", pokemon1.getName(), pokemon1.getPower())));
                        builder.delete(builder.lastIndexOf(";"), builder.length());
                    }
                    builder.append(System.lineSeparator());

                    break;
                case "RANKLIST":
                    int from = Integer.parseInt(input[1]) - 1;
                    int to = Integer.parseInt(input[2]);
                    int counter = from + 1;
                    for (int i = from; i < to - 1; i++) {
                        builder.append(String.format("%d. %s(%d); ", counter, list.get(i).getName(), list.get(i).getPower()));
                        counter++;
                    }
                    builder.append(String.format("%d. %s(%d)%n", counter, list.get(to - 1).getName(), list.get(to - 1).getPower()));
                    break;
                case "END":
                    break;
            }

            input = reader.readLine().split(" ");
        }

        System.out.println(builder.toString());
    }

}

class Pokemon implements Comparable<Pokemon> {
    String name;
    String type;
    int power;
    int position;

    public Pokemon(String name, String type, int power, int position) {
        this.name = name;
        this.type = type;
        this.power = power;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getPower() {
        return power;
    }

    public int getPosition() {
        return position;
    }


    @Override
    public int compareTo(Pokemon o) {
        if (name.equals(o.name)) {
            return o.power - power;
        }
        return name.compareTo(o.name);
    }
}