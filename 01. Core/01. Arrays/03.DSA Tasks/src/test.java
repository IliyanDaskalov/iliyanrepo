public class test {
    public static void main(String[] args) {
        int a = 5;;
        Person p1 = new Person("Ivan");

        method(a, p1);

        System.out.println(a);
        System.out.println(p1.getName());
    }

    private static void method(int a, Person p1) {
        a = 6;
        p1.setName("Gosho");
        p1 = new Person("Misho");
        p1.setName("Tisho");
    }
}
