import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
public class JavaCount {

    static Map<Character, Integer> lowerCase = new TreeMap<>();
    static Map<Character, Integer> upperCase = new TreeMap<>();
    static Map<Character, Integer> symbols = new TreeMap<>();
    static Map.Entry<Character, Integer> maxLower = new TreeMap.SimpleEntry<>('-', 0);
    static Map.Entry<Character, Integer> maxUpper = new TreeMap.SimpleEntry<>('-', 0);
    static Map.Entry<Character, Integer> maxSymbols = new TreeMap.SimpleEntry<>('-', 0);

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        for (int i = 0; i < input.length(); i++) {
            add(input.charAt(i));
        }

        print(maxSymbols);
        print(maxLower);
        print(maxUpper);
    }
    private static void add(char ch) {
        if (ch >= 'a' && ch <= 'z') {
            add(ch, lowerCase);
            if (lowerCase.get(ch) >= maxLower.getValue()) {
                if (lowerCase.get(ch) > maxLower.getValue())
                    maxLower = new TreeMap.SimpleEntry<>(ch, lowerCase.get(ch));
                else if (ch < maxLower.getKey())
                    maxLower = new TreeMap.SimpleEntry<>(ch, lowerCase.get(ch));
            }
        } else if (ch >= 'A' && ch <= 'Z') {
            add(ch, upperCase);
            if (upperCase.get(ch) >= maxUpper.getValue()) {
                if (upperCase.get(ch) > maxUpper.getValue())
                    maxUpper = new TreeMap.SimpleEntry<>(ch, upperCase.get(ch));
                else if (ch < maxUpper.getKey())
                    maxUpper = new TreeMap.SimpleEntry<>(ch, upperCase.get(ch));
            }
        } else {
            add(ch, symbols);
            if (symbols.get(ch) >= maxSymbols.getValue()) {
                if (symbols.get(ch) > maxSymbols.getValue())
                    maxSymbols = new TreeMap.SimpleEntry<>(ch, symbols.get(ch));
                else if (ch < maxSymbols.getKey())
                    maxSymbols = new TreeMap.SimpleEntry<>(ch, symbols.get(ch));
            }
        }
    }
    private static void add(char ch, Map<Character, Integer> map) {
        if (map.containsKey(ch))
            map.put(ch, map.get(ch) + 1);
        else
            map.put(ch, 1);
    }

    private static void print(Map.Entry<Character, Integer> max) {
        System.out.printf("%c%s%n",
                max.getKey(),
                max.getValue() > 0 ? " " + max.getValue() : "");
    }
}