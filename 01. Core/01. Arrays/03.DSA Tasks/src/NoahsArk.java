import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

public class NoahsArk {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        Map<String, Integer> myMap = new TreeMap<>();
        int initialValue = 1;
        for (int i = 0; i < n; i++) {
            String input = reader.readLine();

            if (myMap.containsKey(input)) {
                myMap.replace(input, myMap.get(input) + 1);
            } else {
                myMap.put(input, initialValue);
            }
        }

        myMap.forEach((key, value) -> System.out.println(key + " " + value + " " + isEven(value)));

    }
    public static String isEven(int value){
        if(value % 2 == 0){
            return "Yes";
        }
        return "No";
    }
}
