import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class InventoryManager {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Map<String, Item> myMap = new TreeMap<>();
        Map<String, List<Item>> typeMap = new TreeMap<>();
        String[] input = reader.readLine().split(" ");
        StringBuilder builder = new StringBuilder();

        while (!input[0].equalsIgnoreCase("end")) {
            switch (input[0].toUpperCase()) {
                case "ADD":
                    if (myMap.containsKey(input[1])) {
                        builder.append(String.format("Error: Item %s already exists%n", input[1]));
                        break;
                    }
                    Item item = new Item(input[1], Double.parseDouble(input[2]), input[3]);
                    myMap.put(input[1], item);
                    typeMap.computeIfAbsent(input[3], k -> new ArrayList<>());
                    typeMap.get(input[3]).add(item);
                    builder.append(String.format("Ok: Item %s added successfully%n", input[1]));
                    break;
                case "FILTER":
                    if (input[1].equalsIgnoreCase("by")) {
                        switch (input[2].toLowerCase()) {
                            case "type":
                                String type = input[3];
                                List<Item> tempList = typeMap.get(type);

                                if (tempList == null) {
                                    builder.append(String.format("Error: Type %s does not exists%n", input[3]));
                                } else {
                                    tempList.sort(Comparator.comparing(Item::getPrice));
                                    tempList = tempList.subList(0, Math.min(tempList.size(), 10));

                                    builder.append("Ok: ");
                                    for (int i = 0; i < tempList.size() - 1; i++) {
                                        builder.append(String.format("%s(%s),", tempList.get(i).getName(), formatDouble(tempList.get(i).getPrice()))).append(" ");
                                    }
                                    builder.append(String.format("%s(%s)", tempList.get(tempList.size() - 1).getName(), formatDouble(tempList.get(tempList.size() - 1).getPrice())));
                                    builder.append(System.lineSeparator());
                                }

                                break;
                            case "price":
                                if (input.length == 5) {
                                    switch (input[3].toLowerCase()) {
                                        case "to":
                                            List<Item> tempList3 = new ArrayList<>(myMap.values());
                                            String[] finalInput1 = input;
                                            tempList3.sort(Comparator.comparing(Item::getPrice));
                                            tempList3 = tempList3.stream()
                                                    .filter(item1 -> item1.getPrice() < Double.parseDouble(finalInput1[4]))
                                                    .limit(10)
                                                    .collect(Collectors.toList());
                                            builder.append("Ok: ");
                                            if (tempList3.size() > 0) {
                                                for (int i = 0; i < tempList3.size() - 1; i++) {
                                                    builder.append(String.format("%s(%s),", tempList3.get(i).getName(), formatDouble(tempList3.get(i).getPrice()))).append(" ");
                                                }
                                                builder.append(String.format("%s(%s)", tempList3.get(tempList3.size() - 1).getName(), formatDouble(tempList3.get(tempList3.size() - 1).getPrice())));
                                            }
                                            builder.append(System.lineSeparator());
                                            break;
                                        case "from":
                                            List<Item> tempListFrom = new ArrayList<>(myMap.values());
                                            String[] finalInput2 = input;
                                            tempListFrom.sort(Comparator.comparing(Item::getPrice));
                                            tempListFrom = tempListFrom.stream()
                                                    .filter(item1 -> item1.getPrice() > Double.parseDouble(finalInput2[4]))
                                                    .limit(10)
                                                    .collect(Collectors.toList());
                                            builder.append("Ok: ");
                                            if (tempListFrom.size() > 0) {
                                                for (int i = 0; i < tempListFrom.size() - 1; i++) {
                                                    builder.append(String.format("%s(%s),", tempListFrom.get(i).getName(), formatDouble(tempListFrom.get(i).getPrice()))).append(" ");
                                                }
                                                builder.append(String.format("%s(%s)", tempListFrom.get(tempListFrom.size() - 1).getName(), formatDouble(tempListFrom.get(tempListFrom.size() - 1).getPrice())));
                                            }
                                            builder.append(System.lineSeparator());
                                            break;
                                    }
                                }
                                if (input.length == 7) {
                                    if (input[3].equalsIgnoreCase("from") && input[5].equalsIgnoreCase("to")) {
                                        List<Item> tempListFrom = new ArrayList<>(myMap.values());
                                        String[] finalInput2 = input;
                                        tempListFrom.sort(Comparator.comparing(Item::getPrice));
                                        tempListFrom = tempListFrom.stream()
                                                .filter(item1 -> item1.getPrice() > Double.parseDouble(finalInput2[4]) && item1.getPrice() < Double.parseDouble(finalInput2[6]))
                                                .limit(10)
                                                .collect(Collectors.toList());
                                        builder.append("Ok: ");
                                        if (tempListFrom.size() > 0) {
                                            for (int i = 0; i < tempListFrom.size() - 1; i++) {
                                                builder.append(String.format("%s(%s),", tempListFrom.get(i).getName(), formatDouble(tempListFrom.get(i).getPrice()))).append(" ");
                                            }
                                            builder.append(String.format("%s(%s)", tempListFrom.get(tempListFrom.size() - 1).getName(), formatDouble(tempListFrom.get(tempListFrom.size() - 1).getPrice())));
                                        }
                                        builder.append(System.lineSeparator());
                                    }
                                }
                                break;
                        }
                    }
                    break;
                case "END":
                    break;
            }

            input = reader.readLine().split(" ");
        }

        System.out.println(builder.toString());
    }

    public static String formatDouble(double d) {
        if (d == (long) d)
            return String.format("%d", (long) d);
        else
            return String.format("%s", d);
    }
}

class Item {
    String name;
    double price;
    String type;

    public Item(String name, double price, String type) {
        setName(name);
        setPrice(price);
        setType(type);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < 3 || name.length() > 20) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price < 0 || price > 5000) {
            throw new IllegalArgumentException();
        }
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if (type.length() < 3 || type.length() > 20) {
            throw new IllegalArgumentException();
        }
        this.type = type;
    }
}