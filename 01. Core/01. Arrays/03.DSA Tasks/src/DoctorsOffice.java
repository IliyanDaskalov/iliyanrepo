import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class DoctorsOffice {
    public static void main(String[] args) throws IOException {
        List<String> list = new ArrayList<>();
        Map<String, Integer> findMap = new TreeMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] command = reader.readLine().split(" ");
        StringBuilder builder = new StringBuilder();

        while (!command[0].equalsIgnoreCase("end")) {
            switch (command[0].toUpperCase()) {
                case "APPEND":
                    list.add(command[1]);
                    if (!findMap.containsKey(command[1])) {
                        findMap.put(command[1], 1);
                    } else {
                        findMap.put(command[1], findMap.get(command[1]) + 1);
                    }

                    builder.append("OK").append(System.lineSeparator());
                    break;
                case "INSERT":
                    int index = Integer.parseInt(command[1]);
                    if (index <= list.size()) {
                        list.add(index, command[2]);
                        if (!findMap.containsKey(command[2])) {
                            findMap.put(command[2], 1);
                        } else {
                            findMap.put(command[2], findMap.get(command[2]) + 1);
                        }
                        builder.append("OK").append(System.lineSeparator());
                    } else {
                        builder.append("Error").append(System.lineSeparator());
                    }
                    break;
                case "EXAMINE":
                    int position = Integer.parseInt(command[1]);
                    if (position <= list.size() ) {
                        for (int i = 0; i < position; i++) {
                            builder.append(String.format("%s ", list.get(i)));
                            String removed = list.get(i);
                            if (findMap.containsKey(removed)) {
                                findMap.put(removed, findMap.get(removed) - 1);
                            }
                        }
                        list = list.subList(position , list.size());
                        builder.append(System.lineSeparator());
                    } else {
                        builder.append("Error").append(System.lineSeparator());
                    }
                    break;
                case "FIND":
                    if (findMap.containsKey(command[1])) {
                        builder.append(findMap.get(command[1])).append(System.lineSeparator());
                    } else {
                        builder.append("0").append(System.lineSeparator());
                    }
                    break;
                case "END":
                    break;

            }
            command = reader.readLine().split(" ");
        }
        System.out.println(builder.toString());
    }
}


