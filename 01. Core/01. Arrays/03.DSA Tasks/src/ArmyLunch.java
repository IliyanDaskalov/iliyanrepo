import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class ArmyLunch {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        String[] input = reader.readLine().split(" ");
        List<String> sergeants = new ArrayList<>();
        List<String> corporals = new ArrayList<>();
        List<String> privates = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            if(input[i].charAt(0) == 'S'){
                sergeants.add(input[i]);
            } else if(input[i].charAt(0) == 'C'){
                corporals.add(input[i]);
            } else {
                privates.add(input[i]);
            }
        }

        System.out.print(String.join(" ", sergeants));
        System.out.print(" ");
        System.out.print(String.join(" ", corporals));
        System.out.print(" ");
        System.out.print(String.join(" ", privates));

    }
}
