import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Jumps {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        String[] input = reader.readLine().split(" ");

        int[] numbers = new int[n];

        for (int i = 0; i < n; i++) {
            numbers[i] = Integer.parseInt(input[i]);
        }


        int longestJumps = 0;
        int [] jumps = new int[n];

        for (int i = n - 2; i >= 0; i--) {

            for (int j = i + 1; j < n; j++) {
                if (numbers[j] > numbers[i]) {
                    jumps[i] = jumps[j] + 1;
                    if (jumps[i] > longestJumps) {
                        longestJumps = jumps[i];
                        break;
                    }
                }
            }
        }

        System.out.println(longestJumps);
        for (int i = 0; i < n - 1; i++) {
            System.out.print(jumps[i] + " ");
        }
        System.out.print(0);
    }
}