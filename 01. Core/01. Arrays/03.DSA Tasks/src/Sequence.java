import java.util.Scanner;

public class Sequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int n = scanner.nextInt();

        System.out.println(calculate(k, n));
    }

    public static int calculate(int inputNumber, int level) {
        if (level <= 1) {
            return inputNumber;
        }
        switch (level % 3) {
            case 0:
                return 2 * calculate(inputNumber, level / 3) + 1;
            case 1:
                return calculate(inputNumber, level / 3) + 2;
            case 2:
                return calculate(inputNumber, level / 3 + 1) + 1;
            default:
                throw new IllegalArgumentException();
        }
    }
}
