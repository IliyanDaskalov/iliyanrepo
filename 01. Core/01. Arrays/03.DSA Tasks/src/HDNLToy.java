import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class HDNLToy {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        StringBuilder builder = new StringBuilder();
        Stack<String> stack = new Stack<>();
        int counter = 0;

        for (int i = 0; i < n; i++) {
            String input = reader.readLine();
            int number = Integer.parseInt(input.substring(1));

            if (stack.isEmpty()) {
                builder.append(String.format("<%s>%n", input));
                stack.push(input);
            } else {
                int stackNum = Integer.parseInt(stack.peek().substring(1));

                if (number > stackNum) {
                    counter++;
                    for (int j = 0; j < counter; j++) {
                        builder.append(" ");
                    }
                    builder.append(String.format("<%s>%n", input));
                    stack.push(input);
                } else {
                    String pop = stack.pop();

                    for (int j = 0; j < counter; j++) {
                        builder.append(" ");
                    }

                    builder.append(String.format("</%s>%n", pop));
                    if (!stack.isEmpty()) {
                        int newStackNum = Integer.parseInt(stack.peek().substring(1));
                        while (newStackNum >= number) {
                            if(counter > 0) {
                                counter--;
                            }
                            pop = stack.pop();
                            for (int j = 0; j < counter; j++) {
                                builder.append(" ");
                            }
                            builder.append(String.format("</%s>%n", pop));
                            if(stack.isEmpty()){
                                break;
                            }
                            newStackNum = Integer.parseInt(stack.peek().substring(1));
                        }
                        for (int j = 0; j < counter; j++) {
                            builder.append(" ");
                        }
                        builder.append(String.format("<%s>%n", input));
                        stack.push(input);
                    }
                }
            }
        }

        while (!stack.isEmpty()) {
            String pop = stack.pop();
            builder.append(String.format("</%s>%n", pop));
            counter--;
        }

        System.out.println(builder.toString());
    }
}
