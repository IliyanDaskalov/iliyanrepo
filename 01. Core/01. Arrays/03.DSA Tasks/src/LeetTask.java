public class LeetTask {

    public static void main(String[] args) {
        //array220([1, 2, 20], 0) → true
        //array220([3, 30], 0) → true
        //array220([3], 0) → false
        int[] numbers = {0};
        System.out.println(array220(numbers, 0));
    }

    // [1,2,3,4]  4  <= 3+1 4
    public static boolean array220(int[] nums, int index) {
        if (index + 1 >= nums.length) return false;

        if (nums[index] * 10 == nums[index + 1]) {
            return true;
        }
        return array220(nums, index + 1);
    }
}
