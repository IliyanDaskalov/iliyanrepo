import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class StudentsOrder {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] input = reader.readLine().split(" ");
        int[] NK = new int[2];
        NK[0] = Integer.parseInt(input[0]);
        NK[1] = Integer.parseInt(input[1]);
        int n = NK[0];
        int k = NK[1];
        String[] namesInput = reader.readLine().split(" ");
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(namesInput).subList(0, n));

        for (int i = 0; i < k; i++) {
            String[] rotations = reader.readLine().split(" ");
            if (list.remove(rotations[0])) {
                list.add(list.indexOf(rotations[1]), rotations[0]);
            }
        }

        for (int i = 0; i < n - 1; i++) {
            System.out.print(list.get(i) + " ");
        }
        System.out.println(list.get(n - 1));
    }
}