import java.util.Scanner;

public class LongestBlock {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        int counter = 1;
        int temp = 1;
        int index = 0;
        for (int i = input.length() - 1; i > 0 ; i--) {
            if (input.charAt(i) == input.charAt(i-1)) {
                counter++;
                if (counter >= temp) {
                    temp = counter;
                    index = i;
                }
            } else{
                counter = 1;
            }
        }

        for (int i = index; i < index+temp; i++) {
            System.out.print(input.charAt(index));
        }
    }
}
