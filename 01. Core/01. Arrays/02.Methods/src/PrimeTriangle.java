import java.util.Scanner;

public class PrimeTriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        boolean[] primes = new boolean[n + 1];
        for (int i = 1; i <= n; i++) {
            primes[i] = isPrime(i);
        }

        for (int i = 1; i <= n; i++) {
            if (!primes[i]) {
                continue;
            }
            for (int j = 1; j <= i; j++) {
                if (primes[j]) {
                    System.out.print(1);
                } else {
                    System.out.print(0);
                }
            }
            System.out.println();
        }
    }

    public static boolean isPrime(int number) {
        if(number  == 1){
            return true;
        }
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
