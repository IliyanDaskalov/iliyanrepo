import java.util.Scanner;

public class EvenNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        int[] array = new int[input.length()];

        for (int i = 0; i < input.length(); i++) {
            array[i] = input.charAt(i);
        }
        int counter = 0;
        int temp = 0;
        int index = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 48 && array[i] <= 57) {
                counter++;
                if (counter > temp) {
                    if (array[i] % 2 == 0) {
                        temp = counter;
                        index = i;
                    }
                }
            } else {
                counter = 0;
            }
        }

        if (temp == 0) {
            System.out.println(-1);
        } else {
            for (int i = index - temp + 1; i <= index; i++) {
                System.out.print(String.valueOf(input.charAt(i)));
            }
        }


    }
}
