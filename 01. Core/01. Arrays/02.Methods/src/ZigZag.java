import java.util.Scanner;

public class ZigZag {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] array = input.split(" ");
        int[] numbers = fillArray(array);

        int size1 = numbers[0];
        int size2 = numbers[1];

        calculateSum(size1,size2);

    }

    public static int[] fillArray (String[] strArr){

        int[] numbers = new int[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            numbers[i] = Integer.parseInt(strArr[i]);
        }

        return numbers;
    }
    public static void calculateSum(int size1, int size2){
        int index = 1;
        int temp = 1;
        long sum = 0;

        int [] simpleMatrix = new int[size2];

        for (int row = 0; row < size1; row++) {
            for (int col = 0; col < size2; col++) {
                simpleMatrix[col] = index;
                index += 3;
                if (row % 2 == 0) {
                    if (col % 2 == 0) {
                        if (col != 0 && col != size2 - 1 && row != 0 && row != size1 - 1) {
                            sum += (simpleMatrix[col]) * 2;

                        } else {
                            sum += simpleMatrix[col];

                        }
                    }
                } else {
                    if (col % 2 == 1) {
                        if (col != size2 - 1 && row != size1 - 1) {
                            sum += (simpleMatrix[col]) * 2;
                        } else {
                            sum += simpleMatrix[col];

                        }
                    }
                }
            }
            temp += 3;
            index = temp;
        }

        System.out.println(sum);
    }
}
