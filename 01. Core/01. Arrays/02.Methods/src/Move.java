import java.util.Scanner;

public class Move {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int position = Integer.parseInt(scanner.nextLine());
        String[] input = scanner.nextLine().split(",");
        int[] array = convertArray(input);

        int sumForward = 0;
        int sumBackwards = 0;
        int times = 0;
        int size = 0;
        String[] temp = {""};

        do {
            temp = scanner.nextLine().split(" ");
            if (temp[0].equals("exit")){
                break;
            }
            times = Integer.parseInt(temp[0]);
            size = Integer.parseInt(temp[2]);

            if(temp[1].equals("forward")){
                for (int i = 0; i < times; i++) {
                    position = (position + size) % array.length;
                    sumForward += array[position];
                }
            } else if(temp[1].equals("backwards")){
                for (int i = 0; i < times; i++) {
                    position = position - size;
                    while (position < 0) {
                        position += array.length;
                    }
                    sumBackwards += array[position];
                }
            }
        } while (true);

        System.out.printf("Forward: %d%n", sumForward);
        System.out.printf("Backwards: %d%n", sumBackwards);

    }

    public static int[] convertArray(String[] array) {
        int[] numbers = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            numbers[i] = Integer.parseInt(array[i]);
        }

        return numbers;
    }
}
