import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class Navigation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //The input data is given at the standard input, i.e. the console
        //On the first and the second lines you will find the dimensions of the field R and C
        //On the third line you will find the number N, the number of moves
        //On the fourth line you will find the CODEs, decimal numbers that represents the positions from the path of the pawn. They will be separated by a single space. The position is encoded as follows:
        //A coefficient is calculated, COEFF = MAX(R, C)
        //ROW = CODE / COEF
        //COL = CODE % COEF
        //5
        //6
        //4
        //14 27 1 5
        int r = Integer.parseInt(scanner.nextLine());
        int c = Integer.parseInt(scanner.nextLine());
        int moves = Integer.parseInt(scanner.nextLine());
        String [] tempArr = scanner.nextLine().split(" ");

        ArrayList<Integer> code  = new ArrayList<>();

        for (int i = 0; i < moves; i++) {
            code.add(Integer.parseInt(tempArr[i]));
        }

        BigInteger [][] matrix = new BigInteger[r][c];
        int index = r - 1;
        for (int row = 0; row < r; row++) {
            for (int col = 0; col < c; col++) {
                matrix[row][col] = BigInteger.valueOf(2).pow(index + col);
            }
            index--;
        }

        int coeff = Math.max(r, c);

//        for (BigInteger[] row : matrix) {
//            for (BigInteger cells : row) {
//                System.out.print(cells + "\t");
//            }
//            System.out.println();
//        }

        int temp = 0;
        BigInteger sum = BigInteger.valueOf(0);
        int positionRows = r - 1;
        int positionCol = 0;

        while (temp < moves){
            int moveR = code.get(temp) / coeff;
            int moveC = code.get(temp) % coeff;

            if(positionCol <= moveC){
                for (int i = positionCol; i <= moveC; i++) {
                    sum = sum.add(matrix[positionRows][i]);
                    matrix[positionRows][i] = BigInteger.valueOf(0);
                }
                if(positionRows>=moveR){
                    for (int i = positionRows; i >= moveR ; i--) {
                        sum = sum.add(matrix[i][moveC]);
                        matrix[i][moveC] = BigInteger.valueOf(0);

                    }
                } else {
                    for (int i = positionRows; i <= moveR ; i++) {
                        sum = sum.add(matrix[i][moveC]);
                        matrix[i][moveC] = BigInteger.valueOf(0);
                    }
                }

            } else {
                for (int i = positionCol; i >= moveC; i--) {
                    sum = sum.add(matrix[positionRows][i]);
                    matrix[positionRows][i] = BigInteger.valueOf(0);
                }
                if(positionRows>=moveR){
                    for (int i = positionRows; i >= moveR ; i--) {
                        sum = sum.add(matrix[i][moveC]);
                        matrix[i][moveC] = BigInteger.valueOf(0);
                    }
                } else {
                    for (int i = positionRows; i <= moveR ; i++) {
                        sum = sum.add(matrix[i][moveC]);
                        matrix[i][moveC] = BigInteger.valueOf(0);
                    }
                }
            }

        positionRows = moveR;
        positionCol = moveC;
            temp++;
        }
        System.out.println(sum);
    }
}
