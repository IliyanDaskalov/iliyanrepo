import java.util.Scanner;

public class LongestIncreasingSequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int[] numbers = new int[n];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(scanner.nextLine());
        }
        printLongest(numbers,n);
    }
    public static void printLongest(int[] array, int size){
        int counter = 1;
        int temp = 1;

        for (int i = 0; i < size - 1; i++) {
            if (array[i] < array[i + 1]) {
                counter++;
                if (counter > temp) {
                    temp = counter;
                }
            } else{
                counter = 1;
            }

        }
        System.out.println(temp);

    }
}
