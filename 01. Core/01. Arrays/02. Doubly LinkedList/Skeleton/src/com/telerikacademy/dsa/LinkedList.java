package com.telerikacademy.dsa;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {
    private Node head;
    private Node tail;
    private int size;

    public LinkedList() {
        this.size = 0;
    }

    public LinkedList(Iterable<T> iterable) {
        iterable.forEach(this::addLast);
    }

    @Override
    public void addFirst(T value) {
        Node temp = new Node(value);
        if (size() == 0) {
            head = temp;
            tail = temp;
            tail.prev = head;
        } else {
            temp.next = head;
            head = temp;
        }
        size++;
    }

    @Override
    public void addLast(T value) {
        Node temp = new Node(value);
        if (size() == 0) {
            head = temp;
            tail = temp;
            tail.prev = head;
        } else {
            temp.prev = tail;
            tail.next = temp;
            tail = temp;
        }
        size++;
    }

    @Override
    public void add(int index, T value) {
        Node temp = new Node(value);
        Node oldElement = head;
        if (index < 0 || index > size() - 1) {
            throw new NoSuchElementException();
        }
        if (index == 0) {
            if (size() == 0) {
                head = temp;
                tail = temp;
                tail.prev = head;

            } else {
                temp.next = head;
                head = temp;
            }
        } else if (index == size() - 1) {
            temp.prev = tail;
            tail = temp;
        } else {
            for (int i = 0; i < index; i++) {
                oldElement = oldElement.next;
            }
            temp.next = oldElement;
            temp.prev = oldElement.prev;
            oldElement.prev = temp;
        }
        size++;
    }

    @Override
    public T getFirst() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        return head.value;
    }

    @Override
    public T getLast() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        return tail.value;
    }

    @Override
    public T get(int index) {
//        int count = 0;
//        while ( selected!= null){
//            if(count == index){
//                return selected;
//            }
//            count++;
//            selected = selected.next;
//        }
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(T value) {

        throw new UnsupportedOperationException();
    }

    @Override
    public T removeFirst() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        var removed = head;
        if (size() == 1) {
            head = null;
            tail = null;
        } else {
            head = head.next;
        }
        size--;
        return removed.value;
    }

    @Override
    public T removeLast() {
        if (size() == 0) {
            throw new NoSuchElementException();
        }
        var removed = tail;
        if (size() == 1) {
            head = null;
            tail = null;
        } else {
            tail = tail.prev;
        }
        size--;
        return removed.value;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<T> iterator() {

        return new Iterator<T>() {

            Node current = head;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                T result = current.value;
                current = current.next;

                return result;
            }

        };
    }

    private class Node {
        T value;
        Node prev;
        Node next;

        Node(T value) {
            this.value = value;
        }
    }
}
