package com.telerikacademy.web.springdemo.services;

import com.telerikacademy.web.springdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.web.springdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.web.springdemo.exceptions.UnauthorizedOperationException;
import com.telerikacademy.web.springdemo.models.Beer;
import com.telerikacademy.web.springdemo.models.User;
import com.telerikacademy.web.springdemo.repositories.BeerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.web.springdemo.Helpers.createMockBeer;
import static com.telerikacademy.web.springdemo.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class BeerServiceImplTests {

    @Mock
    BeerRepository mockRepository;

    @InjectMocks
    BeerServiceImpl service;

    @Test
    public void getById_Should_ReturnBeer_When_MatchExist() {
        // Arrange
        Mockito.when(mockRepository.getById(2))
                .thenReturn(new Beer(2, "Name2", 1.3));
        // Act
        Beer result = service.getById(2);

        // Assert
        Assertions.assertEquals(2, result.getId());
        Assertions.assertEquals("Name2", result.getName());
        Assertions.assertEquals(1.3, result.getAbv());
    }

    @Test
    public void create_Should_Throw_When_BeerWithSameNameExists() {
        // Arrange
        var mockBeer = createMockBeer();

        Mockito.when(mockRepository.getByName(mockBeer.getName()))
                .thenReturn(mockBeer);

        // Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockBeer));
    }

    @Test
    public void create_Should_CallRepository_When_BeerWithSameNameDoesNotExist() {
        // Arrange
        var mockBeer = createMockBeer();

        Mockito.when(mockRepository.getByName(mockBeer.getName()))
                .thenThrow(new EntityNotFoundException("Beer", "name", mockBeer.getName()));

        // Act, Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(Mockito.any(Beer.class));
    }

    //update() calls the repository when the user is creator
    @Test
    public void update_Should_CallRepository_WhenUserIsCreator() {
        BeerRepository mockRepo = Mockito.mock(BeerRepository.class);
        Beer mockBeer = createMockBeer();
        User mockUser = createMockUser();
        mockBeer.setCreatedBy(mockUser);

        Mockito.when(mockRepo.getById(mockBeer.getId()))
                .thenReturn(mockBeer);

        Mockito.when(mockRepo.getByName(Mockito.anyString()))
                .thenThrow(new EntityNotFoundException("Beer", "name", mockBeer.getName()));

        Mockito.verify(mockRepo, Mockito.times(3))
                .update(Mockito.any(Beer.class));
    }

    @Test
    public void update_Should_Throw_When_UserIsNotCreatorOrAdmin() {
        Beer mockBeer = createMockBeer();
        mockBeer.setCreatedBy(new User("MockUser"));

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockBeer);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockBeer, createMockUser()));
    }

    @Test
    public void update_Should_Throw_When_BeerNameIsTaken() {
        var mockBeer = createMockBeer();
        var anotherMockBeer = createMockBeer();
        anotherMockBeer.setId(2);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockBeer);

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(anotherMockBeer);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockBeer, createMockUser()));
    }
}
