package com.telerikacademy.web.springdemo;

import com.telerikacademy.web.springdemo.models.Beer;
import com.telerikacademy.web.springdemo.models.Role;
import com.telerikacademy.web.springdemo.models.Style;
import com.telerikacademy.web.springdemo.models.User;

import java.util.Set;

public class Helpers {

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setRoles(Set.of(createMockRole()));
        return mockUser;
    }

    public static Beer createMockBeer() {
        var mockBeer = new Beer();
        mockBeer.setId(1);
        mockBeer.setName("TestBeer");
        mockBeer.setCreatedBy(createMockUser());
        mockBeer.setStyle(createMockStyle());
        return mockBeer;
    }

    public static Role createMockRole() {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setName("User");
        return mockRole;
    }

    public static Style createMockStyle() {
        var mockStyle = new Style();
        mockStyle.setId(1);
        mockStyle.setName("Ale");
        return mockStyle;
    }


}
