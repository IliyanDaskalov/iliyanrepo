package com.telerikacademy.web.demo.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }


    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s %s not found", type, attribute, value));
    }

    public EntityNotFoundException(String type, String firstAttribute,String secondAttribute, String value1, String value2) {
        super(String.format("%s with %s %s %s %s not found", type, firstAttribute, secondAttribute, value1, value2));
    }
}
