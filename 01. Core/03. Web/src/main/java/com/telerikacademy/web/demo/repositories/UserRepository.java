package com.telerikacademy.web.demo.repositories;

import com.telerikacademy.web.demo.models.User;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    User getById(int id);

}
