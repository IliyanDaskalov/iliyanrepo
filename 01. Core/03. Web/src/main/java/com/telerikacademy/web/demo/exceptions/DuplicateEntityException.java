package com.telerikacademy.web.demo.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", type, attribute, value));
    }

    public DuplicateEntityException(String type, String attribute1,String attribute2, String value1, String value2) {
        super(String.format("%s with %s %s %s %s already exists.", type, attribute1, attribute2, value1, value2));
    }
}
