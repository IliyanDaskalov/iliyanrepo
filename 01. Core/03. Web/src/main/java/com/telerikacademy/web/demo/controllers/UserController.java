package com.telerikacademy.web.demo.controllers;

import com.telerikacademy.web.demo.exceptions.EntityNotFoundException;
import com.telerikacademy.web.demo.models.Beer;
import com.telerikacademy.web.demo.models.User;
import com.telerikacademy.web.demo.models.WishlistDto;
import com.telerikacademy.web.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getAll(){
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id){
        try{
            return userService.getById(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/wish-list")
    public List<Beer> getWishList(@PathVariable int id){
        return new ArrayList<>(getById(id).getWishList());
    }

    @PostMapping("/{id}/wish-list")
    public void addBeerToUserWishlist(@Valid @RequestBody WishlistDto wishlistDto, @PathVariable int id){

    }
}
