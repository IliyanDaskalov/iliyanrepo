package com.telerikacademy.web.demo.services;

import com.telerikacademy.web.demo.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(int id);

}
