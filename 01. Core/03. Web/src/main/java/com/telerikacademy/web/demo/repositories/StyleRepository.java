package com.telerikacademy.web.demo.repositories;

import com.telerikacademy.web.demo.models.Style;

public interface StyleRepository {

    Style getById(int id);

}

