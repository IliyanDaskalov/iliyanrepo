Message Service

Overview
A simple chat application that allows users to communicate to each other with private messages.

The requirement to start the project is Java 11. 

The project can be simply run from the MessageServiceApplication by executing it. 
Once the project has started it starts listening on port 8181. You can go to http://localhost:8181/ 
to start testing and playing with the application. 

The following steps should be done in order to make it work. 
From the UI you can see that it requires authentication, so you need to enter random Authentication key and 
a user ID with which you want to connect to the server. Once you hit connect it provides a system message that you
have connected. The same step can be done from another incognito browser tab in order to connect as client 2.
Once you have the 2 clients attached to the server you can start sending messages to each other.
In order to do that you need to enter the user ID to whom you want to message and type down the message.
If you have entered a valid connected user ID, this user should receive the message and a red box with notification
for how many unread messages he has received. If the user presses the box with the notifications the counter gets reset.

The other way to send a message to a connected user is through Postman. Once you start Postman you can create a
POST request to the following endpoint localhost:8181/v1/{userId}. You should replace the {userId} in the endpoint
with a valid connected to the server user ID. After you do that you can body with a message.

