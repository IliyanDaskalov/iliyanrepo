package com.example.messageservice.models;

public class WebSocketResponseMessage {

    private String content;
//    private String dateTime;

    public WebSocketResponseMessage(String content) {
        this.content = content;

    }

    public WebSocketResponseMessage() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


//    public String getDateTime() {
//        return dateTime;
//    }
//
//    public void setDateTime(String dateTime) {
//        this.dateTime = dateTime;
//    }
}
